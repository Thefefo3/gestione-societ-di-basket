// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();

//Create a static server to access the website
gulp.task('browser-sync', ['watch'], function(){
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

// Compile Sass
gulp.task('sass', function() {
    return gulp.src('app/views/main.sass')
        .pipe(sass()).on('error', sass.logError)
        .pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
        .pipe(gulp.dest('build/'))
        .pipe(browserSync.stream());
});

// Concatenate Assets
gulp.task('assets', function() {
    return gulp.src('./assets/js/**/*.js')
        .pipe(concat('assets.js'))
        .pipe(gulp.dest('build'))
        .pipe(browserSync.stream());
});

// Concatenate App files
gulp.task('app', function() {
    return gulp.src(['./app/app.js', './app/**/*.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('build'))
        .pipe(browserSync.stream());
});

// Concatenate Libraries
// Important! Concatenate the angular.js file first or you will have dependency problems!
gulp.task('libs', function() {
    return gulp.src(['./assets/libs/angular.js', './assets/libs/**/*.js'])
        .pipe(concat('libs.js'))
        .pipe(uglify('libs.js'))
        .pipe(gulp.dest('build'))
        .pipe(browserSync.stream());
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('./app/**/*.js', ['app']).on('change', browserSync.reload);
    gulp.watch('./app/views/**/*.sass', ['sass']).on('change', browserSync.reload);
    gulp.watch('./assets/js/**/*.js', ['assets']).on('change', browserSync.reload);
});

// Default Task
gulp.task('default', ['browser-sync', 'sass', 'libs', 'assets', 'app', 'watch']);