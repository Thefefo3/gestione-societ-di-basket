angular
	.module('gsb', [
		'ngAnimate',
		'ui.router',
		'ngCookies',
		'ui.bootstrap',
		'mwl.calendar',
		'ngImgCrop',
		'ngFileUpload',
		'jkuri.gallery'
	])
	.config(function($stateProvider, $urlRouterProvider, calendarConfig) {
		$urlRouterProvider
			.otherwise('/home');
		$stateProvider
			.state('container', {
				template: '<homepage></homepage>'
			})
			.state('container.home-view', {
				url: '/home',
				template: '<home-view></home-view>'
			})
			.state('container.manage-teams', {
				url: '/gestione-squadre',
				template: '<manage-teams></manage-teams>'
			})
			.state('container.calendar', {
				url: '/calendario-partite',
				template: '<calendar></calendar>'
			})
			.state('container.administration', {
				url: '/amministrazione',
				template: '<administration></administration>'
			});

		//Calendar config
		calendarConfig.i18nStrings.weekNumber = '';
		calendarConfig.dateFormatter = 'moment';
	})
	.run(function($rootScope, $location, $cookieStore, $http, $state){
		$rootScope.globals = $cookieStore.get('globals') || {};
		if($rootScope.globals.currentUser){
			$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
		}
		$rootScope.$on('$stateChangeStart', function(event, toState){
			if($rootScope.globals.currentUser){
				if($rootScope.globals.currentUser.loggedIn == true && $rootScope.globals.currentUser.role == 'Allenatore' && toState.name == 'container.administration'){
					Materialize.toast('Non hai i permessi per accedere a questa pagina!', 3000);
					event.preventDefault();
				}
			}
			else{
				if(toState.name != 'container.home-view' && toState.name != 'container.calendar'){
					Materialize.toast('Non hai i permessi per accedere a questa pagina!', 3000);
					event.preventDefault();
				}
			}
		});
	});