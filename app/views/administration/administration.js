angular
	.module('gsb')
	.directive('administration', function(){
		return {
			restrict: 'E',
			templateUrl: 'app/views/administration/administration.html',
			controller: function($scope, $http, $uibModal){
				$scope.trainers = [];

				getTrainers();
				//Get trainers
				function getTrainers(){
					$http.get('assets/php/convoked/get-trainers.php')
					.then(function(response){
						$scope.trainers = response.data;
					});
				}				

				$scope.changeAdminPass = function(username){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/administration/change-admin-password.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, $http){

							$scope.changePass = function(old_pass, new_pass){
								var post_data = { 'username':username, 'old_password':old_pass, 'new_password':new_pass };
								$http.post('assets/php/login/change-admin-password.php', post_data)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
				            			Materialize.toast(response.data['success'], 3000);
										$uibModalInstance.dismiss('cancel');
				            		}
								});
							}

							//Verify if both passwords are equals
							$scope.passwordMatchError = function(password, password_confirm){
								if(password === password_confirm){
									return false;
								}
								else{
									return true;
								}
							}

							//Close modal
							$scope.cancel = function() {
								$uibModalInstance.dismiss('cancel');
							};
						}
					});
				}

				$scope.setLoginData = function(trainer){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/administration/set-login-data.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, $http){
							$scope.trainer = trainer;


							$scope.setData = function(username, password, trainer_id){
								var post_data = { 'username':username, 'password':password, 'trainer_id':trainer_id };
								$http.post('assets/php/login/set-trainer-login-data.php', post_data)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
				            			Materialize.toast(response.data['success'], 3000);
				            			getTrainers();
										$uibModalInstance.dismiss('cancel');
				            		}
								});
							}

							//Verify if both passwords are equals
							$scope.passwordMatchError = function(password, password_confirm){
								if(password === password_confirm){
									return false;
								}
								else{
									return true;
								}
							}

							//Close modal
							$scope.cancel = function() {
								$uibModalInstance.dismiss('cancel');
							};
						}
					});
				}
			}
		};
	});