angular
	.module('gsb')
	.directive('calendar', function(){
		return {
			restrict: 'E',
			templateUrl: 'app/views/calendar/calendar.html',
			controller: function($scope, $uibModal, $rootScope, $http){
				$scope.calendarView = 'month';
				$scope.calendarTitle = "Calendario partite";
				$scope.calendarDate = moment();
				$scope.opponents = [];
				$scope.events = [];

		  		getOpponentsAndMatches();
				
		  		//On event click
				$scope.eventClicked = function(event){
					var modalInstance = $uibModal.open({
	            		animation: 'true',
			            templateUrl: 'app/views/calendar/manage-match.html',
			            windowClass: 'ng-gallery--modal',
			            controller: function($scope, $rootScope, $uibModalInstance, $http, $filter){
			            	$scope.match = event;
			            	$scope.players = [];
			            	$scope.trainers = [];
			            	$scope.convoked_trainers_view = [];
			            	$scope.convoked_players = [];
			            	$scope.convoked_players_view = [];
			            	$scope.match_result = null;
			            	$scope.match_images = [];

			            	getMatchResult();

			            	//Get players
							var post_data = { 
		            			'category': $scope.match.game.category
		            		};
			            	$http.post('assets/php/convoked/get-players-per-category.php', post_data)
			            	.then(function(response){
		            			$scope.players = response.data;

		            			//Get trainers
		            			$http.get('assets/php/convoked/get-trainers.php')
		            			.then(function(response){
		            				$scope.trainers = response.data;

		            				//Get convoked users
			            			var post_data = { 
				            			'match_id': $scope.match.game.id
				            		};
			            			$http.post('assets/php/convoked/get-convoked-users.php', post_data)
					            	.then(function(response){
				            			$scope.convoked_users = response.data;
				            			$scope.convoked_players_view = [];
				            			$scope.convoked_trainers_view = [];

				            			//Checking convoked players
				            			angular.forEach($scope.players, function(player){
				            				if(isInConvoked(player.id) == true){
				            					$scope.convoked_players_view.push({user: player, convoked: true});
				            				}else{
				            					$scope.convoked_players_view.push({user: player, convoked: false});
				            				}
				            			});
				            			//Checking convoked trainers
				            			angular.forEach($scope.trainers, function(trainer){
				            				if(isInConvoked(trainer.id) == true){
				            					$scope.convoked_trainers_view.push({user: trainer, convoked: true});
				            				}else{
				            					$scope.convoked_trainers_view.push({user: trainer, convoked: false});
				            				}
				            			});


				            			//Function to check if a user is already convoked
				            			function isInConvoked(input){
				            				var out = false;
				            				angular.forEach($scope.convoked_users, function(convoked_user){
				            					if(convoked_user.id_user === input){
				            						out = true;
				            					}
				            				});
				            				return out;
				            			}
					            	});
		            			});

		            			
			            	});

							//Change convoked list
							$scope.chooseConvoked = function(convoked_players, convoked_trainers, time, place){
								var format_time = $filter('date')(new Date(time), "HH:mm:ss");
								var true_list = [];	//Contains new convoked users
								for(var i = 0; i < convoked_players.length; i++){
									if(convoked_players[i].convoked == 1){
										true_list.push({
											player: convoked_players[i].user,
											time: format_time,
											place: place
										});
									}
								}
								for(var i = 0; i < convoked_trainers.length; i++){
									if(convoked_trainers[i].convoked == 1){
										true_list.push({
											player: convoked_trainers[i].user,
											time: format_time,
											place: place
										});
									}
								}
								var post_data = {
			            			'convoked_players': true_list,
			            			'match_id': $scope.match.game.id
			            		};
								$http.post('assets/php/convoked/convoke_players.php', post_data)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
										Materialize.toast(response.data['success'], 3000);
				            		}
								});
							}

			            	//Define result modal
			            	$scope.defineResult = function(match){
					  			var modalInstance = $uibModal.open({
					  				animation: 'true',
					  				templateUrl: 'app/views/calendar/define-result.html',
					  				controller: function($scope, $uibModalInstance, $http, Upload){
					  					$scope.match = match;
					  					$scope.result = {};
					  					
					  					//upload result data
					  					$scope.upload = function(result, images){
					  						Upload.upload({
					  							url:'assets/php/result/add-result.php',
					  							method: 'POST',
					  							file: images,
					  							sendFieldsAs: 'form',
					  							fields: {
					  								form_data: result,
					  								match_id: match.game.id
					  							}
					  						})
					  						.then(function(response){
					  							if(response.data.hasOwnProperty('error')){
							            			Materialize.toast(response.data['error'], 3000);
							            		}
							            		else{
							            			$uibModalInstance.dismiss('cancel');
							            		}
					  						});
					  					}
					  					//Close modal
										$scope.cancel = function () {
											$uibModalInstance.dismiss('cancel');
											getMatchResult();
										};
					  				},
					  				size: 'lg'
					  			});
					  		}

					  		//Delete result modal
					  		$scope.deleteResult = function(match){
					  			$http.post('assets/php/result/delete-result.php', match.game.id)
					  			.then(function(response){
					  				if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else{
				            			Materialize.toast(response.data['success'], 3000);
				            		}
				            		$uibModalInstance.dismiss('cancel');
					  			});
					  		}

					  		//Match PDF Modal
					  		$scope.matchPDF = function(){
					  			var modalInstance = $uibModal.open({
					  				animation: 'true',
					  				templateUrl: 'app/views/calendar/match-pdf.html',
					  				controller: function($scope, $uibModalInstance, $http, match, $filter){
					  					$scope.match = match;
					  					
					  					$scope.generate = function(min_shirt_numb){
											table_data = [];
											header = [];
											var body = [];
					  						var post_data = { 'match_id': $scope.match.game.id };
					  						$http.post('assets/php/pdf/get-convoked-players-data.php', post_data)
					  						.then(function(response){
					  							if(response.data.hasOwnProperty('error')){
							            			Materialize.toast(response.data['error'], 3000);
							            		}
							            		else{
							            			var convoked_users_data = response.data;
							            			var convoked_players_data = [];
							            			var convoked_trainers_data = [];
							            			var convoked_trainers_view = '';
							            			//Split trainers and players
							            			for(var i = 0; i < convoked_users_data.length; i++){
							            				if(convoked_users_data[i].role == 'Giocatore'){
							            					convoked_players_data.push(convoked_users_data[i]);
							            				}
							            				else{
							            					convoked_trainers_data.push(convoked_users_data[i]);
							            				}
							            			}
							            			//Prepare convoked_trainers_view
							            			for(var i = 0; i < convoked_trainers_data.length; i++){
							            				convoked_trainers_view += convoked_trainers_data[i].first_name + " " + convoked_trainers_data[i].last_name + ", ";
							            			}
													header.push({text: 'N. Tessera - Cognome - Nome', colSpan: 2, style: 'tableHeader'});
													header.push({});
													header.push({alignment: 'center', text: 'Maglia', style: 'tableHeader'});
													header.push({alignment: 'center', text: '1', style: 'tableHeader'});
													header.push({alignment: 'center', text: '2', style: 'tableHeader'});
													header.push({alignment: 'center', text: '3', style: 'tableHeader'});
													header.push({alignment: 'center', text: '4', style: 'tableHeader'});
													header.push({alignment: 'center', text: '5', style: 'tableHeader'});
													header.push({alignment: 'center', text: '6', style: 'tableHeader'});
													body.push(header);
							            			for(var i = 0; i < 12; i++){
							            				var dataRow = [];
							            				try{
								  							dataRow.push(
								  								{text: convoked_players_data[i].membership_num, style: 'dataStyle'},
								  								{text: convoked_players_data[i].first_name + " " + convoked_players_data[i].last_name, style: 'dataStyle'},
								  								{text: (min_shirt_numb + i).toString(), alignment: 'center', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'}
								  							);
								  						}catch(err){
								  							dataRow.push(
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'}
								  							);
								  						}
								  						body.push(dataRow);
							            			}


							  						var docDefinition = { 
														content: [
															{
																text: 'COMMISSIONE CANTONALE MINIBASKET'
															},
															{
																image: 'logo',
																width: 226,
																height: 105,
																alignment: 'right',
																margin: [0, 0, 0, 10]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'CATEGORIA'
																	},
																	{
																		margin: [40, -7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [250],
																			body: [
																				[$scope.match.game.category],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'DATA'
																	},
																	{
																		margin: [74, -7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [100],
																			body: [
																				[$filter('date')(new Date($scope.match.game.date), "dd/MM/yyyy")],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'INCONTRO'
																	},
																	{
																		margin: [45, -7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [300],
																			body: [
																				[$scope.match.opponent],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'ISTRUTTORI',
																		margin: [0, 13, 0, 0]
																	},
																	{
																		margin: [38	, 10, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [300],
																			body: [
																				[convoked_trainers_view],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																style: 'tableData',
																table: {
																	headerRows: 1,
																	widths: [100, 200, 50, '*', '*', '*', '*', '*', '*'],
																	body: body
																}
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'DATA'
																	},
																	{
																		margin: [74, 7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [100],
																			body: [
																				[''],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'INCONTRO'
																	},
																	{
																		margin: [45, 7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [300],
																			body: [
																				[''],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]	
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'ISTRUTTORI',
																		margin: [0, 13, 0, 0]
																	},
																	{
																		margin: [38	, 20, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [300],
																			body: [
																				[''],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																style: 'tableData',
																table: {
																	headerRows: 1,
																	widths: [100, 200, 50, '*', '*', '*', '*', '*', '*'],
																	body: [
																		[
																			{text: 'N. Tessera - Cognome - Nome', colSpan: 2, style: 'tableHeader'},
																			{},
																			{alignment: 'center', text: 'Maglia', style: 'tableHeader'},
																			{alignment: 'center', text: '1', style: 'tableHeader'},
																			{alignment: 'center', text: '2', style: 'tableHeader'},
																			{alignment: 'center', text: '3', style: 'tableHeader'},
																			{alignment: 'center', text: '4', style: 'tableHeader'},	
																			{alignment: 'center', text: '5', style: 'tableHeader'},
																			{alignment: 'center', text: '6', style: 'tableHeader'}
																		],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' ']
																	]
																}
															}
														],
														styles: {
															tableData: {
																margin: [0, 5, 0, 15]
															},
															dataStyle: {
																fontSize: 9
															},
															tableHeader: {
																fontSize: 13,
																bold: true
															}
														},
														images: {
															logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAABSCAYAAAD90P6tAAAABGdBTUEAALGPC/xhBQAACt1pQ0NQaWNjAAB4AZXBZ1hThxoA4O+c7JCwEiIg4yAbTDAogYDICCFEQCAiJBGVeHKACISYQdwTUcE6UBHBiVRFLVqtoNSJWynuPYp6cddiFReOa/WHz3PHj74vACspRacvQe0BivVmo0IWjylVaozWASgwgApCAA1uMqQOT8yEL+RSCWZSyOLhOwTg9RVA4IuL/KR0DIN/xgE3GM0ASDoADNQSJhwAKQOAIqvZYAZAugGAO7bQYAZAyQDANSpVagDUGQC4+UqVGgDtDwDcsUqVGgCNBQCutlivBUAzAcCgLdZqAdCjADCv1EJoAUgpAFBeqiOsAKRLAOBTZCnWAZDeAgC3mNCYAMgsAPAxE3gBAHkAALCMmQoJAHkwAJ2Vn6mQAJAHA9BZYzMVEgDyYAA6y0xMMMMXkhLDRKMuv8CMBeCBmFAsjsCSCGsRYTbz0zV4ocaoxSQlxQaNfiKAljDh8I2jSSGLx+RSiUgoFon4oQIhfOdoUsjiMblUIhKKRSJ+qEAI/5hSpcbgq5cZgAAAwmtXqtQY/AelSo3BVyX1ABE9AKTFSpUag6/GrgRomQPgfEOpUmPwlc86ALsygOazWsKEwzc8uVSCFZjNhsiQEKvVKtARuAAvgO94cqkEKzCbDZEhIVarVaAjcAFeAP+IljDh8I1ALpVgIqFYJOKHCoRYApGnsRSZMYUsHsNLikosRsxk0OAExsdMClk8fCeQSyWYSCgWifihAiGWQORpLEVmTCGLx/CSohKLETMZNDiB8TGTQhYP/5OWMOHwTX8FkUcYCT1OYFk6wqrT52OSEr1WZ9aV6DGdHpNLJSKhWCTihwqE8F1/BZFHGAk9TmBZOsKq0+djkhK9VmfWlegxnR6TSyUioVgk4ocKhPB/leoIK/yN0/AJuGMEYH+WC6Q/2oHMsQFSzhoAQJQqNQZ/S2FkQToAZHs+LCY0JvgGgf+CLgIA1KTLx+ALiSITwy3GUviKDF9QgAl2wAUX8ABvCAA+hEI4REEsSGEopEEmqGA04FAAxWAEK0yBmVAOlbAYlkMtrIUGaIQm2AktsA8Ow3E4A+fgMtyETuiCp9ANr6EXQRAawkY4iAviifgiwUgoEoEMQaRICqJAVEguko/oEQsyBZmNVCJVSC2yHmlEfkb2IoeRU8h55DpyF3mM/IW8R0koC+Wi7qgfGoJGoHFoMpqJjkLz0fHoJLQMXYjWoPXoNrQZPYyeQS+jnehTtIcEJBsSj+RF4pMiSBJSGklNyiMZSdNIFaRqUj2pidRKOkG6SOokPSO9I1PJHDJG5pOjyEnkEWScPJ48jbyAXEveTG4mHyVfJN8ld5M/UdgUN0owJZIipygp+RQrpZxSTdlI2U05RrlM6aK8plKpPKo/NZyaRFVRx1EnUxdQV1O3Uw9Rz1PvU3toNJoLLZgWTUujaWhmWjltJW0b7SDtAq2L9pZuQ/ekh9IT6Wq6nj6LXk3fQj9Av0B/SO9l2DN8GZGMNIaWMZGxiNHAaGWcZXQxepkOTH9mNDOTOY45k1nDbGIeY95ivrSxselnI7bJsNHZzLCpsdlhc9Lmrs07liMriCVh5bAsrIWsTaxDrOusl2w2248dy1azzeyF7Eb2EfYd9ltbjq3AVm6rtZ1uW2fbbHvB9rkdw87XLs5utN0ku2q7XXZn7Z7ZM+z97CX2Gvtp9nX2e+2v2vc4cByEDmkOxQ4LHLY4nHJ45Ehz9HOUOmodyxw3OB5xvM8hcbw5Eg7Omc1p4BzjdHGpXH+unDuOW8n9idvB7XZydBrklOU0wanOab9TJ4/E8+PJeUW8RbydvCu8933c+8T1IfrM79PU50KfN859nWOdCecK5+3Ol53fu2AuUpdClyUuLS63XcmuQa4ZrlbXNa7HXJ/15faN6ov3rei7s+8NN9QtyE3hNtltg1u7W4+7h7vM3eC+0v2I+zMPnkesxziPZR4HPB57cjyHeOo8l3ke9HyCOWFxWBFWgx3Fur3cvJK8LF7rvTq8evv59xvRb1a/7f1uezO9I7zzvJd5t3l3+3j6pPpM8dnqc8OX4RvhW+C7wveE7xs/f79sv7l+LX6P/J395f6T/Lf63wpgB8QEjA+oD7gUSA2MCCwMXB14LggNCgsqCKoLOhuMBouCdcGrg8/3p/QX99f3r+9/lc/ix/FL+Vv5dwU8QYpglqBF8DzEJ0QdsiTkRMinAWEDigY0DLgpdBQOFc4Stgr/Cg0KxUPrQi8NZA9MHDh94J6BLwYFDyIGrRl0LYwTlho2N6wt7KMoXGQUNYkeh/uE54avCr8awY1Ij1gQcVJMEceLp4v3id9FiiLNkTsj/4ziRxVGbYl6NNh/MDG4YfD96H7Rmuj10Z1DsCG5Q9YN6YzxitHE1Mfci/WO1cZujH0YFxg3Lm5b3PP4AfHG+N3xbySRkqmSQwmkBFlCRUKH1FE6QlorvZPYLzE/cWtityxMNll2KImSlJy0JOmq3F2Oyxvl3UPDh04dejSZlTw8uTb5XkpQijGlNRVNHZq6NPXWMN9h+mEtaZAmT1uadjvdP318+q8Z1Iz0jLqMBwqhYorixHDO8DHDtwx/nRmfuSjz5oiAEZYRbVl2WTlZjVlvshOyq7I7lSHKqcozKleVTrVHTVNnqTeqe0ZKRy4f2ZUTllOec2WU/6gJo06Ndh1dNHr/GLsxmjG7cim52blbcj9o0jT1mp6x8rGrxnbjEnwF/lQbq12mfUxEE1XEw7zovKq8R/nR+UvzHxfEFFQXPNNJdLW6F+OSxq0d96YwrXBT4eei7KLtxfTi3OK9ekd9of5oiUfJhJLzhmBDuaFzfOT45eO7jcnGjSbENMq0x8w1G8ztlgDLHMvd0iGldaVvrVnWXRMcJugntE8Mmjh/4sNJiZN+nEyejE9um+I1ZeaUu1Pjpq6fhkwbO61tuvf0suldM2QzNs9kziyc+dusAbOqZr2anT27tcy9bEbZ/TmyOVvLbcuN5VfnRs1dO488TzevY/7A+Svnf6rQVpyuHFBZXflhAb7g9A/CH2p++Lwwb2HHItGiNYupi/WLryyJWbK5yqFqUtX9palLm5dhyyqWvVo+Zvmp6kHVa1cwV1hWdNak1OxZ6bNy8coPtQW1l+vi67avcls1f9Wb1drVF9bErmla6762cu37dbp119bL1jfX+9VXb6BuKN3woCGr4cSPET82bnTdWLnx4yb9ps7Nis1HG8MbG7e4bVm0Fd1q2fp4W862cz8l/LSnid+0fjtve+UO2GHZ8eTn3J+v7Eze2bYrYlfTL76/rNrN2V3RjDRPbO5uKWjp3KPac37v0L1trVGtu38V/Lppn9e+uv1O+xcdYB4oO/D54KSDPYcMh54dzj98v21M280jyiOXjmYc7TiWfOzk8cTjR07EnTh4MvrkvlORp/aejjjdckZ0prk9rH33b2G/7e4QdTSfDT+755z4XOv5wecPXIi5cPhiwsXjl+SXzlwedvn8lRFXrl3Nudp5TXvt0fWi6y9ulN7ovTnjFuVWxW3729V33O7U/x74+/ZOUef+uwl32+8Nv3fzPn7/6b9M//rQVfaA/aD6oefDxkehj/Y9Tnx87snIJ11PDU97n5X/4fDHqucBz3/5M/bP9m5ld9cL44vPfy146fJy06tBr9p60nvuvC5+3fum4q3L283vIt6deJ/9/mGv9QPtQ83HwI+tn5I/3fpc/PnzvwEDmPP8DKo7+QAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABcRAAAXEQHKJvM/AABDh0lEQVR4Ae3BB4BeVZ3w4d//nHvvW+adnpl0khASQhIgIB0VaRYQQQHFBitYEbF+a8GOuu7qqiiwigpIEbAiCCK9KIhAaElI731mMvWt957z/3InOyEhCQlNv283zyO6Ebvt9j+EYbfd/gcx7Lbb/yABu+32YngF78EaEAHnGGQtqIL3oArGgDH8o4huxG677SrvQRWsZZAqeA/WMsgrGGEr3oMqWMsrLWC33XaFKngPxoAxxM8soPqXRzDDmpEgIH5yNrVHniRZvBzJ5wj23INwv33Ivfl4wn2nMMh5EMAYXimiG7Hbbs/He1AFa4mfnEPvV/8TKdQRTptMPGsulZvvIOlbCWIRDQBFiQFFTD251x1F/ac+SPaEYxjkFazhlSC6EbvttiPegwg4T89nvkb/j35G0398Gcnn6f3Kf5B0LMVIAWwGUPAKAogAAs6h9KPqyJ94Ks0/vJBgzz3AObCWl5voRuy22/Y4D9bgVq+n483vIJ43l/Zb/0Dxl7+l/7IfIKYJY7Koc6DKDlmLiOCTHkxDG203XkXm6CPAObCWl5PoRrwUzoEqLysRsBZUwTl2iQhYy3YlDlCen0Bg2YYqOMcuMQaMYVDiAOV5GQPGsJn34D07ZSwYYStewTt2ibUgwvPyHozBLVvFuqPeTLJuBSP/9hd6v/VdBn51BUE4Fk0SUGWHjAFjwHvUV4AELwOghvZf/578aSdA4iCwvFwCXiprecWIQBDwoqmCKgSWXeI9iIAIm4lAEPCCBZadUmUrxoAxvGDegwgEAbvEewaJsF2qYAy+p4/1b3oHtWVPM+rhR+m/9KcM/OpygmgcWquxXSJgLTiH+j7UVxDqCEaMJzxgKtFB+2NHj0QyIb5zA2ZYC6iCCC8H0Y14MVRBhL5vX0wydyFSlwcEUF4cARQtlggmT6ThCx8jWbKcvgt/gEQhGAvCcwigaKlMMGEPGr78STZTBVUQoe/bl5AsWorksuA9WzEGLVcIJo6n4XMfBVUQAVUwhvjJOfR/7zIknwVj2JYAivb2k33j68i/51S0v0jvl7+D7x9AoghEAGUzMWh/P4WPvo/okBlokiBBQOma31G57R6ksR4QQNmKgvb1U/eh95B5zaHgPaiCtcTzFjHwnz+BMGC7RCBJkLo8DV/6BKapEbwHY9iG92AMXWd9jL6rLqbli98mGD+Wjvf/C0E4Co1jtiECxoKv4rUHMQ1kDj2c3InHEIwfC8aQLF2BW72OZMFiajNnYerraH/oZmz7MFAFEV6qgJeo9PtfU/n7/WAj1NV4KcRG4GpkDno1DV/4GG7dWvquuAiRHF7LgOe5xITgE6LpB9Hw5U8yyHtQwBq6P/oFei/9TzAO9Y7tEWPBW9zqtTT/6BvgHKqKGEO8ZAm9V12EsQW8KwLKc5kgj09KaN6Tf8+p+EqZ/l/8FN/diYpHNWFLYgPUJSSda2m/5QbwnlTlgXvovfZiTJDHJyWeywQFfDJA9NoDyLzmULQWI9kMyZIVdJzwTmqLZ6LGg1e2yxjEC7UnZ9F289VILgvegzFs5hWMIVm+guI1N5AZewC5t53I+uNPxtKCJglbEQFjwVfxroOgeTyFd72f6MiDII6pPfQYxSt+RbJoIZ4+QIEQYwskHfPp/eK3aLnse+A9WMtLFfASmbAZoZ780ScQ7rsPWi6DCFsTEJ6lylZUkVyWZO5CSn+6GRs0kxIJsbQhQZbC207AtreiiQMBVJEoIlm0lNIfb8KGzQxSZZA1dH/o/9B32aUY6smfdBLB+D3QahVEGKSKRBHJ8pWU/nATfRd/D1Caf/RNqNVIiY2wNGPzbeROfzOSy4Iqg7wi2QzVex+i8sR9mKCBlGAw4TCUGvnjjyfcZxJarYEIElhcTw+lX99E5dbbKf3+FvJvPZGUBPUYMmSnv5rM6w5HK1UwAqqItZRuup14+WzE5khJNkOyZAUdx51KvHgWYdsk8qe9FfDgFYRNVCEM0b4ixV/9lvJdf6TjpPfSdvPVSC4LXsEIg4ygA0W0v4TJ15F/7+mUbvg9rmsVNhiOJgmDjICx4Kp414lt3IPGsz5I9k1HU3vsKXo/cyHx6nkgDtE8IjmMbQcUvIIqhlaKV99A/fkfJJw+BbwHY3gpAl4qBaWf/BknU3fOOyFJIAh4QRIHgaV0w00M/Ok6VJVNFCWBJKHhix8nnL43eA/GgHNgLZVb76b4x+tRVQaJoM7Rddr7Kf7+V4Cl/rwP0fyjb4IqiLAVVRCh+7wL6LvkR/RdfBFSyNP0bxcwRKlBJqLpu1/GNDeCKohAkkAQ0P3RL1B64naepeA9SpG6972D/BknQ+IgsKAKIviBPoq/v4a+b19E7sTXI1FIylMlOuIgmr7/VUgSCAJQBRFqM2dRW/4IeE8qmbeY9Se8g3jxHGxdO63X/YzssUeC92AMW/EejCE67EA2nPtJynfdSsdJ76HtpmuQfBZUwXuwlvItd1B97HHqP3kuwV7j6P38NzG0ggBBAM6jvoT6IrZ+LI1nvZ/c20+ictu9dJ7+fpKBlRhpwNgWEMB5UIUkYUsSBLhKJwOX/YLmH/4bqPJSBbxkSkqdI+X7BiBJwBgGeY/kc0guS0orVbRYAmMAAe/AWkxrMzjHtoSU7+pGK1UwBjGKJgk4j+/uBQxb6jzlfZRu+T1CQP35H6P5ogtBFa1W0b4BsBYUUA9BgGlqoPnibyJG6P3RD+n99rcxTU00fPaj4D0goIrv3IBEIZKJQAStVJGMopUq2xABBN/bD3GCJgkCqHNIJqLpGxdQvfMBqn9/iOI1v6Zw9rvAewZVqhAnaK2GGIPvGwABdQmDMhm0UmX98W8jXrEAW2in7aZryRx9BKlkwRLc+k4kDElpLSbYcw/smJEUPnImqQ3nfpryXbfS8dazaL/1erACqqTKt9xB8epfM2rhI9QenUl19VMYm4PYg4BoHeHEKdSddTrRkQdR+dM9dLzlTFzPMow0EQQjUOfAOZ6PeoeQp3zT7TT9+1eQXBZUQYQXy/BycY5U75e+w6oRM1gz+dWsmfJaVg2fQf8lV4K1YC2l39zCquEzWDPltayZdASrRh5Az2e+TkqdYyuqQA0srD/27XSedBYShRBYtFJl7T6vZcMH/w8QQVIl5bu7qT38N0Bo+Pj5NF90IXiPlsp0nnIOq8cezJo9D2fNnoexZq8jWTP+UIpXXE+q6YffoP5970epUrz2lzzLowMDrNn7NfR88qsQBGAttZlPs7JxCuVf34wQokmNIZpUEHL0fOLLrJnyGrRag8CCCBrHhFMnU/jwmSgl+r/9Q1JKjVTxht+zavj+JIuWgjH0fuU7rGqeSjxrLmCQKCCeO5faimcICmNou+U6MkcfQapy5wOsPeAY1r32Daw94ljWHnEc6446nnWHn0g8ax6pwkfOpPm7FyJkqd7/APGSxQwKArRWo/b3J/B0ULz6OqKDDqTtx1fR9JkLaPzXL9H8je8w7HfX0Pj1T5MsWkbnqefQ+91vor29GDscCNAkAVV2yitisrgVa4jnLmCQKi+F4SUTBhlDSislnK5HvUdLRRzr0XKZzaoVHOvR4gAp7zvRSpmUWMtWAoupa8PkC3i3Ad/fy2aq+O4ufKkPGzZjGpoZJBaxdSg1wmlTGGQM3ed/geKfrwcDqooC6hJc31q6P/8V/IYeUpnDDwISxNYxKAqxURsSRXjtwBcHGKJxgiuvR2s1LG2YfIFBItimVky+CV/px3V3gXekStfdiPYPkKr/5LmEY6dTXfAYxSuvJ5q+P9m9XwNBgOteizpPSktFnK5DAotlOBJFiAkRDHb0cDKHv4qUFstseN+5+HIftmkUtmU0tmUUtnE0tZVz6P7k5xmSec1hgCBkEQkYkixeSrJkBUIBO3w4G97/CUq/vBGCAAks8byF9P7rN+l89/vp/8WP0d5ejB0OBOASUGWXGIPYAJUqiV9D8swiBnnlpTC8VEYQctT+NpPyH+/ELVmJkKH9nt+Qf/dpKDEShmwWhEBM9oTjGbn4b9jsSJJFyynfdDvVBx5GyCFBQCqcNoUx/Qto/Ma/opSQIMNWTAQkDPvTNbTf9xsGqYL3DPKeIb67D/Dk330qY3rnMnr9k7Td/AvAIj4C50ipKikRIZV93RGMqSwh//a34KkhYcQQMQbBIHUFRi56iMb/uICUaWpkxLwHGHbTlYBDTAReSVVuv5PiFTeQsiPbafz8JxEMvRd8i9wpb2Dk3PsJ994LcIg1DAoilIT86W9hTLyc3NtOwHf3Agb1HnWelFYraMUDQutvL2PUikcZvf4pGi/8P0AMlYTNvGeQV0AZon0DUKsh5JB8HcnSlZTuv5meb32Fnm99hYGrfkq88BnE1GGCNiAAl4AqOyUCgQVjUB3AubWEe+zJsO//lHC/KQyyhpci4CXytQ0oZfqu/i96r/wBIhmghuRDJJNhkPIsBQUkDJD6PBqVKT/yZ0qn/AkxEVDDxxsYFFgQQfI5QEGVrSgbKaahAEHANoxhiJgABSSKQATJZpG6OsCgfQOsP/p0JBPhOjdgaEC9JyVhACKQiRikbEFRFARMcyMSRaAKIkhgMQ0FQEEVVElJmKP36/9B4UNnIoU8dWedwcBlV1F54h76L/05Td+8AHUOULYRRUgQMEiVlIggxjBIDKgAHlNfQHJZUpLPAR7Uspkx7IijC2vaMI2NUImxtCJBhCqgCt6D9+B5fiJgBMSA86iW0aSIkCGacSh1Z51GdPAM+v/9EuLZc2j56ffAeQgsL1bAS5R/6+lE+xyA1OVBBJxHSyVsWztaq7FDziNBSP1Z70crVYhCUEWLJYIpe7EVVbZPGaTKTqmyibKZOpQqPobq7L8DipgIJUF9la0oz0+VbajyXJLLE/ctof9Hl9Hw+U8g+SyNF36O6kmPMXDJ5dR/5H2YfB5F2ZayPQoI/02VQapspsogVXYm3Hcfmj71JfouugRNSqgmgAcE1AMKImwmAsImIoCAKrgE1RrqKkCMUEc4cQrZY48k8/qjkLoc5WtvpOcLXycpr6TxQ59hE+WlCHixREg1fO48tuEcWAvVGjui3pNq+v7XwBq2R0QYJML2CYNE2HXCIK+E0/Zh5EP3gjFghEEKJAlSqGMrwvMTYRsigLIV7zBk6fvOxeRPO5lg0gRyb349+Te+iYHbbqD3G9+DKAIUlOcQdpkIm4mwq+Kn5yL1dQy/92ZMSxN2WDvJuqWQ9IEKYAFBCEipOsADCjg2MRjbTDByHOG0yUSHvYpoxnSksUDtwUfp+/pFVJ/6O1DDhMMw1GNHjeDlEPBSOQeqbEkTh1jLrlCXIGrYighYyyvKO6SQJzrsQP6hVIEI391F37cvouXnPyDVeOHnqdx+H6XrfofYDIY8qso/mmlvpuebXye8fBKt1/+Y1hv+i2TRMuJnFpDMX4xbvRatVPEdXaBgmhqQxnowlmDCWOywFuzE8dhhzWic4JavojbzaXqvv4l40XxcsgFDHmPqQQRUACWYNIGUKggvXsBLZS3PJSLsKrEBWMM/hSp4z3aJgDG8MhIkamLg8l9Sd+Y7yBx1ONFB+1P3gXfT/5NLMUELYEGVfyhVgvHjye53GKWZt1O56058R4nKLXcQvfoQbNswwimTkNYmtKcPVJH6OnCeZMES1Htqjz2Fv+dBkvmLcGvX4LUPUIQ8IllsMAK8B+9BAAUTNBIduC8psYaUqiIivFAB/5uJgDH8I6n3CBmyx72O0q2/o/eb/0n7Ub8h1fC5j1P6zU1oVz9g+EfTJEHCkMzRh1GeeTu1vz5F/l1vofdHF1JbNhtcDTCAAJZNPOABBRQIAIOQQSTCBMNAAe9BFZKEzcSgWiKaPBWZMI5Zs+dQXygwatRIwjAkpaqICLvK8L+dCIiACIiACIiACK8EMQalRP2nPkI0/WDKd/yR0m9uJhWMH0vD5z6Opxcxln80ESGVf+fbMLRRueevmIYGwpH7YlwBGw7Hhu3YzHBs1IoJmjFBCyYYhgnaMEE7xjZjTCMiGVAgceAcqPJcYgyeMoV3nkJ/YFm+ZCnPzJ3HXx/8GwsWLqJaqyEiqCq7yvC/VRAQz57HugPfwPrDT2L9kaew/tWnsP7Vp7B2/+Ppevd5vCJEUKoE48ZSf/4HUKr0fvXf0WKJVOGDZxFO2hfvi4gx/EMZA6pEr9qf3BuPJqktp/rgI+TfdQqODtR7fDyAq67F1dajSRG8ggLOQeLAefAeVHleImhSI2gaQd3Z72bRrDmItWQyGZIkYeHCRTz00MOsWbMWEUFV2RUBrwRVNlF2TtkpVbZPGaTKLlNliO/vo/L4A4hk8NqLohiTBa+oFtmK8vxU2YYqIGzLon19FD7wXgZ++DMqs/7CwE+uov5TH8Y0FGj66mfpePeZIMKLI+A9m3lll6mCCI1f+Syl224lmbOQlst/QHTADOyIdvyadbg166k9PovqXx4hWTEPJcbQCEEE3oFXdkZsgEu6aPrYJ5BRI6jv6aFUqKNUKmOMIZPJEMcxjz/xJL19fUzZezK7IuCVIMIgEXZO2CkRtk8YJMJOiTBIhCFiQ4Q8tmEYzV/7DnbkcCp/upv+Ky/DhA1sRXh+ImxDBFC2RxNHquELn6D6rkfo+/ZF5E59M8G4MeTPeCvZi1+D7+tlZ1TZmgigYAybGWGQEXbKGFAlOvQAht97B/HjTzPw06sJp0ym881nYseMIhg3mujQA8ifdiKaOMo33kb5pttwxVUIecQUwADOgyrPJUGISzaQPeJoGr/8GVJ7T53CXs6xvqODRYuW0N/fTxRFZLMZFi9eCqpMmbI3O2N4BWipDIlDazHbI2wUJ2ithpbK7Ig6BwpaKgMCImxFGKR9A5AkPJd6zxB1MYLBdXQSz5lP/MRs4tnzGRRa8u86hfzbTyJzxEEoZVAlpXECqlCtsS0BBFTx3b1oLQYRUIU4wfcNAAIibE3AGlL5t59M7qjjiTvm0v+DHzPICE3f/BKSy7OJsCMigPcMUg84ICCeM5/4yTnEzywgWboCCEFrbOYcOyQC3pM96jAqd91N5xc/iSYJ+bNOobLwr5TvvYueb32VztP/hf7/uJRw6iRaf3UZLd/5AdE+++N9N951oFoBEQgsBBYCi4QRPukjGD6a1qv+CwIL3qOqWGsZOWIERxx+KJMnTyJJErxXstkMi5csZfXqNaRUlR0JeDklDgJL75e/Q/9FP0fq6zDkUOfYzDmEHJV7H2Rldk/qP34OTd//GiQOAssg78EY4lnzWH/UqYi1GOrRJGEr3iNk6HjTe4gOmUH7X25kkDGkxBiGmPYWwFD+/a2UfvM7UmICwEFWELEMEmGQMqhy74N0nXI2ZDIY8pA4NlNFsGixxJqJh1H/8Q/Q9IOv4Xt6WXfIibh1HQgZ8J7NVBFCNrz3Y6hT2v50DY3f+iKVI+9j4NIrqHvXaUQHzyB69SGgSkqMsC0BDKoKIqQkX4c01aFdq+g6+1zwCYNsBlDMiHY2s5ZBRkDZljGgyrBfXYmeVGP9GW9nxO13oMUq/df8hCC7BxrHVB97iMpj92Drx5I//S3Uf/ZcGsKA6n0PUbntPuLlS9BkAFAQC5oQtE2g7ebrCSaOA+fAWoRNVBURYeKeE8jncjw9azYiQhAELFm6jBEjhmOMYUcCXlZKSktFnHYQ1EIGqfIsZVBcw2kHWiqyibKZsolL8MUOJCgAAqpsTUn5uAff280Qn/QjZCjfdg/5f3k7EgY0f+9CkkUrqNx1J2LqQBW8w+RbaL3iUsywZrRYonTdHxCy+KSXQbUYV+vAMoyU4hmiKKCgiqcLXxpgkCqupwtf6kUwgDJEUVLxwvn4uA+3fh2ZIw4md8KbKN56Pb1f/XfabrkOMYJ6dkg1BjzJgqUUr7iewofPRLIRLT/7IV2nfRC8B2NABK1Uyex9IM3f+wZDSr+9BVCUMhi2JQKqSC5D283X0nXW+ax/0ym0/uRiTFMLfRdfhEiECZtI6cAA/ZdfysDlPydzwGHkTj+Rxm9/HozB9/aTzF+EL5bQ/iINn/8Y4bTJ4BxYy5ZEhJT3npEjR1AqlViwcBFRFDEwMMCG7m6GtbaiqogI29CXUxxrasNHPq9LQNcf9w6tzZ6nyfJVqs6pOqfJ2vVae3K2rj3g9boEoxs+8jkdFMe6WeI0VX14pi5jmC43o7V4/Y0aL1mhvharJon6YknjRUu1/wc/06XkdM2M43VIzxe+pUup16XktfOdH9YhrqdPq48+qdXHn9bq47O0+sgTGi9aqik/UNJ1x5yqS6nTZTRq79e/r6nSjbfpUup1ZfNULd/1F02WrVRNElXn1HVu0Hj+Yu18x4d1MeiGj3xOU259p65s21eXUtDeb/1Q48XL1A8UVb3XZMVqLd/1F13ZPFWX0aLVvz2mqerfH9fl0RhdRpOW/nCbpvxAUVNdH/6cLgbdcO7ndZBz6itVXXfcabqUrC637Vr+4506yDl1vX3qOjeo6+pW19WtrnOD+nJF1XtNdX/ma7qUel1KXrvOOl8Hea/b5b1qnGhq4PLrdGX9ZO364P/R3q98V1dNOEyX0aTLpV2X2zG6IjtBV4TjdBmtupS8LqNNV7Xvp2v2P067PvSvGi9doYOcU3VOn4/3XlPFYknvvOsevfOue/TWP92ui5cs1ZT3XrfH8ApRwDQ3Ek6djB01HIwBY7Dtwwj3m4rkcoBn5xQUwmlTCMaPQawBa5FMRLDnOILJEwDHZqo0fvPzNJz/cYSAgeuupfvDnyVlGgpEr9qPaMZ0ohnTiF61H8Ge49BimY43v5vy3bchRLRc9J80fOkTDDIG8GAN0QHTsXuMBmPAGExjPcGkCZjWZrahCjiCCXsQTNgDyWRI2TEjiQ6YDsJGDowhFR08g7ozT8fTS+/X/gMSh2QzpMQImwgpdR7JRLT94Spyx5yId310nP4+Knc8AMZgCnWY1mZMSxOmpQnT2oxkIhCh++Nfove73wE8DR8+j5YrLwJVEGG7RMAaSBx17zuDkasew7Y2U5v5NHXvfiv5U96GaWhBXQlXWYePe1BqgAXxaKVKOG1vCh94N3bUCEgSBhnD8xERUmEYYK1FVRGBSqXC8wl4JQgYctSeeobuj30RkgSMYZAqiCFZsQohDwg7JIIQIEFA34U/wLS3gnMgAqoQBrhFyxHyiAiDRCBJaL7oQsQaer//Hfp/8nPcmnUEE8ah5QoYYZBXJJel+rdHqTz8AEJEyyXfp3DuWWilimQzpIQIqjV6Pv01JJdlM++RTET1wUcxZHmWgDEIdRSvuIHaQ4+htRiMkNJyBa3FQIYtNXz2Y5R+dSPxzFl0nvEh7KhRiDVUH3oMQ4YhIgLeI/kcbX+4io43vZPyX/5M5zvfTzR1OiQxIDxLwVq0WKL65OOAo+HcT9B8ybfAeTDC8xKBwIJzmEIdjd/6PL5vgGTOfKLDXkXdWafj1nYQPzmHeNZcgvFjCSbvSebVhxAdvD9SqOOFUgURqNZqJEmCiKAKdfk8zyfgFaBJP54ytUWPUZn3ANtjggKeEur62ZYySGMcnUiSpe9XlwGe5xITAgku7mYza8E5mr73NTR29F78HQb+eA3qHdsjEmDI03LpRRQ+8l6IEyQISKmr4ehGSzG9V/wAUJ7LBHk8FXzSR0rx+LgTpY/iHb9Gb0/YmmAkj1JBNSGltRrBXhOoP//9bPjGBQzceBXqYlImyOOpokkfmygYC94jhTzD/ngNHcecSnnmnSR/XQBe2S5jEW9pOPfTNF/yLXAOjAERdom1oAqJw9TXER12ILskcWAEjGHXKSB0dXWRJAlRFGGMUKgv8HwCXk7GkMoe/wbEFpD6AoiwXapo/wCZY45kkDFsZgwpO2IUTR//AgQhWAMibI+WygTjRrOZCBgDqjRf9HWCPcfhlq9CshlUlS2JEXyxTPY1h5A77c2QOAgD8J5UOGkvms67AMllwQggbEvRvgEyRx9JyuTzNHz04+hACaIQRNiagvNoHGNHjCQlNiBVOPcctJRAYEGETRTtGyBzzJEMMoZBxoD3mMZ6Wv9wJZU/3omEEaBsS1CXYAp15E4/CVTBGBDhBRGBwIIC3oMqGAOq4D0YA14BBREwBgLLCyUi1Go1li1bjrWWJEmor6+nuamJlIiwPaIb8RIlXtmSiCDCLlEFA4jwgiigbM17ZUhgBFRBFYxhV7jEocawJREQEbYkgLB9iVdSxgjPR9jEeWUrqhhreC4BhE28gldlM69gwBjDrvCJAxEQIWWNIIDzivLys0YQdp2qIiI8PWs2q1atJooiqtUqU/eZwrhxe6CqiAjbE/AyCIzwogmogiqIsDVVcI7nUjYyBmMMWzJGGKKAiIAIJA5QdkQBj2ADy65QVVzisMLWjCEwhl3iPanAGLYmbJfzDLIGI2BE2MwIL4QJLNtjjfDPpqqICEuXLWPlqtVkoogkSWhsbGTMmNGkRIQdCXiRFBCgVHNc8OfF9FcTImsQYZcIkHglFxouOGYCw+pCvCpGhM1EIAjYklfFiHDr3C5++cRamnMBXhkkQNV5CpHlG2+YSF1kUUACy/NxXgmMcNXMtdy9cAP1GYtXtmEEBqqO975qBMdMbCHxSmAEVRCBnkrCl/68gEriiaxhewToKsd84ODRpH72yCqaciHC9ikwUE0474ixHDK2gdQDS3r4ycOraMgGCC+OAs4rqvCl48YztjHLJQ+u5OEVvdRnAl4Ozis1p3zp2PFMaMnhFYywQ6qKiLBi5Srmzp1PFIZ470lNnToFay2qioiwIwEvlgIC1UT5xV9W0F2sIomisQMRdkoVExh8YLh/cQ/3fPhVNGQsXhUjwo4IQuq7dy3hnifXYQ242IMIoEgmQEV4+37tHD6uCa+KFWFHYq+ERrj0oZV89IY5SCVBYwciPJcAGhhumLmWm99/AMfu1UziFStCqlh1XP7ACkqVBKk51HkQYTNVgkxAUk44bFQDoFx3+2KC+oikGIMIW1ElyIUk1YTjJ7ZwyNgGUs+s7ufaOxcT5EOSYgwivCCqYA1kLXj4yGGjGduY5dan13PrY6uxgcFVHYjwoglILkBjz4cPGcWElhyqCiI8l6qSEhGWLV/BM8/MJQgCUkmSMH3aVJoaG1FVRITnE/ASiUBYCCFxvG6/VvYbWaCaeIwIKa8KCgibeYVcYFjYVeKWWR3MXNLDm3/+BLe+fwaFyOJVMSI8l1cwAsu7KzyyZgCaspy6XzsjGjLUnCeyhlvmdLJoZR+3PNPF4eOaUAWE7Uq8Ehrh0odW8tFfzQFVDtu3ncPGNVKqOYwIQxQlG1junN/FrGW9nPzzx/nDOTM4dq8WEq8EIohAWBeCgVMPG82ohgyxU0TAq5INDLPXFblj5lrCyKKqkA8ZObKek6cOI/GKESHlVQmMcPv8Dcxf2UdDPmCIjSzkQkaNrOctU4fhPIiwmSo7pCiZwLC6t8pvZq3HiGCMkIpyIWQCjtl/ONNHFCjHDkF4LhE2U2UrihIYoRJ7rn9yHQPlBDHCIGEbqoqIoKo8M3ceS5cuI4oiUtVajUl7TWTMmNGoKiLCzgS8RAqoApWEd84YwQcOGUXilcAIz0cBAT7y27n8+L5lPDC/i5OveIKbzz6AfGhQQNiaV8WIcPv8Lga6y0wY08DPT59KIWOJnRJaoTUX8pWlPdw2t4sL3ziRwAgKCFvzqgRG+NGDKzn/13PAKcdOb+Om9+1PPrSoggibeQUjsLxnLMf++DEWru7n5Muf5A/nzODYic0McaoQe/71qHEcPLYBp4oVIfFKYITLH1nDHQ+uZLOaY2xjhh+dvDeqIMIg5xVrhNOvmcX8Rd2ExrCVmmNMY4Yfnbw3qiDCLnGqWBH+vqKPXz++FhcYUJ5VSThj/+GcffAoEq8ERnghVEEEussJv5vdAc6zPapKSkQol8s8+dQsunt6iKKIVLVaY/Lkvdhr4p6oKiLCrgh4uRhhbX+V9QM1ChkLGARY2FVmoOqwRkgJEHvPmMYsI+ojLn7bFGrOc/lfV3D37E7ecvkT3Pr+GUTWoIDwLCMM+v2sDkg8J0wZRiFjGag5vCqhDThuUjNfqQt5bHUfM1f08aqxDagqIsIQp4oV4T/uW8Znfz8PVDl2ehs3vW8G+dCQeKWnnLAlARpzAXs0Zbntgwdy/I9nsmRNP6de/gSPffowJrbk8AqCgAjd5YRy7BEBNRA7RYCBagJWMAKeTRKv9Fcd3eWY4YWI0Bp6KwlLuit0lWpgBVVlKyI4r/RUEgQoRJZU1XkGqg4RQdiaVyUwQks+pOo821IQcKqkBmqOWuIxIqS8KqEV6jMBAsRe6a8kIIIAykaqDKuLcF7ZEVVFREitWbuWZ+bOI67FZKII5xzOOaZP24c99hiLqiIi7KqAl0GSeIgsX759MRc/uJK5nzmcfCikzrxhNn9f2I2NLM4rRsA7ZeKIAne8fwYTWnJcdto+9Nccv35yHXfN6eDxlf0cOq4Rr4oVIeUVjAhLNpS5f0kPZANeN7GZ1PVPrGNFT4WvvX5P9htVz+Thdcxf0sPdi7p51dgGvIIRNjMIqRsfXwuJ44hp7dz0vv3JhYbUZ29dyCUPrSQTGFQVESF2nu+eOIlzDx/DxJYcD3z0IF7z48dYsrqfzv4aE1tyqCo+8WDgrVc9xfD6iMfOP4TmXEA18ez/g7+zqKsMTqlUEtQIhJbZ64o0fPEevnjcBC58w0Rir7zzl7O5/cm1hLkArCHxyhDnFIwwa22Rlq/cx8dfvQffP2kSqVtnd3HGtbNozgdUE4+ySTYwdBZjPnbkWH74lslYEXYk8UrqikdW8+k/LqA5F5LqryacMGUYN561H6kn1gzw2v96jGxoSJziVWmtC3nyE4cSGEFV2YqCoogI1WqV+QsWsmrVaqy1hGFIHMcEQcB++01neHs7qoqI8EIYXiIBmupC8vkQnBInni0Jm+Szlpb6iKa6iHxdyKJlPXzkN3NJWSN88dgJYARCizHCc3lVUvct6magp8yYtjqOnthM6san1/ObJ9aSKkSWN0xqBa/csWADKSvC9gTZAGLPq8c3kg8tAlz3xFq+d8sC1HlUlZRXpVx1nP+7ufxuVgczV/VTih2Th+VBIbCGlBGhuRBSyIWUyjE9xRqqSkqBroEaceJ529HjOXBcI3s0ZyEXUOyt8oFDR3PhGyaSOv/Gedz++BryhYim+gxhXUgUGIZkQoM0RFgD2l+jVHUMqSYeN1CjpxijCgII0FNOIPHcuWADn711Id+7fzkSGqwREP6bgIIVIeW8ov01as5Tjh3xQI3+SsKQxHlKAzV6ijUUZaCc0FOMsSIERhii/DcBEWH1mjU8+NDDrFy1miiKMMZQrVZpbGzgsEMPZnh7O6qKiPBCBbxIIgwqZCzzPnM49y/p5rhLHsUYYUuBEagkXHjaPnzosNGkPvL7eVx59xIGYscQ5b+pomzLCIN+N6sDap43TG6htS5kTV+Vh5b3sqEUM2vNANNHFjhpWhs/un8Zf1naw+KuMnu25vAKRtiK80rKezYrVR0oHDC2kdvPmYFTpasYc/B3H6JnoMZplz6KklIktOCVxHlSw+pCFn/2CO5f0sNxlzyKsQYRBgmgImQjyxWn70NDNuC0a56GYsxJh4zmstP2IfWvtyzkx3cvIayL+OV79+VNe7fSV0lozAYMOX2/dt59wAjOvGE2192zjCAQhhgjINBUF/LweQfTkg+wxvC6HzzMzEXdzFvUzTNzOsAayFqcB++VzSLLQ8t6GVGfYebqfvDKj982hSdW9/PdG+cRBoYhRgQEGvMRcz59GG+96ikeXtHLNY+vxauSeAVhs/6+fhYsWEhnZxdBYMlEEUmSoKpMnLgne03cE2MMqoqI8GIEvEQiEBihIROQUrambKRKIWPJBoZULjDgFWOFISLskFcwIiztLnPPog2QDzll33ZSf1vWy4b+GnjPvYu6mT6ywEFj6hk7LM+K1f38eV4XHzliDF4VI8L2iLCZGAFVrEBDNiAVWcMpB46kr+oIjYBAOfb0VxMSrzTmQlLGCKERGjIBKQWUZ3lA2eSdv5zFb+9eyvGHjea6904n9W/3LOU7ty2EyPKLd03n5KlteFWG1UVsKWMN1giRNaDKVhRQBjXnQpqyAakzjxjL4Xu3kgsM5cTjPThVVKG1LiSVVBPwytUPLOfKOxcTZANwnowImcCAgrI9SmM2wDqP9lY57xdP4pxHChlQQ7VSZdnC+TyzeAWCkslEeO+pVqvU19czbeo+NDc3oaqoKiLCixXwMlG2TwQILX9Z2kvGGlLzOkoQWZKqY4gqmwiIsBWvihHhT3M6GeiuMGZkgSPGNZK6cXYHJB4EbluwgfNePZbmfMjr9mzm6uW93Dq3k48cMQYj7JAqmzmnEFpW91X5wm2LSLwiwPiWHAIYEQZqjrdOa+PQPRrwCkYYZIRByg4oZKzw8Zvmc/19yzjqoJH87l/2py60XPrQSr5w03wIDJecPpV3zhhO4hUrgiqIsJmyiSrbEsAIXmHmqn6acwGxU964dysnyjCMwPiWLEaEIV6V1JRR9Szur9GcC0gJsLa/RltDhnhNPwg75FSZOrqBnsTTko+wQUDfQJHegRILnplNkouxNsQaoVarYa1l0qS9mDB+HNZaVBUR4aUKeJkI2xdXHKhy9X3LuPKOxaRsLoDYc+AejQxxXhnkFHXKIAUEjDDo9gUbIPEcPbGFlnxIVzHm5jmdjG7LUx9Zbp/XyYqeCmObspw8rY2rH1zBY6v66SrGtNaFqIIIm1kjpIywWUMugMSzYlU//7awm81USQmgoeWi+5bxh3Nm8PpJLcReCY0wRNiWAqEVOvpjrrx/ORPHN/GLd02nEFk6izFfv2UhxJ5PvXEi5x4+htgpoRW2R9gx7xWcp3egxnEXP8IQUUUVmgsR875wJG2FiNgpImCNkPrOiXvxnRP3YkuJVwIj3PDkWhBhe0SEOPFcduoUlI28Y82qVaxYWaRcFjyKSoQmCbXE097exqS99qK+voCqoqqICC+HgFfYtDH1lICGjCVlRCjWHDNGFfj3E/ciVY49n/3TQqgmNNVnaKuPSIkIXsGIsKKnwr2LeyATcPL0NlJ3L9xA9+p+PvnOaYxuyHDOj2dyz8JuzjxoJK/Zs4mR7XWsWV/k/sXdvHXfdrwqVoQhSSWBwPDXZb2UY08uNLx1ehsXnj6VeR0lCpHF898UEMiHhluf6WD+yj7edvkT/OGcAzh2r2a8Ks9LwRqBmmPi6Hru+PCBjGvKEjtlWF3Iqye38Nu/rqCvkpAywouSDy11LTmacyFelZQIxE5Z31dFMpbAGlKBEUTYTJVteGWXiBFcErN0xSpWrlzFwECRMAgw1uCdx7kqTU2N7LnnBIa3t5NSVUSEl1PAK0gVLj99KjvilUFv/tnj3D2nA7IBV54xjQktOZxXrBESrxgR/jy3i56uEnuMqufoic2kbpvXBRmLiFCKPeQDbnqmgzMPGkl7IeK1E5q4YUUfN83p4K37tjPEo1iEsw4fw0PLevnr3E5OuvwJbjp7BvnQcMGx4xG25RWMwPlHjuG4n8xk8ZoBTv7Z4/zpwwfymvFNxE6xlq0ImxgjJLEjkwn4zVn7M6E5x/1LehhRHzF5WJ5zjxjLbx9fyy8fW8NnjhrH3m15vIIRdlnilZOnDaPrK69FhEHOQy40PLislyN/8DDPR4RtGGGHlP+myuLFSyl1raV3oEQUBuSyGZxzxLUahUId48ZNZszoURhjUFVSIsLLLeAVFjtFeQ5VAitsIhyzdyt3L9gATrn+yXWcPK0NawQFjDDod7M6wHkOH9dISz6kqxRzy5PrSH35V3NAFQLD/XM6WT9Qo70QcfTEZm54aCX3LOphoOooZCwKWBG8Kh86dDSJU8779TPcNbuDU658kn85eCT9FYcIm4nAQNVxyNgGjhzfxISWHLd/8ECO+elMli/r5Q9Pruc145vwqoCwJWWT2HlyoeW3Z09jxqgCS7rLnHjZTD565Fi+/eZJHLNXM8dNGcadM9dw2d9W8p8nTUZVQYRdJYDzUEk8IgzyHjKBUHOeV4QISeJYvHgJLTlLLpvBOUe1WiWfzzNpr4mMHTsGay2qiqoiIrxSAl5BInDGL2fx0LJeGrIBKSNQrDlmjCpw7RnTKWQsFxwzHgEuuHk+1/99NZ9+zR4cNLYB55XACKt6qzy0vBdCy9umt5Mq1RwXnLAXgRFEBAG8KolXYqek3jBlGIXmHMs6ijy2so+jJjbjvWKNYESInfLRI8bgUc6/fg53PrqaOx5aCcImIqCKWIOGhrr6DE/+6+FMbMkxsTXHwWMaWL64hzCy7IggpJxXrn7XdI4Y18jS7grH/3gmA8WYXz+9ns8dO4GmXMBZB4/izlnruXbmWj579HjaCxGqIMIusUb43dPrOePap2nOhXhlkAg4rxAYXihVBYTnJRBlIlQTqtUqhbo6Jk+exOhRIwmCgJSqIiK80gJeJFVFRNiZlRtKrFk3wPrA4KqOlA0Ny1f28U4HN5+9P6ljJ7VwgTUg4NnEqwLCHfO76NlQZtTwOo6d1EJqTGOWjx05lh3xqoxvzvLaCU3c+vAq/vhMJ0dNbEZ5VmgFVfjYEWMZlo+4f0kPhcjiVfEK1cQTWKG7lPCbp9dRLMd09teY2JIj5VVBFa/KzjTlAo4Y18j6Yo03/vRxFq3uJ5cPWbxmgOufWMuHDx/D2/Zt47vjm3hy4QaueWwtnzpqD5wqgQi7qpZ4/EBMjwcXO4aIAl7R0JF4TyrxighYIwhbU1VEBBEhpcqOKdRqNdpb6pk0ZgyjRo4gCAJSqoqIICL8IwS8SCLClpTtC0ILXjl2ejvnHjaa1K3zurjsvmU8vrKP3kpCYzbAe2WQKkMEIfWbpzsgdrx+ciutdSHd5Ziv3rGEnkoCyjZa8gFfPX5PGrMBJ00dxq2PruZP87r4txP2IjCCKohAx0CNDeUEI8LrJ7dw4pRheFWcKqEx1GcsIrC4q8yvn1oHIhgrDBGEXWVEKMWed187i3mLupkxpZXzXz2Ws695mh8/tJKzDxlFPrScc+hozl/UzaUPruCcQ0fRmA1QBRF2iREBA435kF+95wCacyGxU5pyAVbAGKE1H5IKrbAlVSUlIogIqkpPTy+lrjWsW98JQQCqbEVBjLDv9OnsvcdwEENKVRERRIR/pIAXSFUREfr6+gnDgGwuR0p4HolnYkuOk6e1keooxlwWe8KMxYiQMiI8V2iFZd0VHljSDZHlDZNbSP11aS8/vHUhJhPgM5YtSTVBY8/JU9t43cRmXjOhiaAxw+zV/Ty8rJcjJzRRdZ5sYPjKHUv4r/uXEWUDapWElA0MzimH7NnEw+cdTKrmFGUjBVV2Stm+M6+bxZ2Pr6N1RIFfnDGN/UYWuOrR1dz7VAd3L+jmjVNaOXXfdr41vI5FK/v47VPrOfuQUSReCYwgwiBlJzwYAweObqA5F5C66C8rWNBZJhcayrHHq+JUUYUvHzeeMY1ZQBCBcrlMR0cna9aupau7l/pASZIARFCeJaQUay3t7W0gBu8VYwQR4Z8h4AVQVUSEYrHIo4/N5IAD9ieXy/FCKf9NQdnEq7I9f1ncQ193heFteY6a2EzqxqfXg8JV79uPd88YQXc5IdWcC/jtrPWc9qNHuG1uJ6+b2MzU4QUOHdvIX59ax1+X9nDkhCaGeFWoOYYNyzN2TD2C0DFQY9HqfoRnhVYQAYStKMr2CM9SlFR/NeFvS3shMvzijKnsN7JA6vPH7cm9szr4/v3LeeOUVkY1ZjjroJH8+80LuPyR1Zx9yChCKyjPEjYRAZRd8suHVvL3BV3YwOKqCVgDWQsK5x0xhjGNnrXrO1m/bh2dnV3UajWMtURhQH0GrGEjZWdEhH+mgF2kqogIpXKZRx+bSbVWIxMGFGPljT+byaq+KgSG5xIBciF3LdzAGdfOIrW4uwwZi6smiDDIGGETIRMYPvaH+dy/pIeeUg2M8LqJzYxsyNBVirlt/gaoj7ju8XVUY8/ZB48i9V9/W8mf522AQsSt87r4+hsnElnDCVOG8de5nVz84Ep+O7uTXGi490MHkg0tVB1nvmok//bGiaQuf3g151zxBIs2VHjNfz1Gqpx4vFdIPFaEIVYEBEQYtKEc857r5rC2vwqhxYogbGJEiDIWIsulf1tJosrYxiw/+dsq8i05bp/Xyb2LunndxGbOPmQ0P/rLSv62vJeDfvgIiVcOHF3P5afvQ+qmOZ186fbF9FRiyIeo8iwBQkM59rzp508QWEGAxeUYmnJMH1XgDZOHsWhDid89vR5rDYsXL6a8pJ+uvhJGIAxDwihDVhJuX5VwzRJD0QGRQYTNlI0CQzn2HHnpoxw+rpGfn7YPqiDCP0XALhIRksTxxBNPUalUCWxAyinMWtFHTzkGI2js2VJcSqAcs2hZD/Pnd5ESayBj+dzr96Q+sqSufHQ1qIJTcJ45q/p5al4nYV0I1YS3Tm8ndd+CDaxa3kvUmuOWv61Eao6zDx5F6oa/r+G+OR0EDRmenr+BBxb3cOykFt40pZULRFi5vsiK9UXqcyEpTTxYYX5HiZTzytsPGM41T4zgnplreXB1P14VYwQv8JkTJ/OqMfUoUKw5FnaVwRpqVUeqlih/W7iBUuzAeRIBVQZ5hXJ/DWLPrX9dwX6teeKxjfzu7qVkRhagr8rF9y7jdRObmdyW5y1Th3H9/ct4uuaoVR0ZVYZ09FeZPa+TbFMWijVcNWFIEnsYqFH1ysPriwwJMgEUaxw0Yzj/fsJEHlzWy28fX0sisHbdelqbDVEUgXriOMYpjGpvIuwLWLK+k6jOQLFGrZwwRL3CQI3YK890l6k3wj9bwC5QVUSEuXPn0dvbSyaToVqrod4jQJgNoFjjKydM4u37tRNZIfFKYISTZgxn0qh68pFFACNCfy3h+EktnPWqkaTOu3EeP753GRhhz/Y8e7bX4a2ANRwxqZVzDh7FMZOaSQ1vzPLDs/bjRw+uZMFAjUw+ZEiuLoTA8OqJzZwxYzjN+ZDUxNYcvzx7f+5Y0M0VD60gyAak2hsyYA2/e2It59VHXHzK3hQiyw3v3pff7D8cAYxA7JTWupBTp7eR6q8mnPDzJ3hiaQ9EloPGN5EyBoJcAF75zHF7st/IAqE1qIIR+OHbp/KHOR1ce+8ygmyACQ3UhWRCw9ffsy9tdSHFmqMusnzlDXtyzN4tXPfEeu55eh1h1jLEWAOBYY+2POe9ZTLThhcYcvAeDXz/rP3JBAYBRBikCuXYMW1EgVTilZQgBIHBOUctceRzWUaNGklb+3DaWpt5MFkNupa2+no+deIkJrbm8ApGYHRTlp+dPYP7Fndz9QMrCLMB/2wBO6GqiAjrOzpYtXo1URSRJAn1hQJ1dXX01DyqgFemtOeZOryOxCuBEVJfPHYC26Nsct6N87jknqVgDXsOy3H7h19FfWSpJh5ix9T2Ot574AgSr6SOGNfIkeMbuf7JdSxIPF6VIZ6Nao592uv40KGjSbySykeWd84YgRHDFfcvwwp0lxMuOHY8SzaU+Pn9y7nknqUocMkpezOsEPKRw0azJa9gBFb2VnnrlU/y6JJuEOHiM6bxrhnDSVkRvCo4z9v3a+fgsQ04VUQgExhO36+d/qrj2tsXA4oq4DwZa/jAIaNpzgWoMmivYXmmtNcxr6PMPY+tRpVnCZB4huVDPnbkWFTZbEp7HVPa69gRp0oqGxhSqooxlpEj26hvbaOttYUwDEm8klI2ckpjLuDjR47FGkHZZER9xDkHjyJ19d1LQfmnC9gJESG1ePEShhhjmD59GhiL+hgRIDDcvbCbxCulmsMaIeW84hVEAAUEnFfqMwH3LNzAFQ+tBCNMHl7Hnz94IOObs6SMEQgtczuKXD1zLeXYYY3gvGKN0FmMITBsSdgoMsztKHL1zLWUYkdgBOeVXGi4f0kPBAYRYciPT90Hr3DFX1Zw6b3LKNccx05qoa+SYERAQBUEiALDD+5fxlNLe8EaLnn7Ppx7+Bhip4RWSBkRCAy/n93B7HVFYu8xIjiv5EPLA0u7ITSgIAIYIfHKlY+upj4TkBKBxCv50DJn3QCEFhGepQrW0Fms8bO/r8J5xRohpQpOlWcJIgxSBQHqMpa564tgDQaYsf9+7D22Ga+KEUFVUVVAEDayht5Kwk8eXkVoBRFBgMQr+dDywJIeCC0I/3QBz0NVERF6enro6+snDAJqccyECeNpamxgEyGpJOCVn929hJ/+2YOwcwomMBAY9p/QxK3nzGBUQ4bYK6ER4koCqtw/q4N7Hl0DwlZsLoTEk9QcQ2qVBLxy/6wO7nl0DQjPUpDQQGBIKglDBPjZ6ftQij03PLyKX9y7jCvuWsL2CKChhUzAz86YyjkHjyLxijVCShWSSgI1x7dvmoc6ZSsKQcaCV5LY452HqqO/r8qnrn0alK0p2IwF74nLCUO8ZyNh8boyH7jyabYhwmbeg/dswwhkA7xCEISkVBVEEBFEGOSdh9ixrrPER698km0o2MiCKnEl4Z8tYBd0bejGOY+1ljAMGTtmNEMCK7xqQhPd5ZhMYBABVXbKiFB1nrZ8yC/eMY1hdSHOK4ERUtPGNFAG6jMWEVBlMwEU6K0k7D2ywJCpo+vpqCY0ZANEQJXNREAVqomnORcSGCElwkbCde+aTiFjmbmqn0LG4ryyJQGMEfoqjvOPHMvZB48k8UpgBFUGRYHh4D2bKdYckRVEQJXNREAQ1g3UGNuao6UuYtLerbTVhWxJVUkZYxARiomy/+h6UEXjMlLtY1SboSVngAwICClBUbzzeO9AhLp8gWw2i6oyRARUIfGKAvnIkBIxDBE2aW/IMGVyKy25gO0RYVBfxTFtTAP/bKIbsQOqiogwa/YcVq5chTGGxsZGDj3kIF5OXhlkhH8KBVQVI8KucqpYEV5pPqnR29NDZ9cG1nV2UyqVMaJkAktKAVXFOYd6JQgsuXyelpZm2tvaaGhsABPwv0XALlBVRISUESGlCiIMUuVFU0AAEbaiyq4REDZRZZeJsJmwkQheFUF4PkpKsSJsjyrbpSiDFBQwAiLClrwqpVKZ3t5euru76ento1KuUItjBAgCi7UGBUo1h/MeEYiiiIamRoYNa6W1tZW6ujzWGIao8rxE2CFlI2WXifBPFbALoihCVbHWUiyVqFSqZLMZVBURQYQXTdg+EV4wEV40AUSEnRFSwo6IMEhV2ZIRYZCwWeISigNFenr76OnpoVgsUSqViOMEEbDWIsaQiUK89zjnSJKEIAior6+jubmZ1pYWGhoayGYzbElVSYkIIrxowkbC/zcCdkFrSwtLly7DGEOlUmHRosVMm7YPKVVFRPjfTFXZkoggImzJe0+5UqG/r5/evj6KxRIDA/1UKlWcc4gxWGMwxhBFId57nHNokmCtJZfL0dTUSHNTE42NDRQKBUSELakqKRFBRPjfKOB5iAiplpZmCoUCxWKRKIpYsXIlxgh77z0ZYwyqSkpE+J9MVdmSiJASEbbkvKdSLjMwUKRULtPX10d//wCVSoUkSVAFYwRrLdZarLU453DOkyQOG1jyuRwNDfXU19fT0txMoVCHtZYtqSopESElIvxvJ7oRz0NVERG6urp49LHHsdZijFCt1mhpbmby3pNobmpiiKqSEhH+f6SqbElE2BFVpVarUalWKZfKDAwM0Nc/QLFYpFqtkiQJqmCMYIzBGENKVfHe470nFYYh+XyeunyepuZGmpqaKNTVYYxhS6rKEBFht22JbsROqCoiwqpVq3l61mystVhrieOYVNuwYYwdO4bm5iaCIGBLqsqWRIR/FlXluUSEnXHO4bynUqlQKpUoDpQYKA5QKpaoVCvEcYL3HgWMCMYYjDGkVBXvPd57VCEILFEUkcvlaGpsoLGxgYaGBrLZLCLCllSVISLCbjsnuhG7QFURETo6Onl61myqtRpRGCIixHGMKuRzWVpaWhg2rJWmpkYymQwiwo6oKq8UEeGFcM6RJAmVapVKuUKpXKZcKlEqlymXKziXEMcJzjlUwRjBGIOIICKkVBXnPeo9KWMMYRiSz+epry/Q0FBPY0MDuVyOIAh4LlVliIiw2wsnuhG7SFURESqVKgsXLWL16jU45wnDAGMM3nuSJCEVRRG5XI58Pkd9fT2FQh25XI5sJoO1FmMM/wjOOVSVOI6J45hqtUatVqNcqVCr1SiXyjjvqVarxHFMkiR471EFETDGICKICCJCSlXx3uO9RxVEwFpLJhORz+cpFAo0NNRTl68jm82QyWTYHlUlJSLs9vIQ3YgXQFUREVK9vX2sXLWK9es7qFQqiAhBEGCMQVVxzqGqeK+IgLWWMAyJoogwDMhms4RhiHolCCy5XA4RYTMBaywIg7z3qFe2pKqUKxWSJCElIsRxTLlcRkSoVqskicN7R5I4VBXvFREGiQgigoggIogIKVVFVfHeo6qogogQBJYwDMnlsuTzeeoLBTLZDHV1deSyWYIgYHtUlSEiwm6vDNGNeIFUlZSIkKpWa6xfv54NG7rp6e2lUqngvccYg4hgjEFESKkqqoqq4lVBle1RZZAIW1EFBITnIYIRISUiiAgiwhBVJeW9J+W9AooqGCMYYwjDkEwmIpfLkclkyGWz1NXVkcvlyGYzBEHAjqgqQ0SE3f5xRDfiRVJVUiLCkCRJ6Ovrp6enh96+PiqVKpVKhThO8N6hCiIgIogIKWMMzyUi7Iiqsj2qiqqSUlVUlSEKWGNIhWGIMYZsNosxQjabJZPJkMvlyGWzZLMZMpkMQRAgIuyIqrIlEWG3fy7RjXgZqCopEWFLqkocx1QqFUrlCpVymXKlQq1Wo1at4bynWq2gCsJGAqqKcx7vParKswRjBGMMxhhEAAVEUFXCMCCKIlSVTCZDJopI1dXVYawhl8sR2IBMJsLagCCw7Iyq8lwiwm7/bxLdiJeZqjJERHg+qopzDlUQYZD3niRJcM6hqjxLMNYQWIu1FhFhiKpircUYwwulqjyXiLDb/39EN+IfQFV5LhHhlaKq7IiIsNv/TKIb8f8gVWVHRITddtuegP9HiQi77fZC/V//0Ho2QJ82fQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNC0wMS0zMVQwOToxNjoyNiswMDowMGY4EaoAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTQtMDEtMzFUMDk6MTY6MjYrMDA6MDAXZakWAAAAH3RFWHRleGlmOnRodW1ibmFpbDpSZXNvbHV0aW9uVW5pdAAyJUBe0wAAAB90RVh0ZXhpZjp0aHVtYm5haWw6WFJlc29sdXRpb24ANzIvMdqHGCwAAAAfdEVYdGV4aWY6dGh1bWJuYWlsOllSZXNvbHV0aW9uADcyLzF074m9AAAAEXRFWHRqcGVnOmNvbG9yc3BhY2UAMix1VZ8AAAAgdEVYdGpwZWc6c2FtcGxpbmctZmFjdG9yADF4MSwxeDEsMXgx6ZX8cAAAAAp0RVh0cmRmOkFsdAAgIM2DzKsAAABGdEVYdHNvZnR3YXJlAEltYWdlTWFnaWNrIDYuOC4zLTYgMjAxMy0wMy0xNSBRMTYgaHR0cDovL3d3dy5pbWFnZW1hZ2ljay5vcmf+g02TAAAAGHRFWHRUaHVtYjo6RG9jdW1lbnQ6OlBhZ2VzADGn/7svAAAAGXRFWHRUaHVtYjo6SW1hZ2U6OmhlaWdodAAxMjk5/3smcgAAABh0RVh0VGh1bWI6OkltYWdlOjpXaWR0aAAyODM1gNX7JQAAABp0RVh0VGh1bWI6Ok1pbWV0eXBlAGltYWdlL2pwZWeLpZ60AAAAF3RFWHRUaHVtYjo6TVRpbWUAMTM5MTE1OTc4NqvhAFQAAAASdEVYdFRodW1iOjpTaXplADc1MktCQj0+6hoAAABvdEVYdFRodW1iOjpVUkkAZmlsZTovLy9ob21lL3NpdGVzL3RpY2lub2Jhc2tldC5jaC9wdWJsaWNfaHRtbC9hc3NldHMvbWVkaWEvY2x1Yi9sb2dvL0xvZ29UaWNpbm8tQmFza2V0X0NvbG9yLmpwZ1swXWaNJRUAAAASdEVYdHhtcE1NOkRlcml2ZWRGcm9tAJeoJAgAAAATdEVYdHhtcFRQZzpNYXhQYWdlU2l6ZQBzgNfZAAAAAElFTkSuQmCC'
														}
													}
							            			pdfMake.createPdf(docDefinition).open();
							            		}
					  						});
					  					}
					  					
					  					//Close modal
										$scope.cancel = function () {
											$uibModalInstance.dismiss('cancel');
										};
					  				},
					  				size: 'lg',
					  				resolve:{
					  					match: function(){
					  						return $scope.match;
					  					}
					  				}
					  			});
					  		}

					  		//Send email
					  		$scope.sendEmail = function(players, match){
					  			console.log(players);
					  			console.log(match);
					  			var modalInstance = $uibModal.open({
					  				animation: 'true',
					  				templateUrl: 'app/views/calendar/send-email.html',
					  				controller: function($scope, $uibModalInstance, $http, $filter){
					  					var date = $filter('date')(new Date(match.game.date), "dd/MM/yyyy HH:mm")
					  					$scope.email = {
					  						subject: 'Comunicazione partita',
					  						message: 'Sei convocato alla partita che si terrà contro ' + match.opponent + '.\r\nLa partità si svolgerà il ' + date + '.\n' + "Questa è un\'email automatica. Non rispondere a questo indirizzo."
					  					}


					  					$scope.send = function(email){
					  						var post_data = { 'subject': email.subject, 'message': email.message, 'players': players }
					  						$http.post('assets/php/email/send_email.php', post_data)
					  						.then(function(response){
					  							$uibModalInstance.dismiss('cancel');
					  						});
					  					}

					  					$scope.cancel = function () {
											$uibModalInstance.dismiss('cancel');
										};
					  				},
					  				size: 'lg'
					  			})
					  		}

			            	//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};

							//Check if an object is empty
							$scope.checkObject = function(obj) {
								return angular.equals([], obj);
							};

							function getMatchResult(){
								var post_data = { 'match_id': $scope.match.game.id };
								$http.post('assets/php/result/get-result.php', post_data)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else{
				            			$scope.match_result = response.data;

				            			post_data = { 'result_id': $scope.match_result[0].id };
				            			$http.post('assets/php/result/get-images.php', post_data)
				            			.then(function(response){
				            				for(var i = 0; i < response.data.length; i++){
				            					var img_path = "assets/" + (response.data[i].image_path).substring(6, (response.data[i].image_path).length);
				            					$scope.match_images.push({
				            						thumb: img_path, img: img_path
				            					});
				            				}
				            			});
				            		}
								});
							}
			            },
			            size: 'lg',
			            resolve: {
			            	
			            }
      				});
				}

				//Blocking the user from going into day mode
				$scope.viewChangeClicked = function(nextView) {
					if (nextView === 'day') {
						return false;
					}
				}

				//Add event modal
				$scope.addEvent = function(){
					var modalInstance = $uibModal.open({
	            		animation: 'true',
			            templateUrl: 'app/views/calendar/new-event.html',
			            controller: function($scope, $rootScope, $uibModalInstance, $http, opponents){
			            	$scope.categories = [];
			            	$scope.selected_opponent = [];
			            	$scope.selected_category = [];
			            	$scope.opponents = opponents;


			            	//Get categories
			            	$http.get('assets/php/team-management/get-categories.php')
							.then(function(response){
								$scope.categories = response.data;
								$scope.selected_category = $scope.categories[0];
		            			$scope.selected_opponent = $scope.opponents[0];
								$('select').material_select();
							});

			            	//Upload data
			            	$scope.upload = function(opponent, category, new_event){
			            		var post_data = { 
			            			'loggedIn': $rootScope.globals.currentUser.loggedIn,
			            			'role': $rootScope.globals.currentUser.role,
			            			'opponent': opponent.id,
			            			'category': category.category,
			            			'event': new_event
			            		};
				            	$http.post('assets/php/calendar/add-event.php', post_data)
				            	.then(function(response){
				            		if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
										getOpponentsAndMatches();
										$uibModalInstance.dismiss('cancel');
				            		}
				            	});
			            	}

			            	//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
								getOpponentsAndMatches();
							};
			            },
			            size: 'lg',
			            resolve: {
			            	opponents: function(){
			            		return $scope.opponents;
			            	}
			            }
      				});
				}

				//Get opponents list
				function getOpponentsAndMatches(){
	            	$http.get('assets/php/calendar/get-opponents.php')
	            	.then(function(response){
            			$scope.opponents = response.data;

            			//get matches
		            	$http.get('assets/php/calendar/get-matches.php')
		            	.then(function(response){
		            		if(response.data.hasOwnProperty('error')){
		            			Materialize.toast(response.data['error'], 3000);
		            		}
		            		else{
		            			//syncEvents with calendar
		            			setTimeout(syncEvents(response.data), 1000);
		            		}
		            	});
	            	});
				}
            	

				function syncEvents(events){
					$scope.events = [];
					for(var i = 0; i < events.length; i++){
        				var opponent_name;
        				for(var j = 0; j < $scope.opponents.length; j++){
        					if(events[i].id_opponent == $scope.opponents[j].id){
        						opponent_name = $scope.opponents[j].opponent_name;
        					}
        				}
        				$scope.events.push({
        					title: "Partita contro " + opponent_name,
        					type: 'info',
        					startsAt: new Date(events[i]['date']),
        					editable: false,
        					draggable: false,
        					opponent: opponent_name,
        					game: events[i]
        				});
        			}
				}
			}
		};
	});