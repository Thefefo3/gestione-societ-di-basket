angular
	.module('gsb')
	.directive('homepage', function(){
		return {
			restrict: 'E',
			templateUrl: 'app/views/homepage/homepage.html',
			controller: function($scope, $uibModal, LoginService){
				$scope.loginModal = function(){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/homepage/login-modal.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, LoginService){
							$scope.login = function(credentials){
								LoginService.login(credentials, function(response){
									if(response.loggedIn){
										LoginService.setCredentials(credentials.username, credentials.password, response.loggedIn, response.role);
										$uibModalInstance.dismiss('cancel');
									}
									else{
										$scope.loginError = true;
										Materialize.toast('Errore: I dati sono incorretti oppure non sei registrato al sito!', 3000);
									}
								});
							}

							$scope.logout = function(){
								LoginService.logout(function(response){
									LoginService.clearCredentials();
								});
							}

							//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
						}
					});
				}
				$scope.logout = function(){
					LoginService.clearCredentials();
				}
			}
		};
	});