angular
	.module('gsb')
	.directive('manageTeams', function(){
		return {
			restrict: 'E',
			templateUrl: 'app/views/manage-teams/manage-teams.html',
			controller: function($scope, $http, $timeout, $uibModal){
				$scope.roles = [];
				$scope.categories = [];
				$scope.selected_role = [];
				$scope.selected_category = [];
				$scope.members = [];

				//get roles
				$http.get('assets/php/team-management/get-roles.php')
				.success(function(data){
					$scope.roles = data;
					$timeout(function(){
						$scope.selected_role = $scope.roles[0];
						$('select').material_select();
					}, 0);
				});

				//get categories
				$http.get('assets/php/team-management/get-categories.php')
				.success(function(data){
					$scope.categories = data;
					$timeout(function(){
						$scope.selected_category = $scope.categories[0];
						$('select').material_select();
					}, 0);
				});

				//get members
				function getMembers(){
					$http.get('assets/php/team-management/get-users.php')
					.then(function(res){
						$scope.members = res.data;
						$(document).ready(function(){
							$('.collapsible').collapsible();
						});
						return res.data;
					});
				}
				

				getMembers();

				//Add Member
				$scope.addMember = function(){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/manage-teams/add-user.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, roles, Upload){
							$scope.roles = roles;
							$scope.selected_role = [];
							$scope.member_data = {};
							//Upload member
							$scope.upload = function(member, dataUrl, name, role, category){
								Upload.upload({
									url: 'assets/php/team-management/add-member.php',
									method: 'POST',
									file: Upload.dataUrltoBlob(dataUrl, name),
									sendFieldsAs: 'form',
									fields: {
										data: {
											'member': member, 'role': role
										}
									}
								})
								.then(function(response){
									$uibModalInstance.dismiss('cancel');
									getMembers();
								})
							}

							$scope.player_selected = function(value){
								$scope.show_player = value;
							}

							$scope.membership_nr_required = function(value){
								$scope.required_msn = value;
							}

							//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
						},
						resolve: {
							roles: function(){
								return $scope.roles;
							},
							categories: function(){
								return $scope.categories;
							}
						}
					});
				}

				$scope.editMember = function(member){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/manage-teams/edit-user.html',
						size: 'lg',
						controller: function($scope, member, $uibModalInstance, Upload){
							$scope.member = member;
							$scope.member.birth_year = parseInt(member.birth_year);
							try{
								$scope.member.membership_num = parseInt(member.membership_num);
								$scope.member.pic_path = "assets/" + ($scope.member.pic_path).substring(6, ($scope.member.pic_path).length);
							}
							catch(err){

							}
							Materialize.updateTextFields();	//Update collapsible

							$scope.upload = function(member, dataUrl){
								Upload.upload({
									url: 'assets/php/team-management/edit-member.php',
									method: 'POST',
									file: Upload.dataUrltoBlob(dataUrl, member.name),
									sendFieldsAs: 'form',
									fields: {
										data: {
											'member': member
										}
									}
								})
								.then(function(response){
									$uibModalInstance.dismiss('cancel');
									getMembers();
								})
							}

							//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
						},
						resolve: {
							member: function(){
								return member;
							}
						}
					})
				}

				$scope.paymentsManagement = function(members){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/manage-teams/payments-management.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, $filter, $http){
							$scope.members = members;
							//Filter players
							$scope.members = $filter('filter')($scope.members, { role: 'Giocatore' });
							
							$scope.payed = function(member){
								$http.post('assets/php/payments/player-payed.php', member.id)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
				            			Materialize.toast(response.data['success'], 3000);
										getMembers();
										$uibModalInstance.dismiss('cancel');
				            		}
								});
							}

							$scope.checkLastPayment = function(member){
								var today = new Date();
								
							}

							//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
						},
						resolve: {
						}
					})
				}

				$scope.deleteMember = function(member_id){
				}

				$scope.PDFMembers = function(){
					
					//Format players
					var players_list = [];
					var players_index = [];
					var players_data = [];
					for(var i = 0; i < $scope.members.length; i++){
						players_data = [];
						players_index = [];
						if($scope.members[i].role == 'Giocatore'){
							players_data.push(
								{
									ul: [
										{text: 'Categoria: ' + $scope.members[i].category }
									]
								},
								{
									ul: [
										{text: 'Anno di nascita: ' + $scope.members[i].birth_year }
									]
								},
								{
									ul: [
										{text: 'Indirizzo completo: ' + $scope.members[i].address }
									]
								},
								{
									ul: [
										{text: 'Numero di tesseramento: ' + $scope.members[i].membership_num }
									]
								},
								{
									ul: [
										{text: 'Genitore: ' + $scope.members[i].par_fn + " " + $scope.members[i].par_ln }
									]
								},
								{
									ul: [
										{text: 'Telefono di casa: ' + $scope.members[i].par_home_num }
									]
								},
								{
									ul: [
										{text: 'Cellulare: ' + $scope.members[i].par_mob_num }
									]
								},
								{
									ul: [
										{text: 'Email: ' + $scope.members[i].par_email }
									]
								}
							)
							players_index.push({text: $scope.members[i].first_name + " " + $scope.members[i].last_name});
							players_index.push(players_data);
							players_list.push(
								{text: '\n'},
								{
									ul:[
										players_index
									]
								}
							)
						}
					}

					//Format trainers
					var trainers_list = [];
					var trainers_index = [];
					var trainers_data = [];
					for(var i = 0; i < $scope.members.length; i++){
						trainers_index = [];
						trainers_data = [];
						if($scope.members[i].role == 'Allenatore'){
							trainers_data.push(
								{
									ul: [
										{text: 'Anno di nascita: ' + $scope.members[i].birth_year }
									]
								},
								{
									ul: [
										{text: 'Indirizzo completo: ' + $scope.members[i].address }
									]
								},
								{
									ul: [
										{text: 'Numero di tesseramento: ' + $scope.members[i].membership_num }
									]
								}
							)
							trainers_index.push({text: $scope.members[i].first_name + " " + $scope.members[i].last_name});
							trainers_index.push(trainers_data);
							trainers_list.push(
								{text: '\n'},
								{
									ul:[
										trainers_index
									]
								}
							)
						}
					}

					//Format staff
					var staff_list = [];
					var staff_index = [];
					var staff_data = [];
					for(var i = 0; i < $scope.members.length; i++){
						staff_index = [];
						staff_data = [];
						if($scope.members[i].role == 'Staff'){
							staff_data.push(
								{
									ul: [
										{text: 'Anno di nascita: ' + $scope.members[i].birth_year }
									]
								},
								{
									ul: [
										{text: 'Indirizzo completo: ' + $scope.members[i].address }
									]
								}
							)
							staff_index.push({text: $scope.members[i].first_name + " " + $scope.members[i].last_name});
							staff_index.push(staff_data);
							staff_list.push(
								{text: '\n'},
								{
									ul:[
										staff_index
									]
								}
							)
						}
					}

					console.log((players_list));
					var docDefinition = {
						footer: function(currentPage, pageCount) { 
							return {text: currentPage.toString() + "/" + pageCount, alignment: 'right', margin: [0, 0, 30, 0]}; 
						},
						content: [
							{ text: 'LISTA DEI MEMBRI FILTRATI PER RUOLO E CATEGORIA' },
							{
								image: 'logo',
								width: 226,
								height: 105,
								alignment: 'right',
								margin: [0, 0, 0, 10]
							},
							{ text: 'Giocatori', style: 'roleHeader'},
							players_list,
							{ text: 'Allenatori', style: 'roleHeader'},
							trainers_list,
							{ text: 'Staff', style: 'roleHeader'},
							staff_list
						],
						styles: {
							roleHeader: {
								fontSize: 18,
								bold: true,
								margin: [0, 20, 0, 20]
							}
						},
						images: {
							logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXgAAAC3CAIAAACXNtIkAABYMUlEQVR4nO29C3xU1b02vOeey0yCwQoxKCokgkhtUC5ViXIIqEFBwEJLFVCqb4WI2HNeAwTf1k8CYl9PkZu+tWigPbRYAW9Jq4QqESwCJipCYiIqlxBQiCQzydxnvmftNWx29m32npmEhOzH+eFkZl9nr/Ws//1vDofDjA4dOhKHsM/Vtm+d7/B75sxc84+GmPv82JI57EJf1AWG+UJfgA4dFw9AMa17Vvq/eT8ccONP/9HdeBGiufulC31pFxg60ejQkQAIKAYwGK3WnIKU4XMNVvuFvbauAJ1odOiICxIUY0lJzn0oaeiMC3thXQo60ejQETs8Bza5q14OB7z0T51i5KATjTY0AMePK2+TlpY2aPDgzrkeHR0BCCn+xqrgmXq8t+UUGO2Z4m38x3a3frg85G6K/G00Jw2ekjJqQWdeZzeCTjTasG3L1rWrVytvM3zEiI3/85fOuR4diYW3vsx/pNJ35EPuk6Trpwu2AQ05KxYFvjvABH3kb4PJ2n906ujFui1GATrRaIYyj6xZtXrvxx935vXoiB/gDs8Xmz0HN+MN//OkIdME9OH98s22PSsjupLBaEy9zJG/wtQ7uzOvtjtCJxodPR2QYtxV60Ouk4LPTRkDk4f9ivsTHNRSXhj84TATCrJfW1NufEQ3x6iETjQ6ei6CZ+pb96wMnKwWfwWWSZuwjhNnfIffa921/JwgYzJfeq3jzpW6rqQe8RKNv7Eq0CjxnBQAOdPaP0/ribDshJzCNUcZcmY8HToASDHu6vWSX5n75jrGreB4xLVjMbHahCOCTOpPn7BdO6nTrvPiQLxE46sv99aXaztl31ytRBNyNbZWlmjahZwoM1cnGh1iYDi5di6VFGSA5Nw5ycPmcFu2vPUw51oypmWl3bVKH1QxIG6iOVKpdRe5B6wAzxebte6iQ4ckMGJbK5cKjL4URnvf1LwlXF6S/9hu547iiGvJaLLl3JN6y5OJugxcQOBMneRX5t45F59SFhfR4JlJPrCowEKhflnAKbQKTTp0SKJtz0rPwdckv0oaMi152K+4Gd62dw3ZMhQgf5isjrEllitu0Xq64Jl6g81OhzomS6CxKnCmPthUr37WQPynbyj9GR19cTSj1dHt/FxxEQ1+uNh2DDo1EA3EmdjoTIcODsRnVDY32PSV+CuBIAO0lD8WOPUp9S4ZknqlT1ofg7oElmkpn9dr+lYIUDEvyZz4L6kHUBoyOTJxeWA0U0Z2l00Tj1eiiW3HQGO1+l/EW18W21l06KCgc1481SG/JA2ZzllkGMpHb/0q2HyU/mnqnZNWsCYGRYZT0M5untJxyyRlHwEHmTIGYnKZM4fh366jgsVONHh44tADlYDqpHJL4myK9Sw6dDDsEJL0JFj7j04ZtYAvqhBe+Pu0sOcs+cNosvb7qX3cczGcke/P6nxhHFIbEdxYDRH3aOmfZ+1/2wVnnNiJJmZxhmFVJ5Vb4pnFfBYdOiBWiA18Yl2JOedgirCMwZj8kweTcx/SejocxLm9SFJBuyDwHfkQrzbrC9b+eRDcLqC/LB6i2RnzviodT/7GqnjEGU0Kmo6LDCQjaXuReKTxvdcciG71zq9pnQeDyZZ848MxhPwqBOZcWFB3Cl627AIIcRdEuomRaMDccdI2Hm1Uy7kuzuiIDZJGGQgyknlJApZJGfmYbfAUTafDigjRqevr+OAaKCKQ5mKImI0TMRKNP1Z/E4egq1GZaPD4Y4i40aHDc3Bz254XBB8Kgn05xMkypDzwnpXdKPwCF+yqWGjPf7aTuSZWoonDQENBin0o3iqGS5yn0NHTQIrdVT7DL/JAAZUBy7jk9s53n4iZZTBEIXR3x9gLkGM3IBr8suJnqRXBJumwSAqoZt1oldDRFQApG2u1eNonDZkmWY+K+Jheuy/sbWG0swyWSVflM13H6KsVna/lxUI08etNTDTHk55zoEMT5AyxqXnFtuwJ4s/BMs1bZlCWIT6mGx9WzzKSqln3Ahdw3HlnjGGf+PUmhvX2y32l5xzoiIrampqWFkITYXdT2/7/F3I2iLe59f7fSbIM4Hzvv0Jtp8k7oyn5Jw+q9DHJqWbdC8S7L1NylPSi6jO0I7zgsRBNPBE0fEAyknQ/6zkHOqJi2dKSfXv3Km9T87Q0y7Tufi7w/SH63nL5TSrjZbq7ukQh6eHGrbVV/TFwYn844E0ZUdgR1bw0Ew3YIVEsIBcfrOcc6IiOcPDhCYNmDz0t+eUXoWH/67n3JL/y1mz1fvk2LS5j6p3juOMPas4ml8TQXQBmAcUkXT9dIK14DmzyHHo97PmBa+Tg/vRVy+XDE560qZ1oEiTOAJKFrPScAx1RQUIfTtcGbWGGSRF/S2aUbRzDSBANWbo/Xk1ZxpDUK61gjcrTdVOW4fKeBD4mrPFtVev93+zg+AUwmJMNSelGR5Zr59MQ9BLb0UEz0SRKb2JY4SiZEcZoJjBIT31GlY5uhIgtlpSJsYi/xdQinmyp+vARZ3aQnVoma/qk9WpiZLsFy/DTuBm25BtzrrKEAKTo1wdPE82RVsAg/GIz/ej6pCE/61CHtzaiiSeRUgwxEcSZcyCA+owqHd0CUW2xII60Cevkvm0pL4zUyjMY7aMXqzF5dlmWAbOwokqu+ipZpBHVnpWh1u8jpbxMVlNav+QbZloHjO/Ya2WhjWj8JxPg2OYg5hQ950CHHDDnnRVFyutQWsFauVnnrno5+MPX9L3t2olqZlcXZBmjvW/S9dNt2RM05SsRivnoeeJlY6UYqEiWfiNTRj7WmTmW2ojGW5dgMy3f8UTqnOs5Bzqk4K0vg7qkPOeTc+fImTBBGZ7PN9FpZkzOUFORs6uxDESY5GFztCYJS1DM1WNSL0RepQaiUZ9IiR9FJWUEm+q5386nOnYGvK4bjHsOFOpvcmB7MAntfRz4ppm0iS9HPWOXYhlbdkEMFR7EFGO79h5+rdJOhgaiUR8QbO6drbIwatgb2UZTzoG1f57yyKus81XWe79q2lO/IIf/+SDgusGTp0wZMXIk9yEX+iWHrH79srKyVF6bVjhbWioqKvZ+/HHtoZra2lrBt8NHjBg8ePDkqVM0NfPGHW3bsrWmpkYQaXJ5VhaONnZcPuBIS6MfRu0mLtlKHJeN4ytfBv9Hprts27p178d7cbNOp5P/FW4TG+M2Bb8zP2WxsTl4sjl0/mieMH9Le95T4gugLUM9X/zNf/SU3WbIzkxK/ekTUWdszeeflL/0vz/58pTgjEDfdGNmuin3SktetjW7jzZtoP5UwOUNK2xAD87/5HPXVbu/z9r/xt7a2o38z+kwJr/YFIloZhHF2GzXTpKjGDx8/Epyw487EX/AxAYtRKPa3wQJ1nQmW41QwzmevHVqWQYEb7A65L5dv6vttX1uPFGMhtuG/ujx4hWOtPMbY5Tv2L591tYHMOWWr3iWzoSooV/zHnuscP5jKi9PPfCM16xa9cbWbXg/Nj9/7Lhxi5YUCzbA48cg2LhhAy4Y1yA5sPjYsb1iWUnJiYYGjBDx1AUB4RdYXLRwucOBo82cPZtR0U1csgUwWGbW/Q8oX0xNfSSdDRSzZtVq3IXD4QDN4dR85qK3CQ7CZeBci5cU028FVX7LD3hf2dXGP35ejpW+kVOa+FeI8fDS4z9W6MdEeXDDq6+eONGIOY/tR2dbc9qzSd2pQP13gfIDHlwJtplza0rB0CTlH4HDCztaq4/6FTZ46NYUHJC+33Us5YUdbSdO7h80yIWBUTh/vmAY41GS57i0hHuODC279c8ngi0NEf+9oqJExlXphh0VFXgoGCriszhbnHjKOBHOgnPdO2WyYIXWBA1Eoz7y2pyRE3BUqyGasC+ysqnP1U4aMl0yog8rxtIy18nm4LThyQVDbVgczH0HpI3L52+DnwkPBiN70ZNFGIUcgyhQycxf3q/ywtSDm3igj2UrnlVaLlhioZSEhw3WkHvSOOaiooUYNxgQs2bPlpSAsC8GJbbcULphecmyiu0Va18kPhqFbuLKrcQ5KhEAu3CTHCOV/oa4U1minMKAZLEXxvTkiZNKSn43+d6J4lrimPxrZqQLdiV1f6+fLr5Tem1nN0+Blo3lp/pY0HHnSrkboQTtbGkuuMF+1x295KQVXAB9g8G2eb+7pMyFIy+Z4OA+VwafSgQo3NTM3c7ynfa3d36BtWfj34olRWk6BrjniKWi9I/PGz55PnC69lxzGLP1yltTRy+Wk2Iw/rG44rmvWbdubPs5wgf3FSgYZ8EzxehaXFwcg3Sjlmg0hc9geVGpUtKRBOJQqQ+b++bi4IE99YLPoSuVlDn7pptKH+olkD/FwMPDvNpYWoqHBLVBzXkTCDzmwl8/Sh528WJuLVJGFpG/VlDZRxIYc5jMOOaGv/w56pqDUQJWzR+Xj13winmNUgMMUPAj5gzkx6ijE1ey7e23wDXFxb/zfPbnO69pVnMKLDxydgfPgU0hNxs6bDAaHX0lN8NPB4rBb4spNP9Wv+27PWomBZgI/ALWeKGiFRwx7abkx/NT1VytMtzpQ+e9euxE4xHl+U9Bn+PY22+e98iv7v/ZvWt/kWZPMhCnda+rHPnL5SYgngh+YYwoNUOFA1YIvEDHi4qKIE9hfdKkyzPqiUa93kRjh0jIkDoPEuQ99QcXr10Mu7ws2tpy11Abnr3K4wCY5HhUmAZUdFS/YzygyztlOq2PSg6UZfBmxwfvq19qcHZcA3aEYIWVLSFXIgCVujGBwZLq94Jok2psfebV1wc8KCtZ8CE5JBhq3/nkj0yQtVNYU40pl4q34QgaE3uUbY/WVF4sac9OTaOLXNVR/9oZ6WSqxwpLv5HLd5hONJ5SPzbApH2rX149zTr7lbal5a7nZman3vyfCv2nKO9rfSIcwH07Rr5P1yetAzjxEg31Ipl750TdkiJwpl6lUma09xUHL7o8YSwpUKc1sQwFSLr2UA1mmtYdYwOG9bxH51KWidO0xgeOiamiiWUoMFDWvLguqqklZmA44hQxjOlHH7xvz443X9jRKlaUBGANdtLiTOuHyzg9wjbwTqb6kGADjmXwOK74fkvMBQPycqyZ6ekYhPM2NcfGNaaMgaberZvK9judzm1vvalmAhOL74fLafwh+G7JxN4L/3667qqiEVfILpmUZZQUWBXAGKPrE14QP9U7SVQRTfCMlt56bPgzHr9KJ7RPdQqlpP9yaZkTj1aSZdQYibB+VlRUqLyAOAFGwL+JZRmMHijbEINjOyax2syaFdV/FDOoDSgGQBmBlNrYHFRWhC0yUfMQk31Hd9H31gHjDU6J4bGoaGH8LEMByQucCK5ZucOldcGjdbkMr98PloE2HZVliMW3YlGw6TC1+BJzzIDxEx9YUHpgBgaDnGwOrScqy7Dup4hXZMTIEXKH4rhm0ZNFcqY9MVQRjfp0avALFxdj7p3tU0M0qsUZcW2R6qP+D+t9a+ITWZeveJb7fTsOlBGwXiWQZbAmQzcBU8Sj+kHPx5RL1CXxAVkmtpsNNtVDTOibboRWMn14ssKWcgFsLf+YH3Hu2tJI7ZVPXxVssGbV6h0VFXgc8bMMBbgGahS4Jvsys/I1c8Bksec/y90CFFhlmx3JwNiz0nf4vUiaktFstPdJu2sVNcdMnjoFN8VIiY/EIFhURN1Gkkemxhcw3aBBg/DIMK7Wrl59eVbW4uJiSVMRtln+3IrJEydtLC1VaWdURTRa9KbzlbtMGTlM4koESarim/e5c6+0qLH519bUVGw/L7lk9cvifD2YpWmJm/yS4Bgh6npFr7Oh4Tj+w0WyYS/j5ARUKoupcb3TcAkck/6ZlubgDosfQeBWTwgwbbgxilODZ7mz476UHaU0ugozFgsJf9JCwFnP83DfNOSqMVJ6E9QKnKv88zbGYLBcfr35pVcFjjMiyJSWznvssUSxDAXG4UO3pryyq41VpqJ4JKz9R6fmPcXX+7jnSB3t/OGaPy5/Qu4lxgMvRkoCUgL96W/4iRT4PZ3OZbg18WhZs2oVMUcWSz9lqlJhcM58cDa3L/mJXi0tnDtXzmWBkYwfENSGR6lmOYlONBDV1Ifh8kXZBJa0YKtpCMUZDDuIM8UTokQ6UqdpbW0tGJr7Hfet3ruY9WrPmj3LIRWQllioYQRqwYHUQ8PqMG5AOtR/ibWocP588S4YCpjMyo+Z86o4HA7uNomZtmQZuADrUhbvZ0kU0tgVj77HUomzY6kEtdFPaCiTghhPs20hI/ADTzLZeDbuk/pTAXOGZYzU7q0fPX/yrB8TPveqZLO3jfmGsMxg3iMmE89hf/AnzQos4/KEN+93lx/wcDF7kLAKhiZNvylZQXyec2sKdqE+b7ltgJRRjycNabdwzpo9mzIvhmshq2LjyWZl9cOb49/Wr35+xd/TDBEDEKsriaNj6MNtOH5c8DTxrPH7r1m3TnKc0HgcMZvgIFh+Bl03GN8OYkejeF/MHfA1eEqNUBOdaDQ5tvmirDlxRCPpwsSYs9sMyhFTlK0xowQ2NrpoEPl5+/bEGk0kEZURcD33TiSxZGKnIw3xmHzPRMEuJM6ttlaSgDhQJxfOK3aX4rCYbzhsAv1fHLgDgmVwIsFNgXDxyyuEBdG0ezDLa6cC3Id40PxnDSVF0gwMcSZSoxM/5ivrxC4Y/G6YeE9NG6DAMmAxGtiCM3JhgdDjXtvnxguquoI7DFxTUuZaMNYuyUemjIH2vKfEazB9OnheYBm8p7EqEV3J8ekD/YhSRvxKswdxupIYWEtwBMFPuqG0lGV5aWc5vlVQ2bASbNtCpsnG/5F4TLhCHBYbJIZo1CdS4kfk/wR4j6EQf8KIZEQWUHXUr6w0YbgsXyvtXsVvNJNdQ4hNq2jhmlhtlmpAGUFZPYEsk+ZwSFIenuWIkSOoVML/nKoDytEWi54sknNy0cPi9nHqNxJqOeJfIV1L+UPf93WF+bIh4BqqT0kSDZVoIL8ox+wbUnqLP4Q4w1VakXT0bn/jr/Ykk0KQDiRlzOq+6SaBCwmDDSQyb1MzvlUI1wI3vVDRWnbAI7bUmDOy0yasU8g2Wra0hPPT+Q6/1/rv/6a6Es61YPwlC/9+pvXmlb3kI9Swb0uLU/Ahsb/Ijz08IIiWct8yrKgFBUpSI6PfTp44Se5bPqIQDWhCfZFUsWXOlKEqEUEZ1v55ks+m/lSQW20kAQk2K+saBfcqfaj4HTHoOy6UhjKCwvGj2omJ7W3FCgj/fFtS7aEa5fgXHBYEVyHv9qbug7G3j4GC1hE5FrgAvqWGwnPodePxPfa8Jfnj8uWMlypVdYNJ+PTPizMGo+QuwTP1723bODpbadhj2IBfJB3V9PNZr/6grByNzrFCqecTDXHCpqdbZEYyRS2bmwbpL+JX+uHrCGOarJas4RMf+G3JP2+DAK7S+EpB08rkxh4dmRC3IZUoHweDTZJKMIMgRuE4UV3mUYhGU4NtsRkF1BM/0chl5X71XWCBfDgmdGzikFqnpFkw7MIOwVLBLxg/Go43KDMC1pyx+flR9RfBCKupqRmsuAsGEKQ55aWGSna4/Y4gGvFa6q3ZGvj+kDWNmB5wv5gD0BkFPEg6C6qDmGg4ccaQ1IthfhB8660va60sqT7iUbDrYdj844AXG8gZYvC5snIEDLvSgg24P2krXuM/n1a+nYrtFZdnZQ21fN68ZTHtaUdvxJ5XTEUzSYFFGXs/3ovhLTcGsG7Neyz6c8fU4OdACYCrSgDRqI/ZBVWLNc/47cG27AJJjdTlURKqGVaxYqJpFhRjx43DTIvt8tQAj0GZEbBBbH4fZX0nqgWHApLF2tWr1Ui/moD1WbyWejnX7DkRr0ZkUwj51M4lQ3I71YmUf6QF9IympCHTmPbCEmUZqEUM68+SOyYdNsqGP3wLHsGWcgI1PT4NAhJ7l+RQ89m+n1zW6q4+54k3mm05d/NL5wxmpzTDaFgSGhqOZ/XrJ/ctOCJ+8xwen0I2HIeoEo1a/7RkwdH47cFy4kz9d2S8KthosIHKVLcRI0copy/HjyiOIaczhkmOmTx5qlLwFcPeWtTjyHkr4gStvCE4phGyzKnPA4r1RoJNaiUaAVr//TwNBTba0s2XXd/umGfqwTJ4Q11ICqZcDJuB8jTEAUMLW8oSDXt8nGvAHb8ReJfk4KpcevTLT/KyLWRKmizG5N5pE9byl1jcQqqxNRLrrBqQpjs6vSarX1ZtafSAT6XfVJO/yZwp0fsuzlqBWA0kj2C0qS3eA3W3M+sVxoBaNio3hkAeNfTU0d40BVChXfChkWg0UcCVKNKEsM/FtWoibYl49SQwBlrK56k/lCOO4E8+kkcKfdiS8B/b7Xr/t2F/21en/AvGpjAma/KPf5k87GFuA0zDtj0rQ66T3vq2wPf+lrJ5qaMWJLwdSszAIBRUF5KEEtFo6qxi7X+b9AlUV9sTwybznEgoIPMvNUcIOrs60VCyUK681U0hpjnLFTd7DmwKe4TWEz6Um7LLoW3fOi4UmBANT5h3bi/q5Fp5poyBDHM6qjhPvNcfLvMd+4iTU4yOzF73/T9uxJK2DduLBNMHfzorinpN70BlvyOgSDSqS+rhl5XTQvFzx0Y0tM678jYuT1gheooq5FHh1GhgSyxiFjouz8oSB01woCISNujoWERNMFodBrMN/8cUkhswIZUSTXtLsO+byMJjvfo/+J9DFtDaW9IZzfynvA1tBck8GWXoEkGmsiTsOUv/ZH8WJnV0MccymH2uioWSFAnpRq7Lqybwk5tihspCK7JEo6mzipw4w8RhD1YoAcucs84oGGKGXWl5ZVfbiZPf948m0ODn7qA6CRSY88oPAyrGju0VWnVpiEIKPgjqd6xRQTQ72FB3ZXN1okAGg9FC1JwzdXLzJKzOGMx3OfkOvwfVg7wzmlOGz+U+JzXJ25d87ZtO3N7V8hFYdNgoL2D49qvvApIlrGjIL1WH5aywuH3Xzqf9x/ZwWZHm3jmkKNfS8z+Iu2q9u1qpIwi/2LYynPLCMlhmY2lp/EuRmukjSzSa+tIqtJ4yZ6itF8EH7bAXw44cMJLsNsOb/9g5P/cOhc1oiHBHOHc5DIo4C2QBiqmoUIqqosDwhfjDt8sojCGG9bht27I1qt+xomI7BkrnWXOMZMj5j30k93xjaG7tqdlK9Sbzj67jC0qte4Ql9TLTTRgVEHVzpZrPMazDCBtU1nsVHE/4lmEpif8hP0MS/A6Wl7SgEUHmg9+Ffa2Rvcy21FsX8VOW+GWSFaDSkhXVJUTjOdUcKk7IEo16vclo76sgtsQm0chVM+Jj4GVm5eDgacOT//xa+fjJDyhwNokZY0OtY7hIlcjql7VvtZKAOvPB2bTumQLX0GSCNS+u44Zv1DGEm5p1/wM4snJlABq8G+0mEgZjckbQczbw3ReJOuB5M7DBlHxDu9o6kjo7BkxlnU+ORyDI0NDevGybpFADcQbfTmuf8SRILJCWT8NB3zc7nDu2RCwyBpP50mshyPCZMRxwiwuYSkLltFIeexBj165eLQ5l6ghIE436ziqMfKr++XNotAdLVoQQI7uPqZ6XCyMGhNvdJ1PnPTpXrvIgLdy7TEWVyXhAxxyJypMJ6qEJbKTWH1ucUbwBrToq+JCOEuXzzpw1azFbAkKSa2j+3r1TJquJNkoUTJcOCv7wdbD5aKIO6P3yrYgZ2JqqUFyOQ16OtaTMpaAcYdhAZpm3qfnZqQ5BngFEoYVbSMVYvt4EluEnFmDe7qioEMT1Q5ABGwZtBiZIdhQLMhStHy4P/kiV5mhS5+JQHns0+gHSdNSFdvI9EwddN1iuFrUaSBONtkTKaC17TY5MTUSjbJ3hIIi/lMQrq/7PnN/8fvLESZhO+fnj8GNhVpPmEmxi9ImGhjgLjqkBzjho0CBoKArzmV4DSAECyKzZs2i+LG0PsKG0dN/evWPz83e0L9CFe2EU+Ythy3pFDgsdauqUwWyAFu694XgDPWzMVR1jhi27gDTwCnjj9AQZzJEAf07L4C94YRq5JwWIKi/YWjfvd8vVCQcBrZmRDkK578Uf7hpqgzKV08dM+x/844AXcjQIiCMp3A5p9c0DxhX0pvz88w/FVbn0XBEZi6Qgw5yzVLCXrSr4S61Eozj2sLCRAbC0RLmbyqKiIgxFvME8wtEggMfQfUWGaL6NMWNbEuRHUR2EJVkRQhIYMSWMq/yAR0GddthTt739FmbvxldL+UmJ5/p+zO+4hk184NngcTqLlWRUWqJlzapVkLP4gQmgGJr9PDi7nbVLDX8xLNdgg42lGxa3r27FHTbWe4oRGC3gCOgI6nVzaRiJrAG2CrWQOWAwWVNGnhcG/ac+k9sPHAGd+rV9boWCDxBkNjzUC0OLZGzvd59sDvVNN4JxiifY+YMtOXeOYFGEOENrQdEHTdr4vvtE6BzrGYwm+21PiQUZNR3y+LD2H61+Y4w9PHq5ob64uHjvx3uJVv7Si+INuBojdD1ml+fS2LqvSBANUXpVCyBQi6KGV5syNJhpVEZSMuyIGZ1tLT+gZLejoDXcmXO94rCwCya8slwQP7AC4PFEzV2k3Q6YFZH2bJKd2/igYyhq+wuMBjogaK84cT+8Ds0plYDZxgTc3rq34zqIgRAN9KZIC0pLCj9givh05AGKAdGs39Wm3LpAUJhCgNS8YvGKSGsVQibFv21713gPbYlcntFssKQkXf9zAcsIelephFmLnwQjH0sXhp9kiQKMnLUvrlv0ZFH+7WPAHRgGdGxwzbYYXp8cmoSM9RLaFlZuQX+0KNcs/khTIiUoqWn9zXLfsvE1GkqoylWEkMP04cmFm5oVvJUCSM5bUjCltLRDiYYaX5aXLMsfFz15km6v5uER/nI4MIxUpkpJ1rgCqRU+Ondv1SdqjpAQmC8b4j+6O/DdQclvVZaaxrxleHoTVBjuKyLmOBsU9sUSVTzBsWhrS3Yfk/oOcOdPbbWnFawVKy+YllBv16xbZ08ynv37z0Kt3zFBkjlFLTKmio2C2J+YG+8qOHklAS5QcAtgQFKpHxOByN2sNE3LpNGehYJlDH9y3VdoBQk11yBBNJoCgpWhlaoVevRIgrYTXFrm3PDgJZJicKCxWlmzY/uuFXVCVBvE6YrtFYkt/kKrcEKo0dozlw9ckpoQ8gQi6fpfgGgikS8iQDBRRTTspA22HKfvk66byn2lZqXMy7HeNdT2QkUrFCJNzW0VWIYWxBx9TejsX+8JByKCjCn9yrS7X2JHdbu2tjTJU/15OQiqPqkB5xYYLJ9FyUn96kFLaqncS4poGuMt7BAzNIkzFEsmOKa+2BRD9XnmXMONTptmkFHH3j6G9sRJFNfQGmgx8xekIeV2wB0BYqaxpnKxJAKoCSUn89ZkDf5wmEmNuIoF+YdqLgMDZinjLCTepTSVErHAwcSB65f02E9bXTufiUTimawpNz5C8iFE0GqU4cOWo8qCKQAWpBZnC42QSIiajOWZFlFSub2QaHxHKjs5MeQ8jOaQs1Fr3A0EGVp9nmGHjvodqaELgt/Y/PzOSTXi98SRtL3FBvDXvRMnxcBftN01podCD8yEwF213nNwM8YVZz01XTIgcOpzyY3VDADW6tdMiIYNvjVf3k5oVW9m5rhm/tjUqK0L5FgGZI2fcdKEsU/++IDvWzalwGQxpl4mWXOT5DdVPqO+KIIYKl0lYixfsWIRQzpBq2+RKgdaB3rZimfVy9FCokmg3qQZoQBU1vTJG7RKhliOiifYaXAEho6a1itcX1pMTmg0agpqJAS0P+SiJ4sm3zMRi4x6YVUhCJja80A0oBuVvUr53gTwXYcSTWvlUm99OeanuW8uKMDmIgWGkgZPcbFEI/ZDq4kIt16VxzBvh1q/x+rEsLoY/1tNCjsGTGa6adWO1g/rfXNuTZETbcRubOZc3XuMoqcL7x2Xvi/sIeqSwWSzXTc1ZUShxFGCvhhMv3wYkjM02RYEANdkZfWjbdcL5z8Wg2jD3bLWuBAR0cTpdIwPbBrI0rQJa7XuWDA0CcNl4ZYWqFHThicreC5pa/SNpaVcX1q2vUkDDREWQ6WtSz0o19Dq3PiXtMFWjEqgFwzpw+FwyFWTwDF3fPA+FCgaMaTstqdV2QnXsGOFkqzc7StTsNxe3I6QZai91kTK5Z5/rKSp20e/Z5jTr298ad/nwonnOUiyLhVyYi19hzHhN6q+aQ0HTAajKal1N8Pspl+FXI3e+jaV+bQU4Je8bOsLO0gXbRDNtJuSBYVmaI83/ie06T10h+HDb/zzb3J/5N8TZnvvGpMzHHf8QVIoC/vdH5VtcF8hXWNUJU42BxV+czVjlbZdX7a0BKLN8BEjJk+doiYoBqOloqIicssjRmx8bkVcvbc1JVJ2EKCfe+vLYpAPMUS2PJqxeT+pU//KrrbR2dbsPuYhgfpL/WTEO1ucNTU1tTU1OyoqMGMhOtJGKwwbpk2j+CQPS77rl+BYG2rEnfng7DWrVhH/NJuWRjyL/c57hVhXNGnGhEcruGC5Y4K/KInk3z6GtjfhH3Dvx3txxB3bK5xOJ5+MQF44uwKhSOZbRt0LuPbqTIXMQFPvQblXnmg4+vUpr/AgoRZzqI34ayTlC5rykpPlOHQqVH00xJhNZs/5I4Ravws5ZfeVA201WX3Uv35X26KtLXabAbvjw8x049WjZ1kMo5iPP+Y/EYYNRHp13bPXfvensOcIe1nCgnh8YEhfYz1SHw5WH5VlQJqdp3CRl192SZZPdqAyqscqXepocXg6/DBa2HqdaYJKaRgzNNKCu+WYY6/aEU2gqQ7yreR2YU9z8OzXMZwgBkB9UyYao6Ov5HX2Ypj/dRUzY0Jg54Hvqr764cNvnK/s2sowkcodmBh4DFjGBRQeg709IaBRM4uLSU/e2kM1JF65tIazTFP/IjBr9mz1rnfO78hWAPiYn+dC20VhQRN0pKPDTuvFq9mredvM4DnFSFw3L3Xk/LUzvwiHgva8mYLoEkglZzdPZWRAPQaPj0/3DyKduZOGzuDrKVHznhWAqQ66gTQExqk66q8+FsKKxZRBECOyGH0ipPvdVDKELEfecX+ymobJGCwpjvHPm/veIHlYz8HNbXteeHwMpCSlWvpRcd+v/uuXsRpoxKDRVTRgD1RCypizDSr529DGlXQQgoPi8WC0IxpMb7kZTup6dBbR+I58qFCvhFG8TgA/xoz7GAlbf5cEjUpgEkp0IKbOTF+SBNZwvjECD1TwTCGVGJIuCbtOtu1/SUA0RnumXH4cFzgeCcMxmGwD2mXnx1Y3iw/o4JlDTffcdr1j3ApJcyGpR1WxqO3UZySrwGg297nBkb9cbrhSE1Wcl8RoiZjXBNqbiR0tHVjAgFHZEpfRmP0UPxTqlejoFoBkIfjE31gliDRLvfk/MWND7iZxGafkYXOc5RL21PORVgFqeTULDCJq62YpghavkuSOwMnPnO/9Jw0CAssl3/iwpAObiTXqVw7qI+a7JlQRjaauuAmB+ro+OrogIM6IBww0YgHRWK64xZhyKbbEsi+oTYmnnzRkmiDYxGjvS/UmEBNtSAKZKLFXDnJJGfW4nOzQuvs5b907NFlcwe7L0CyniqJEzRqtEfNdEKqIRpM4Y89/lo4n6sACZfi+rdRazTO2CtU6uggkgyQwisSZRRGhpu20/9huQZEHyBQYQpxEgMnmyF9BpQz/sY/oh+KsH62lAtrtK9OvlhGkR5qs1ituto8VFu7gAJ5t2/NCAuPRtEbMd0GoIhrl/hgCcKsWFUnYpWl6zAHXOi4sqNMh6maCPFXJgDRMPLE/EeRiSr8y+MPXrvd/e8nMCsEu6ZM3QgUD3YA+oExxFpPA6S/pG2uWsIhkzLXoxanY3GWTBtjnOlLxO7pJIp6oX0lcBOIMo1qiURvIKOe0wvAKNFart4p1nW4SPRxgmVn3PxB1M77XUyEDwFtXLtZKHON/3/z6jLC/DYqJ2D2MyZ/MCOc/zZk0mKzmPkMFX8UwchQEmXYlxFlBJnX0YjnhQrJpQfywZRd0d3GGUUM0mvQmNmRTGpb+eeqJ5iL4ZS8m1NQruXIEhXKCrka5LTEJxXZfyCC266Z6Dmzy1f/DNuAOOScxH2EPyTghkf4i+QUCtcr8b0bRIhNpgN10mCYuGSwp9jFPKwgyiTXK8HERiDOMGqLRpDeRkM1EQLcEd18o5zSK7b5AyohC35GdoZYG0rFo2uvKy0zoHJHJVSBJGbXAVbFQ8isORB8ZMh1zWPJckK18X70baYCtGIlHQSNllM8YG+RaQnc7JFKiUa5Srr55u5z+peMiANZ8TEuxszZ90qukuoK3paW8MP3eUoUjBJ2NlAJMGQMkN4BQk5w7Ry5sD4oS+MXa/zZJioFg5f5sIy6D/GE0G+19JHMjOcSfJKkMlWVtuz6iEI0mx7ayGKK+npYtpyD6Rjq6KqKmy7mr1rMKTrvZS5xKd6x0vvub4Jm6lnd+nXb3S7LHP+dyMqX3l9sG8xOjyPPF5sC55c3kyDRnkpaEcqzhP7a79aPnQ22nI0bfaLoSw67BENA6rtrBRSPOMFGJJv722xR8P6UyMOAU2tHpuAhAjabpkzcKPjf3vSFlxLy2j1cHvj/kfPcJxx1/kNw9dK67o+UK2dKODGv6EWRCygGDs3XX8pDzZMQcY04GT8mF4XG34K76U2K9S2J09yA9PqIQjUbHtjRBkJzsaDozh4sgZEBHVGDVgSwgLrxgG0xyMcA1/uMfQ65xjP+/4sEQPF3LEJeTxailSqwkQDFtu58LQmanvZZMVus1+akyMcEcsPq27VnZ0SGs5r65F5PvNapEo1b5lGu/rSl4iQv91HHRw1tfjokkXrTBNRhIrbtWBE59fvbv09LufEEw3yI1QE22eOYhUZRAFq3fU4oxmG2Wq8dGpZiQqxEU03EWGT4uMgOCEtFo0pugA3PKObHsOE8Gm+r8jdV8iqF5sfWngvXftWv8lnulZdiVFvzLhX4mHLSfOcnzP96uZgftNJDVL0uhnGrXh7Olhebg0jYP/K8Gs3c3YuTILnh3WIEkcwWtA8YbU/s4K4rCnrMtbz9izb6L7/QJBzz0/zGckQQNfvmW+/O/kGFJbTG2tKTrpiZd/wvlgYftPV9spnUCYzivVqjsodiNoEQ0mvQm0LwC05cf8Gze5/nqu4DdZsjuY+bXCgH7VNb5XtnVlnnZJVNa/1k4P7JM0dpCyieNWqmArWi1ihZhoXUSBNU0QD1cOQWHw4Fvx47Lj6E/liYUPjpXffFQUKFkowwKWrye9pYbNGhQVr9+ghvE3VVUVCwvWaa+ldXMX97PvVdu781h+dIS7hcLNtWvvs+kvD0HGi8unlTmvjf0mvZ6S3lhsOmwt/YN3zf/Sr5hZsRuEulLqU1v8h1+z1OzNXimPuK0NlkxmZN/fD9V1pQBqdxdtb4z0/209jnoyoDYQdLxFbZISMZ2/anA0jIXKOauobYF+amS5YiwmJy9+oGyqrMbS0t3bN++nK3fBdEDkKsUA4KIWoCSFnMFv4iLsIgBWQCgHScWFy0cm5+PU3dEsYVFRUWY/GqKtuLiwQ5yZEprXJ1oaLh3yuQ169bJlwsh6f/4JfHDbijdQNv3KLeCoh0ss7L60T/x0ylf57zHzlcYIM+FFEm6NMq98QCugZgg1qEwKtLvLfV++WbbvhfD3pa2vWvcn20kocDhkMojY4j7vnnfe/i9kLMhUgvdaIYIg4Ok/vQ3ahw6nU8xFBeNASFw8jP3p6867lwpSzQJydiGIFNS5gK5vP7oJYI2xhxoAPglvbMLb2UmT51S+OtHaZ1thi0NJddxjXanlzsvbW+A2aW+DvMgVnWivEaFqcK5cyEjbHv7LTW7qwSYlOv7p7wl+IiyjFjlwd3Ne3QupQOVzTaxDX4HvCg9jb19zPIVK5Rb9KovpMZ/RsrPRQ7QoSBrSFZmsF07Ca+2PSu9X/0TdOM/GinZGfY0uyqXGpN68X1P4bbTvoa9YW8z5KCwzwnZh2t7An4xZWQnDfmZGmGB1AOtK+80RUmAGBqqdE14DmzC8kDlUFmiiV+coSwDQUauOQGRXYfN4YvNmA+Y2JhjoImYm0JwLCM5S9WA1qnDQRJbxJOISyXLsP6rYRlsLHn9kLxwYZBHYiuqiFNDMVxWUgIa7YS+4+rhrS8PnKmTyzkCB+GFgQv1J+Q8wRBLjdvHZrTgQ8kDGsw20lbpkixLv1G2AXeotBxj2PvqyzrH3CuH2BqqdDW0fvA779c7mHNRCLJEo8lAI0b1Ub8cy4BfLJnDLP3z5NYWCPa1h0hx3+EjhLm5aoAVOx6WoQBbQWSYNVvWOKIVIAjQByuDRCllptDLgrIMLasesxUJO9Jq+LQbd9fhmmDTV81vzErOnSOXGYC10XTpIFoQy3TJNXgvHqW0cIQtu8DkyFQpF0B+8TdWBRqrL2SvIR4SlcdzoYDfsPnNB2lPdIYNsMR8lyWaeNrIuTzhhVtaRmdbxSyDAaTMMgw7E9a89OLkeybGcGpCMRs2QGOK08lSwZpXE2WjocoOCAIcqrzl8qUlcroVZRlIMQq2YfUA3+GqlLsXxgxr/9ExCwXu6vXe+jKBqCsGqAS8Q8rp+6T7/ymwDHWPglmCTXWBLlCQnw/lPJ6uD5Lv/q8lEY2VLUJoTM5g5CQaUsEsDmpfv4tEOkhqTGxRknK8xHoTH9SmEEO7pY2vll7O7quwjaDGiqQCAs1lbH7CLMGkH2ZLS9Rmktu2bgVLSrIMad37ZBFbz/xZud3xc20s3YBbO8G23aCl7Wc+OFvOiLNoSTE2TmyXXgosJPFoH2zNvRJ31XpSHDpHOgwf4gz+bd2zMuFlGS4sum86Mdsr6Wn/sT2RRp1s6IAjfwVNx5chmjgMNI3Nwdf2u4sn2JUbudHB5K0rd4yTjp3Bkruo6LjWs2Pm5CsSBHVFCT4Uz0lMWsxDrWeXBFQhqspFZRnIF3J2E6oPbnv7LcmDcOZhkOOs2bMgoThbnNSxjcMqdKqjXXo3lG6IqtBpAggifmcNdod0gxfWJHHCN0U89fS6Jizd07HtO/xe60e/P9/m2Gg2985x3LmSm9qyEk3Mpyw/4O2bbiwYmhR1y+qj/rKyyq9W3FrfSCQgLoyFmxXLV6zQenbWsDJb7lvMPbAMfzJT6QbyCwBpYviIEYuXFDccb3A6nQlpUcy5mZTVE655syQjcPqgrGxStBCKlcA8zEbNPLashHjr0xxpkmogaAvSHy5S0DRKUOxKUz0aCjW1GlRCgbAgFHcRw0qi0O0kmkjhnh++pvFNBCZr6k+fsF07ib+ZBNFgz3hKt5cf8ORl25S3cXnCS8ucH9b7Bl5mzssx/dcD45N+/ACNZKH9G1W2dpWEI002lAuahWAys02zSIMbLPugmw2lpZMnToLyRYLf4u6NrdLNBPqjLCNHrGtWrVLQB2nAnqQTitp9GerGGvm+pDQEigHRgOm44+NQ3Ld4KLgFhYunEBvFrP3zxNXFY0bwTL2k5YLNnHz8oqkSK5fH0zVBi5z6v95Bm1sREDffNWkFa8R3IUE0msSZ5Nw5/KTtQ59Unmxed/etA6FCye0Clpm3qZk095yRfi5+r8px5UMjRs5miMY0f9GTRTSUJuFGSnAQaa425WNJaYU2uMGUW760JH5fjEo3EzYrfHSuAssQ4w4rE8kdAeSI3RXkr8XFxVRkk7wp2tln25bzRBODKDdIFHLNsEINNclpPZoYIZ9TLtb44gg5oehGdQtI4Z6qlzmjLxOtmrIU0Wgx0AhsdZ+d/hwa0PB5W7AEuSqfkZSM1u9qoyyT3ef82X315VRopL5bEE3MRkpni7QbgmGnHNQi5a7DNNJE60lF16DKzcQ5khSUROr/krskNW54SiXgGjn2xOeztj4ABS1OIa759Z+n3rqIX4szNW+JpX9eJ+Q6XxzoFpkH7WqDsTCYk60D71AuQigp0ai1rolDGDGNqRgCQTdtwrqzm6cI9GfOVMxnGUbUi4MaKfnyvErQhtBybmnaoJp6Z7iuw2PHjcsfl8+XnuJ3wahxM3FBMQqOJIZVrMbK515R91nh3LlRL+lyeRKhwghOFKccl3LLky3lhZCc+VxjZUMZsPDgEZNwlYvLdptAdH3HtphiSLWOrJvsY56JqvEJiUaTY1tsuMK450RoNjG3QKClV9b57DaD2FSMk/L1cLE8rxKgjDWrVkNbUZjh1CjDsNYN2uqcpkTNmj0LMy1+loGIQbMHFA5Fw5fVhN7VHqpRyDbK6tePb1KJGSBonCjOzrwmRyZjNDnf/U3KiHmCZEU82WS82H4GeNCBprpAIylUrlLS6QkNBbusOIO52bZvne+bf7WjGJPVfOkg++2/Vam6iohGi94U1RUnTrGtPuqXzKtkRHp4fv64GLJmwBQgGrzUOKepUQZbQrgAqWGv5SXLZs6apcxTaoBjKhiY1LMMUFtbq3AvWSxiv9BzGDx4sJr+TcrwHNrCBH1hNrwl6DyRMkKipy3Dkg5eNIQKpOM5uDmqHacnNBTsgnUhQq5GV+Wy4PeHIinvFEaz+UfXqacYCgmJRv3O4kUGM3byVKVl0eUNyxGNAIOuG0wPqMkkjHmLaQm1CLurVwRwCuyFF7UEk38Vcw6VoexmoizT4nQmPEwuHiTkSsAsgdNfBk59xoQCkGQD330hWSKPDzBOat6S5GFzXDuXxqZVxV9nryvAlDGwS+lN3pqt7i/+yhUGozCYk02gmLzFMRjg2xGNJse2tf9o8YdOp5O/wIZlwsPVgB5Hfd0WDnSSg2uId3nFs5qmUPw5h+BZZREDFNPQ0ADdreuwDMNKNOI4xhiQVrC6be8aoi+HAqRE3mv32W97SrnEN8N6jtImrHVXrZdrXaCALjU/Y0YXqQsBBmj9938HTlS1E2HYGF/r1f+RMnxuzN73dkSjSZwRtz0WI57Av3gAgkhzpC0qKhp7+5iZs2cLotGUQWNPMPFAVaAMrb5eqGDYV0Ea4txquLwYIhI7CArBR1oBucaSmev64HdhXyu0emfFIuuVtyo0eOQAuQasodxXgMgvBhMTDobbTifqgi84JMsMdibY+oF/9dRsYwJuvsealAdLzlBZHkwZ7YlGU1NKKQPN5VlZtefswXLykcsjXYSRmBJ5oIlOg2MNpSEO3ZHvbyjdsLG0FGv12Px8TaXz2EyrvYsgFn3wvtZTg0E29lMKAsJXa15cR6Nv4+QaNXUI1UBlJT2VgAjTa/o25z8XkCbZoYDv2w/8J/Yl/+RB5dYCDDuoTAVr5QIjGFZ+MZhtYX+br2GvdcB4+iEmarcODr5QzZtoYVNvfXmopeF80B0L/Mjmy29SWR5MDQQSjVol2WjvK3kFWK5bzoWxQBIWb5B9mbnqqF/8OcaK4IAgLAcQh36BfQvnP4YXrXfJ+bOp1ymqCWbRkuL828eQ7EqNxhroj1GDgHABUM1ooYZ4uEa5DqEmRK2kpwl4oGkT/0RyeXc+A7kG0k3b/pc8h15Pvfk/lTUpGhih1MzEYBTukpHdfb3mpCB/57ZVUeIXk82YlpU0eEr8IowA54lGk2NbwRVH10ZvfZmkKyEvx/rafndjc1BQcM8i6gmFhToGcyyZdsePC/QdWsiKOefPJnE0GzaAxJS1qiw2EUEhKkcO2KvF6aTBzQpcw9mSsrL6KUQP84VESaQ5HIlNiUwgiGgz7XUSqP7N+1D7Q66T0KSMKZcq0w3bEnuBpX8e1ioxg4R55kkK61V53ZdoxD1nOgjBM/Xew+/6vvonUWnb8wvpyWlLsw2aFLVIe8zgEY22XnHSBhrMh717PlKw6uVeaRl4mbmkzLVmRjr/c4GnHPpObW3tmpdeVH9JFDRvSM6OS/3ZDMuGVKvasX27Ah3gdmJw+hIrz3MrQDTLSkqUpZXzXNMvS04qUXY8jxg5AoohbqdLmZb5wMC15y0Jj1rgev8pIjIHfZRuDCZrcu5Dtmsnyo1sUrdowjBalYov7RqTM3AEfskrKwk+7pDW1x2N5Nw5HRofBNHBf+wjz5dvB5vqmaC3nf2FlV8YSzKp7HPd1I7O5Ggn0ajfTfDr4H4CZ+owJi5trdq37xN39RGFfZdMsM9+9ezSMidXsEbQnZJ4l9l4lhgiREA0EAGiupyoVpU/Lh90oBB/HPPs5awwxCatGNHDcQ0jU+wOVLJm1WpGhq+oDUsuj4kPGg/dQRXXo4LtePsH0heJbQ4H6SYcCpB645++au77k5Rhj8g5jzABohpKsY1Cs+0uC8zwDrLOYC6T0qjQUdw/CIUX1kttdOBXLbBePabTMsUiRKM1Y/uHP4+X/HyojdS8UYjKA7L7mIsn2F+oaJ16tGnOrSl52bbe2Xl0WSO1Dti6CvdOmRxbOZi9H++lBVkWFS2M6nLCZviqRT43Kh5wVpioET34tvZQjRzXjB03DrQrZyrC9ZMuCKtWK9u5Sd2sooX4d0dFBYiY6JLR3PAdAQxr+9hl/EhTiPH+o7tbTuxnzGqXVlPGAEg0AjUfMzbsc3Z0j9oEAsyYWJY5Ty6eZoFzmqEVlE02U6+rkgZPsVxxc+fniJu5q0zI4TLTTdCMyg54lKPyCoYmYYP1u9qgQ5UwGDEb2RfBoEGD1qxbF9uqW8sWl6PN0t54602qHOHF+pvGiRuSLF9agu0VKpDH6YvhGCRquUywaouzRZJrQAfDR4zYwN6F5L6Li4sJpcqbhGiIIP6lHR22bdlKsy46rqWMMjDKU295Ei/f4ffcn20kJkmI9AGv58Amb80WyjgK5cQNNlbpDgUFn6eMWpB0/XTPF5t9Ryq7bA4nK7zngWLiFyWCZ+r9J/b5T1YHvjtI+j2IDaxGM/jFmNpHU3n2DsI5oklECyeKgqG2V3a1ucaGlSvsgZKgOi0Yaz+SPh4Di36Y1a8ff5nVanqgehOd0lQ5gjhDqsyxvVMY1rDKHR+s5HQ6oaApiBs4YJzlr8AgDQ0NaqpeUGuOJCuRG7n/AbmLoZmiOMW9EyfhfvkdrLALRCHQCt2Gfk49cfRz/CwdIeBQVTrYVE9TB0jOgT1TPNCtA8bjJQziYBkHsonBkiK5AluzRpD+ByE/7UzGPyBbnmYBLU+BC6BVgYPOuOorxQC2rIyDb14gXnmrXX29dEmAWYJnv/E17CViC7+ZDB8gF6PJkHKpuc8NmFZdJ0GMk2gSZrSfMDQJRLN5vxtqUdSN0y/PHj35acmvqLSvqRA39CaILfxPMME4lxNmHdsxNqIoYWph3irMLhBEbW1t4fz56i9AEstXPEvC854siprZRLlGzErEGZ+fj19DzmWOjXd88P6ykhKarsX/ilbMEuuP1C5Oave9SuKMSOfcBBGNq6JIsmCw0d6XVMO6frpgsmEGJg97GC8Qh+fQFt+RneHW0+GgF4pV4NTnrlOfG8zJjNlmvmyIpW+u9eoxmEXYKxwKgEHk5m2kBj5vmtEy5gF2nIN9QiwDxtm8jHBHBmFPeiJzZm6cVMIH5Ur/sY+CzUdImypPM65XgllYmwtjshhTLu0KkoscCNHEWYpcAAgy04Ynv7bPDdFGrmkcBeuPeEruW8wrrfkHOyoqFApEcUnbKkHrnMevWVBpgmo3UdvRgWsKW+aKuQZshSPMe3SuXOPKSDG9FQy//bZAQhQD39bU1LC1MhKmQMmVJceUhpyCl7lvLnQH8WJL5JERhXhFfCU1W4Nnv2V9JW4IO/6ju/FyV72MRZsxGJhwGIKP0epQOa+IVKWlUKZcfwVz75yOMHDQ07GxLcdDrd+xAktIbGqhoC2rIDSZM4dBvjP3Gdr1q3+xRJM4vYkCskxlnW/hFueGh3rJbUMCugrWyo0S4njW2NeJRhInpNAvwypWtCFBQo7GaTdq0g6oBCTgGvVH0JSDit953969CSk0oR6Bk9XO8kLqc5GcIcSQwWpVzLm2tr4jlUT9CYfZuRdZ1UFGzQ17DeYkEgaSnGG6dJDJ3seceWNCxApKTAkHNYbiykOes8HTtWF/G5sPaAgHPOdr7opAqC0UNDoycY/dhVkEiEg0CT/ukgn2wk3NfB82H8osQ93bDoe27Jsd2ysuT1DNBFqSilThS1xzNcx/Iq3MnQslRbnIDscpAq7Bm8jnx+9f++K6+ANn6O8877HHEsXOFBBY1ITP0a47Cu3iKDCjkobOoLkLVJvAXr7D75FpCbnmnBE0iHn7w9dkh+pX2QXfwhiMBgtR3mnMl5ltOMew2VIdpFxwQhDtzEs/pPE+IBTaMjyq6hC5eCKwpJr73EAvu+uYWmKGOc5S5HLI7mN+PD+1pIz8rAvGtmu9YsoY6Bi3Qo6SuX4AWVn9NPV1yuqXdaKhAfpF4fzoPWcVgJMWslU41yaiTxsfpPZN8WLCoazlSGHL8/JLe8sO5RooUFGbZyvD2dKyZtVqiGy0+E5sB5GD0aZBs3BXr/cc3Jwy6nE1WYWc5cXF1n41mKz28c8T6cB5IsBKB5jMEA1YQwaRemihJto593w0MfHF8OquCf7UAo4+In8qSiVC0POGgoYk4kQDG9I+4gm08nQpmDsuwbpgaJLdZiwpc9afal6Qn5p7pQUDJWnIdLnwAWr9hcaE0b9oSTGJUtMCiAljx41bs2oV7aMg8L+oQUNDA3Z/Y+u2sfn5WutLqL/Impqa5UtLojq8BXINn2veeOtNWsgCMtcsctca6AY/ckVFBS4A72MOI1CGKSOH0dI9jhTTZ9vFRe1OyeGc4ymIaWlpX16LihVUN6H2DnIKTzNjNGF7onmJPMHSOb5x47yYdo7L+LJVxwlWXRPmhBto+MjLsWb36QW5BmrUjUOumjJj9riBd4s3o/U0ofsw7Uf/vr17JXsGyYGtv7uicP78ja+WUv8LLQlMZvV1sl4V8At1A9NgtqhlaKJelbJpCVcI3WfyxEkK2wgg4Bpq96W1BKl/Oj8/n0QPRbtHepsMK1stLi5Wz6SanoK1f14MQbq0oWDbnheIU7Z/nkBZIJaaI5Vc8qG5z1CDyRIO+gOnDljbr/8KRl+qedH3lIm4r6i5ROs1M6JcHDAgdYox3bBDU4fCnEDHNh/U84cFZ2Bm7l8eGbb/4NGNpRuKi3+HF9+SggW2traWYeP0qMrDjX7W/azBGMwBB6fl8mgK5Y7t27mSTjiLYHaBNegbSDFqKl2puaq0aBMYSln8dTPBLBv/ZyQYBDdYsZ10v6OfC2iO+4UZ9h5JSrrqWhkMm+KgxlTMr+aBqW60943NbUxialjPFMOrEEAtPtC4OaIhn5usTNDPLxYRFVTzou91FuhkmO35iXGsnD+ilP9vxMhMrtQ+P5iFYRs5SS7F8VfD5VIoGSbi9N378V7BNjRQTb01NCE1emnXujgPwl0P1DFqXcZv62xxiikM5ALui61JVmyXSlrixp15BKris1Ww6avWyqVcrrPB6gj7WgMn9rffpTHsdXWOSkKMm85GEonjPMnEFJuTfu+GnqM9mTuZ2rUGsyQKdJpdkFN3GujdXZCcSQFsOQUdkeLorS+3sM1bGFZn8dWXh9xN/A3APs3bZoGMEttRgAv2C+NfvI8vzI+DQle8iw/Svbd16IgHbMp1QUJ6VAoAocYyfSvt5EPsweEQv0sPzSRyVSxktaRcU0aOOTNXvdmVOkZo5x8mEsjq7OT0hYsVOtHo6BBYO4Zo2MSozTSw2GBOCgc83sPvpvB4BF/5jlRiMxKgjFd7CyTNQuL+TJRsoiMqdKLR0SEAEaiM3NMKz8HNNMbPkNQr7Drp+/Z9fgMp0lChYG1L+TzJ0LguJZ4IimRf3NCJRkdHAcKFs1y6h1w8YKWVnbbsCdar/8NzYFO47YxgA1J1uGCts6Koi0srF2Vgnhx0otHRUYBQY+0/Wi7HMh5QR49twB2QbsJBn+/wewInN7gmffLGtj0rO0J9Swgg7l3oS+hU6ESjowORMmpBRxCNgc1yIEVeLKlhb4vny7fF0TSkvFbekqQh093Vf+qIa4gT5h7j2KbQiUZHB6Ij3E+QBbhMBVOvqwKnPg+eqZXbGGRkz19BKt18QTp8d532T2r6L15M0IlGR8ciedicRBGN0d5XkA+VNHiK69TnYb+b7+SW2jFSec93pNJ/pJK6pRJySbGBuuEv4AV0PnSi0dGxwCSP3/0kphgKaEyGj35PutNV/dEx7vdRj2Nl4/1SSXWuC8k4PY1lGJ1odHQC4nE/yVEMw0bB+BurDBY7aaVw/N+tlUsJqWXmqgl25xiHtFWrL+vkeubWc0Wyew50otHR4cDMjy3N0pZdINnIEfzirlofcjcZrSmk/BUQCgW+P2QK+gInP23d+f9Zr7o9ediv1NTchMKV0ptoVZ3GOPgpemBKp040OjoDEB+0Nl2SZBm2eM0zgaavmYAb8kvI10L+bTvNhAKk1G7ASxxSRpP/5Ke+rfebMwbYhv5S5azmGAcs5mOr/2m6WvVIur5TO213EehEo6MzAN1HE9GYMgZizgs+hNDh2vk0OMWY1CvU5rcNvpeqVM7t/9t/dDcTDKRP3kA6Bxyp9H3zvjH1ssDZb4O7ngX7JN/0iMqSWgxtxZs5DPpa256VCfeLs1laaq/kYoJONDo6A7SxkXrLqz3vKYHiA5YBoRhs6eGAy3rNOH6dxpRhj7Q07AsHfd4v30oaOgM0AZKCHuStI+nd4ZDP88Vrns//kvTj+9VPctJUM38FNKnWyqUJNBhDsuv8LpFdATrR6OgkYI6p1EeShkwT+Kox1SnLMAZT8o0PC/giwmLuJvdnG2kZc4aVofACPXkObvaf2B8OuGOgG1yzZfpWKGuJEm06qNl214dONDo6CebMXDVEw/aT+5XgQ+f2IpKr7W0WswwF+KVt75owqRa823LFLdzn4KDUvCUhV6O7ar3v6G6DyeKtecP9yctyx5G8Hog2YCscIU7RBvpgj8pv4kMnGh2dBGv/21qZkqibJQ0Rdl/BDGeCPiYUVGAH27UT3VUvhwNe96elfKKhwPQG3SRzdGM0Qbrx1r6dfNMjKk3FuCpL32Guymfiyf+25fRE6wyFTjQ6OglsOGz0HEtbTrsYEwgj3i/fIP2OfjRYwDKCkleWfj/1fftB4PtacU9uivN0s/8l37F/M0ZT64fLjamX2W9bokbQ4BI1tbrPOICqYtvxIoBONDo6D5b+ecpEY8suEMx5186lxADsbU5t3z25tXKp70hl+uQN3PYpIx/zHd3FhAKuD55Ju1u6JxfVfVJvf5rSjf/UgVDzkZZ3HrVcflPKqAVqzLTYzJw5LAYLMSnX38MSKfnQiUZH5wEiSdueFxSmqMBW6m+sCrmbDOFg8k2/5rOA5+Bmau6BfGHPjzQIJmHBP7oucOrzwOmDOIWYNSABGWz2s5unQrACX4BuiL9893OMtxknan7jQdvAO5XbZlJY++eZCtZqVaMsmT2rLoQAOtHo6FSwdRuk65Zj/gvEGWJ/9Z41OPrxlSZq2aXvIR+BIzg7i/323559/RdM0NcKAhIF+3Fl97AXXimjHsfFpE98GUdo/eBpxhD01r3l+6ZCjVuK1NaasE5TvRvSV68HQycaHZ0KiAykWpWUUJOc287ZRHoPkCQDR8rwR/mfC7w/mO3pkzfS9+Apk71vsPmo/5v3wyJVyIiveDIIZCucIjVvCXiq1y/eJH6lz/4Mzcvz6QY1dmJa7waMg+OouXGzLtHo0NFpoPPTVbFQ8Lktu0BgwsDMZ/ytTGof/oQntuH2QgS4w1tfxskgKbc86Xz3N+GA2/X+U447/sDf0uTIFCg79FA00QHSDWlHVfUn37cfhF0nSLsFNj5Y2U6MvUwZ2bidqCabHpjfxIdONDo6G9b+eUlDpvF9N2AfQcIB5q3/uwOMyZY0+F7+55zSJPjQ2v82Kr9gPpt6XRU8U+dv2C9wP0maovlcQy8DdNP60fOgKv+JT/yN1WBAZcMNzpg+eYNze5GCyQZaodxXPQQ60ei4AGBdPA7OWIN5LpjJviM7DaakcNDFN5ewZcklWsWHXCdpDxb6pyN/ObXUOCsWpd9bym0GMmqzSpii+VzDUPvLPS/hRO79LzJGK0ll+Pq9lOHzFIrIkNYLE9YpBBD3tHp6YuhEo+PCALxgzsyFMGLLKRDPYf+RyrD7jPWadl03wT5yGgr0LByHyi/E/XTpIFLis+kwP1AYXCZnihZwDUOTDzKHkQKgX/3TwBjc+1/yfL7JPua3cpoUDSCGwiVpHu6ZiZR86ESj44KB5ElPkFjqSa/IlgZjWj/B/PTWybp4sAs4i2MK++2/bd5yP9Sf1g+X95rxDrcZlCBIKJIVZ8RcwyZDzAF/uXYuZcLBYPOxlnceVa50I2kehvLVMxMp+dCJRkeXA2nbdO09kCb45uGQq1G5HiiYgphm2V0gd1iuHkObc7urXk4e9jDdhpqi5cr9ibmGiahFa0FP7k9eNvW6yndst++bHck3/VpOSKEpFK2VJdwZe2wiJR860ejocoDeZMrIEehTtDG2Mlr3rAQp0Pepoxb4j+4izVgO/C3p+l9wMgXEKIEpmg/KNeIoYdKvrv9tbXtWhtw/MIZIqlTqzf8pGeyLjc0ZOTRsJ2XU4z02kZIPnWh0dDkEncRbZGlPNIHG6OXNIfJwrm4ivPz0N67KpVCgWt75dfqUv3CbgUcCZ+rl5CNwTeBMnWPcCgFBUGmIRPdVLjXZLw86jzl3LLYNvFNSYKHdMoOuxh5Yh1wSOtHo6FoInqk3987BfBY4vNVINAwbhse5uq0Dxps+2xj84etg81FBN0vwSPO2mXLlgYNNXzVvmwUGgR4k+IpE903fSioWtzYyRqv/+B7ft+/bb/utWLTBJz05uUkAnWh0dC34T1aZM3MhUwg+V1kznBYV5hKgHON/T63Crg+XXXLFzZxChDeO/BVcUoLkccBZgcaqVFGtP4Z1mUWMxEFPOOiFaGO98haV5dB7JnSi0dG1gLltYV3L/A9VijMUbCpTJdVZoP4kDf25+/O/MEGfQIGi2o0C19BDBbbNBCWJZZPzRuL9fzQkX2J0ZLaUzQXX6LqSJHSi0dG1EHQ2WjCNHXEZUFsrl5rPVZBIHvaw9/B7oZYGKFBte9ekjDjvcmIb5j6r3HMKklTzG7NoBqb4W2okZhMXKsEy/iOVni82qyxw06OgE42OLgQqXIScJwUpiObe2lKf2RrDRWkT1lFdJu2uVTRW2Htoi/XK0ea+N3BbQnRKzSvmvNFyoBmYkjVraOICvm3ds9LkyEy6fjpOzeZJ6ZrUeehEo6MLgQTvXj8ds9renmhimLHBpq/a9qykQTGkpcHoxa6dzxB7SkVRr2mv8w9IvVRRuYZ6o+x5T0maeNnCEawmVfUnYo222aFJ4Y2aAjc9ATrR6OhawLQPNFabRKpTDA28QQ2gGOp+tg4Yb/7y7UDjJ2FvS0t5IT8HilHNNSCvlvJ5IC85QwzVpEjiQl2ZLWdC2OsC3YBr9BQEnWh0dDlItsG1XpWnlWgAd/V6o6Mvneck2fLv08Kes8Ezda27n0u95Un+liq5BkqZq2IhrdEnaYihocCsXLaSOumhVTVvm9nD6UYnGh3dA7QMaAw7gjtI+F/mMFBA+qT1EWPNl2+ZMwbaBk8RnIJRwTVMpLJfddKQ6XKaEY3uo8UAg87G5GG/6uF0oxONju4BNvdaNnVAGZBB0grWmnpnR4w1lUuZUKDt49WmSwbwDcOMFq4haZzV6/EiBWvO5VgJQPsuiOmmB9pudKLR0W2AieqtL4+hixt2aSmfR7nGOmB80pk6EBYxDL+7IO3uPwo4AlwTcp6UK2wsBi4JL3PfXFtOgaS0wqcbKFOgGHzYUjbX3DsnagW/iwY60ejoNmBrvkQJe5ED5RraniVlRGHg9JfEMBwA1zyRft/fBMIF5r+4ZqgyAier8SLldbInSEorHN14vtgMurHlTMA2JEvT62Ir8tx2cQs4OtHo6E6wZA5LGfV4bMYafnBNWsHq5td/Hmw+GnI3nX3tPoHDmzlnkNbENQwb3QdRyHNws2SeFMPSTcqoBbgSb30ZGAdCTRLbIJwKONbsgou1tLBONDq6GTCBMTO1UgAF8U+XzaVdE9Im/ok6ocLeluYtM9KnbkoI1zDn8qS8dWVyQTe01h9eviOVvvqyoLORFYIcvvpyyDjgGnPmsIsslUEnGh3dD5r6nAgArmmtXEqrFPf62WsQZ0A0obbTzVsfSJ/y50RxDT2RQu4CBdgEL+hTPjZ3gQYWM2xFHnfVnyDjmDNzwTsdZMcBIYZ9zs4xEulEo6NbArMXMySG1rQML5CPcM201yNc03oqsXINhUIKOAeS+ckKOKykVgbSMffOpjIOOAgyDkQeSjrmjJz4S0+QDhONVeAynMiWXSAox9FB0IlGR3cFqR8+fatC7wEFcIF8xF5z1+qWd34dDrgh18jZazDJ1fi8JaGQAi4ANkjpvQAzn8o43rpySByQaKzsdQab6j0HN9MCGpB9TBk5uAWQlNHqUD4yzX0PNFaTcqhn6lhBhrQMxg/YOSzD6ESjo1uD9h7ARHJXrdcaNwzigMhAGmn3zk67+yXnu0+E3E3EXvP6zx13/EHs82bUxddIgqaAJ+fOURk+w8k4DEsT4Aj8C9IhBuP+t4H1KFOAeih9QOTh9sVmDPllHNwnkI8CZ+qNNjs2w474k9qAOtOzrhONjm4P2k0hBrohRTnZ4Bq80u/7W0SHcje1vPOI446V4lg+zEw1TSnlQB1SpCXmuc4wakDuLnNYMkMytqBbBSGVNFYHm+pCXhfXIQ/cwWcWhqUbkAv3BhsYHZnJucMuVNE/nWh0XCSgdEPj4qB3qKEDfiAftddAnCFyTcDr/OeClJGPCXIUcAps7KwoUlnuT/KMNJ7YlDGQWHmJBpQNPUgl70TKg7Z3SOGY4oKEDFsaObaL7AjoRKPjogKNi0shPS13er7YrNCmloJyjT3/2Ugy1H1/aykvDJ6pCwe9rf/+70DTV4LcSyL7TN7o3F4UQ4YnH7gw8bXhAsA73J/qY2ooUXblGBydaHRchMCsg6ZjY2PhoK0oCzgkkK+8MDWvmNqG0+8tdb77hP/EJ0wo4K17J/D9obSCNXzDCrEfT1gLuUl9moJKENmEx18xcJm1/2hL/7wumLepE42OixlY6qmAQ2rEyPSopGitLPEfqaR+aMcdf3BXv+L57M+QayDdnH3tvrS7VgusG8nD5lj758WjRnUE2HrJxAfX1bhGJxodFz9ojRi8wDVte16Qk25I8YfNU2jxh+Tch8yXXuv615JwwEtqZb39SNKPZ3AdLymoGuWu+lNsOeUdBFu2dG7nhYVONDp6ELha4nLUQI210LYgrYBxev3i7eY3Hwy1NEC0cX/2Z9/R3WI1KmXUAhzWVflMVHtQJyBpyLQuZQPmoBONjp4FNdTAJj1Gij+k3rrI93WFt+4dJhQgatRf78En/F50zDnRRllc6mgY7X2hJHZZe7BONDp6Itha4uuiaj2Bk9XUTmy98hZS29zbAk0Kb8wHX3PcuVKyP7fni80QiDqTbkwZA7t+4T6daHT0UFDRxtI/jyQTKWo9kFN6Td/aa9rrrp1P+4/tYcLBwPeHfth0d8qNjyQNnSE4Ji0Y3Al0AxHGyjqYukXjXZ1odPRoQNdgDbrrFXiBZiFiVjvG/d5/bLersiTsOcsEfW37X/Icel2cxMS3PXvryuOMuBEA2pyFTSDoFvzCQScaHToivbRBN3JZ2sEz9TQe13LFLb1+9lrrnpW+w+8xoQBJYnr7YfNlQx35y8VJTDSWh2ZI+r6tjI1xoBmZe5OkbVNGdpc1wUSFTjQ6dBDQkGIihtSVK0fckEzOvCWhYXOcFYuCTYch2gQaPzn710mWq8ekSrWy5DIkaa4Al6kUbKoXyFDgFIPVQTMSCLOw/3bI3XY6dKLRoeM8aJ0avCDC+E9WkSRGNjFaXO8OW6bfWwpNqvWj50Ntp8MBt6++3P/NDtu1k+Sa4dIsge4rlcQDnWh06JBAJH0xGogmNf0Wb83Wtr1rwTXhgNdz8DXvl2/brr1H773Nh040OnTEC9vgKXh5DmxyV78SDvrAOKAbT+0b1mvyU3pMQxVl6ESjQ0dikDR0Bl6+w++17X8p5G5igj4oU3hZrrzFfttve7h0oxONDh2JhHXAeLyCZ+rdBzfbrh7DmJMDjZ9Q7/iFvrQLif8fSOvgZPhXOCIAAAAASUVORK5CYII='
						}
					}	
					pdfMake.createPdf(docDefinition).open();
				}
			}
		};
	})
	.filter('categoriesFilter', function(){
		return function(input, role_filter, selected_category){
			var output = [];

			if(role_filter.role === 'Giocatore'){
				angular.forEach(input, function(player){
					if(player.category === selected_category.category){
						output.push(player);
					}
				});
				return output;
			}
			else{
				return input;
			}
		};
	})
	.filter("emptyToStart", function () {
		return function (array, key) {
			if(!angular.isArray(array)) return;
			var present = array.filter(function (item) {
				return item[key];
			});
			var empty = array.filter(function (item) {
				return !item[key]
			});
			return empty.concat(present);
		}
	});