angular.module("app/views/manage-teams/modal/backdrop.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("app/views/manage-teams/modal/backdrop.html",
    "<div class=\"modal-backdrop\"\n" +
    "     uib-modal-animation-class=\"fade\"\n" +
    "     modal-in-class=\"in\"\n" +
    "     ng-style=\"{'z-index': 1040 + (index && 1 || 0) + index*10}\"\n" +
    "></div>\n" +
    "");
}]);
