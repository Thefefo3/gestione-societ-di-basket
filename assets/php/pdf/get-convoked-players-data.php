<?php
	require("../db_conf.php");
	session_start();
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);

	$match_id = mysqli_escape_string($link, $objData['match_id']);
	header('Content-Type: application/json');
	
	if($_SESSION['loggedIn'] == true && ($_SESSION['role'] == 'Amministratore' || $_SESSION['role'] == 'Allenatore')){
		$sql = "SELECT id_user FROM convoked WHERE id_match = $match_id";
		$result = mysqli_query($link, $sql);
		$convoked_users_ids = array();
		while($row = mysqli_fetch_array($result)){
			$convoked_users_ids[] = $row;
		}
		$data = array();
		for($i = 0; $i < count($convoked_users_ids); $i++){
			$user_id = mysqli_escape_string($link, $convoked_users_ids[$i]['id_user']);
			$sql = "SELECT * FROM user WHERE id = $user_id";
			$result = mysqli_query($link, $sql);
			while($row = mysqli_fetch_assoc($result)){
				$data[] = $row;
			}
		}

		echo json_encode($data);
	}
	else{
		$return['error'] = "Errore: non hai i permessi necessari per la seguente azione!";
		echo json_encode($return);
	}

	mysqli_close($link);
?>