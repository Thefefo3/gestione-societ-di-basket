<?php
	require("../db_conf.php");
	session_start();
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);
	$member_id = mysqli_escape_string($link, $objData);
	$return = array();

	if($_SESSION['loggedIn'] == true && ($_SESSION['role'] == 'Amministratore' || $_SESSION['role'] == 'Allenatore')){
		$today = mysqli_escape_string($link, date('Y-m-d'));
		$sql = "UPDATE user SET last_payed = '$today' WHERE id = $member_id";
		mysqli_query($link, $sql) or die(mysqli_error());
		$return['success'] = "La data del pagamento è stata aggiornata!";
		echo json_encode($return);
	}
	else{
		$return['error'] = "Errore: non hai i permessi necessari per la seguente azione!";
		echo json_encode($return);
	}

	mysqli_close($link);
?>