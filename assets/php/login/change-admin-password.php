<?php
	require("../db_conf.php");
	session_start();
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);

	$old_pass = $objData['old_password'];
	$password = $objData['new_password'];
	$username = mysqli_escape_string($link, $objData['username']);

	$return = array();
	header('Content-Type: application/json');
 
	if($_SESSION['loggedIn'] == true && ($_SESSION['role'] == 'Amministratore')){
		//check if old password corresponds
		$old_pass = mysqli_escape_string($link, md5($old_pass));
		$sql = "SELECT * FROM user WHERE username = '$username' AND password = '$old_pass'";
		$result = mysqli_query($link, $sql) or die(mysqli_error());
		$count = mysqli_num_rows($result);
		if($count == 1){ //password corresponds
			$password = md5($password);
			$password = mysqli_escape_string($link, $password);
			$sql = "UPDATE user SET password = '$password' WHERE username = '$username'";
			mysqli_query($link, $sql) or die(mysqli_error());;
			$return['success'] = "Operazione completata con successo";
		}	
		else{
			$return['error'] = "La vecchia password non è corretta!";
		}
		echo json_encode($return);
	}
	else{
		$return['error'] = "Non sei loggato o non hai i permessi per questa azione";
		echo json_encode($return);
	}

	mysqli_close($link);
?>