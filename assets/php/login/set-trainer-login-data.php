<?php
	require("../db_conf.php");
	session_start();
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);

	$username = mysqli_escape_string($link, $objData['username']);
	$password = $objData['password'];
	$trainer_id = mysqli_escape_string($link, $objData['trainer_id']);

	$return = array();
	header('Content-Type: application/json');


 
	if($_SESSION['loggedIn'] == true && ($_SESSION['role'] == 'Amministratore')){
		//check if username already exists
		$sql = "SELECT * FROM user WHERE username = '$username'";
		$result = mysqli_query($link, $sql) or die(mysqli_error());
		$count = mysqli_num_rows($result);
		if($count == 1){ //username already exists
			$return['error'] = "Il nome utente scelto è già in uso";
		}	
		else{
			$password = md5($password);
			$password = mysqli_escape_string($link, $password);
			$sql = "UPDATE user SET username = '$username', password = '$password' WHERE id = $trainer_id";
			mysqli_query($link, $sql) or die(mysqli_error());;
			$return['success'] = "Operazione completata con successo";
		}
		echo json_encode($return);
	}
	else{
		$return['error'] = "Non sei loggato o non hai i permessi per questa azione";
		echo json_encode($return);
	}

	mysqli_close($link);
?>