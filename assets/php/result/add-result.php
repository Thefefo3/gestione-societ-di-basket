<?php
	session_start();
	require("../db_conf.php");
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$images = $_FILES['file'];
	$result_data = $_POST['form_data'];
	$match_id = $_POST['match_id'];

	if($_SESSION['loggedIn'] == true && ($_SESSION['role'] == 'Amministratore' || $_SESSION['role'] == 'Allenatore')){
		$our_score = mysqli_escape_string($link, $result_data['our_score']);
		$their_score = mysqli_escape_string($link, $result_data['their_score']);
		$match_id = mysqli_escape_string($link, $_POST['match_id']);

		if(array_key_exists('comment', $result_data)){ //If comment exists
			$comment = mysqli_escape_string($link, $result_data['comment']);

			$sql = "INSERT INTO result VALUES(null, $match_id, $our_score, $their_score, '$comment')";
			mysqli_query($link, $sql) or die(mysqli_error($link));
		}
		else{
			$sql = "INSERT INTO result VALUES(null, $match_id, $our_score, $their_score, null)";
			mysqli_query($link, $sql) or die(mysqli_error($link));
		}

		$result_id = mysqli_escape_string($link, mysqli_insert_id($link));

		//upload images
		for ($i = 0; $i < count($images['name']); $i++) { 
			$image_name = mysqli_escape_string($link, $images['name'][$i]);
			$image_unique_name = uniqid('match_img_');
			if(!is_dir("../../img/matches_imgs/" . $_POST['match_id'])){
				mkdir("../../img/matches_imgs/" . $_POST['match_id']);
			}
			$img_dest = "../../img/matches_imgs/" . $_POST['match_id'] . "/" . $image_unique_name . ".jpg";
			move_uploaded_file($images['tmp_name'][$i], $img_dest);

			$sql = "INSERT INTO match_image VALUES($result_id, null, '$img_dest', '$image_name')";
			mysqli_query($link, $sql) or die(mysqli_error($link));
		}
	}
	else{
		$return['error'] = "Errore: non hai i permessi necessari per la seguente azione!";
		echo json_encode($return);
	}

	mysqli_close($link);
?>