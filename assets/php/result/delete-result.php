<?php
	session_start();
	require("../db_conf.php");
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);
	$match_id = mysqli_escape_string($link, $objData);
	$return = array();

	if($_SESSION['loggedIn'] == true && ($_SESSION['role'] == 'Amministratore' || $_SESSION['role'] == 'Allenatore')){
		$sql = "DELETE FROM result WHERE id_match = $match_id";
		mysqli_query($link, $sql) or die(mysqli_error($link));
		$return['success'] = "Operazione completata con successo";
		echo json_encode($return);
	}
	else{
		$return['error'] = "Errore: non hai i permessi necessari per la seguente azione!";
		echo json_encode($return);
	}

	mysqli_close($link);
?>