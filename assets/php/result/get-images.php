<?php
	session_start();
	require("../db_conf.php");
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);

	$result_id = mysqli_escape_string($link, $objData['result_id']);
	header('Content-Type: application/json');
	
	if($_SESSION['loggedIn'] == true && ($_SESSION['role'] == 'Amministratore' || $_SESSION['role'] == 'Allenatore')){
		$sql = "SELECT * FROM match_image WHERE id_result = $result_id";
		$result = mysqli_query($link, $sql);
		$data = array();
		while($row = mysqli_fetch_assoc($result)){
			$data[] = $row;
		} 
		echo json_encode($data);
	}
	else{
		$return['error'] = "Errore: non hai i permessi necessari per la seguente azione!";
		echo json_encode($return);
	}

	mysqli_close($link);
?>