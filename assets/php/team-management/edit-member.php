<?php
	require("../db_conf.php");
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$member = $_POST['data']['member'];

	$member_id = mysqli_escape_string($link, $member['id']);
	$member_first_name = mysqli_escape_string($link, $member['first_name']);
	$member_last_name = mysqli_escape_string($link, $member['last_name']);
	$member_address = mysqli_escape_string($link, $member['address']);
	$member_birth_year = mysqli_escape_string($link, $member['birth_year']);

	$sql = "Update user SET first_name = '$member_first_name', last_name = '$member_last_name', birth_year = $member_birth_year, address = '$member_address' where id = $member_id";

	if($member['role'] == "Giocatore"){
		$member_membership_nr = mysqli_escape_string($link, $member['membership_num']);
		$member_par_first_name = mysqli_escape_string($link, $member['par_fn']);
		$member_par_last_name = mysqli_escape_string($link, $member['par_ln']);
		$member_par_email = mysqli_escape_string($link, $member['par_email']);
		$member_par_mob_num = mysqli_escape_string($link, $member['par_mob_num']);

		//delete old pic
		$sql = "select pic_path from user where id = $member_id";
		$result = mysqli_fetch_assoc(mysqli_query($link, $sql));
		unlink($result['pic_path']);

		$member_pic_name = mysqli_escape_string($link, $_FILES['file']['name']);
		echo $member_pic_name;
		$member_pic_unique_name = uniqid('profile_pic_');
		$file_extension = pathinfo($member_pic_name, PATHINFO_EXTENSION); //save extension
		$pic_dest = "../../img/players_pics/" . $member_pic_unique_name . ".jpg";
		move_uploaded_file($_FILES['file']['tmp_name'], $pic_dest);
		$category;
		if((date("Y") - $member_birth_year) <= 8){
			$category = "U8";
		} 
		else if((date("Y") - $member_birth_year) <= 10 && (date("Y") - $member_birth_year) > 8){
			$category = "U10";
		}
		else if((date("Y") - $member_birth_year) > 10){
			$category = "U12";
		}

		if(isset($member['par_home_num'])){
			$member_par_home_num = mysqli_escape_string($link, $member['par_home_num']);
			$sql = "UPDATE user SET first_name = '$member_first_name', last_name = '$member_last_name', birth_year = $member_birth_year, address = '$member_address', membership_num = '$member_membership_nr', par_fn = '$member_par_first_name', par_ln = '$member_last_name', par_email = '$member_par_email', par_mob_num = '$member_par_mob_num', par_home_num = '$member_par_home_num', pic_path = '$pic_dest', pic_name = '$member_pic_name' where id = $member_id";
		}
		else{
			$sql = "UPDATE user SET first_name = '$member_first_name', last_name = '$member_last_name', birth_year = $member_birth_year,address = '$member_address', membership_num = '$member_membership_nr', par_fn = '$member_par_first_name', par_ln = '$member_last_name', par_email = '$member_par_email', par_mob_num = '$member_par_mob_num', pic_path = '$pic_dest', pic_name = '$member_pic_name' where id = $member_id";
		}
	}

	mysqli_query($link, $sql) or die(mysqli_error($link));
?>