<?php
	require("../db_conf.php");
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$sql = "SELECT * from user";
	$result = mysqli_query($link, $sql);
	$data = array();
	while($row = mysqli_fetch_assoc($result)){
		$data[] = $row;
	}
	header('Content-Type: application/json');
	echo json_encode($data);
?>