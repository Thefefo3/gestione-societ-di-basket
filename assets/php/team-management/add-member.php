<?php
	require("../db_conf.php");
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$member = $_POST['data']['member'];
	$role = mysqli_escape_string($link, $_POST['data']['role']);

	$member_first_name = mysqli_escape_string($link, $member['first_name']);
	$member_last_name = mysqli_escape_string($link, $member['last_name']);
	$member_address = mysqli_escape_string($link, $member['address']);
	$member_birth_year = mysqli_escape_string($link, $member['birth_year']);

	$sql = "INSERT INTO user (first_name, last_name, birth_year, address, role) VALUES ('$member_first_name', '$member_last_name', $member_birth_year, '$member_address', '$role')";

	if($role == "Giocatore"){
		$member_membership_nr = mysqli_escape_string($link, $member['membership_nr']);
		$member_par_first_name = mysqli_escape_string($link, $member['par_first_name']);
		$member_par_last_name = mysqli_escape_string($link, $member['par_last_name']);
		$member_par_email = mysqli_escape_string($link, $member['par_email']);
		$member_par_mob_num = mysqli_escape_string($link, $member['par_mob_num']);

		$member_pic_name = mysqli_escape_string($link, $_FILES['file']['name']);
		$member_pic_unique_name = uniqid('profile_pic_');
		$file_extension = pathinfo($member_pic_name, PATHINFO_EXTENSION); //save extension
		$pic_dest = "../../img/players_pics/" . $member_pic_unique_name . ".jpg";
		move_uploaded_file($_FILES['file']['tmp_name'], $pic_dest);
		$category;
		if((date("Y") - $member_birth_year) <= 8){
			$category = "U8";
		} 
		else if((date("Y") - $member_birth_year) <= 10 && (date("Y") - $member_birth_year) > 8){
			$category = "U10";
		}
		else if((date("Y") - $member_birth_year) > 10){
			$category = "U12";
		}

		if(isset($member['par_home_num'])){
			$member_par_home_num = mysqli_escape_string($link, $member['par_home_num']);
			$sql = "INSERT INTO user (first_name, last_name, birth_year, address, role, category, membership_num, par_fn, par_ln, par_email, par_mob_num, par_home_num, pic_path, pic_name) VALUES('$member_first_name', '$member_last_name', $member_birth_year, '$member_address', '$role', '$category', '$member_membership_nr', '$member_par_first_name', '$member_par_last_name', '$member_par_email', '$member_par_mob_num', '$member_par_home_num', '$pic_dest', '$member_pic_name')";
		}
		else{
			$sql = "INSERT INTO user (first_name, last_name, birth_year, address, role, category, membership_num, par_fn, par_ln, par_email, par_mob_num, pic_path, pic_name) VALUES('$member_first_name', '$member_last_name', $member_birth_year, '$member_address', '$role', '$category', '$member_membership_nr', '$member_par_first_name', '$member_par_last_name', '$member_par_email', '$member_par_mob_num', '$pic_dest', '$member_pic_name')";
		}
	}

	mysqli_query($link, $sql) or die(mysqli_error($link));


?>