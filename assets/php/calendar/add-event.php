<?php
	require("../db_conf.php");
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);

	$loggedIn = $objData['loggedIn'];
	$role = $objData['role'];
	$return = array();
	header('Content-Type: application/json');
 
	if($loggedIn == true && ($role == "Allenatore" || $role == 'Amministratore')){
		$opponent_id = mysqli_escape_string($link, $objData['opponent']);
		$category = mysqli_escape_string($link, $objData['category']);
		$event = $objData['event'];

		//format date
		$date = explode('T', $event['date'], 2); //split date
		$date = $date[0]; //take date before T
		$time = explode('T', $event['time']); //split time
		$time = $time[1]; //take time after T
		$time = explode('.', $time);
		$time = $time[0]; //take time before '.'
		$datetime = $date . " " . $time;
		$datetime = new DateTime($datetime);
		$datetime->modify('+1 day');
		$datetime->modify('+1 hour');
		$datetime = $datetime->format('Y-m-d H:i:s');

		$datetime = mysqli_escape_string($link, $datetime);
		$place = mysqli_escape_string($link, $event['place']);
		$who_does_what = mysqli_escape_string($link, $event['who_does_what']);

		$sql = "INSERT INTO game VALUES(null, $opponent_id, '$category', '$datetime', '$place', '$who_does_what')";
		mysqli_query($link, $sql) or die(mysqli_error($link));

		$return['success'] = "Operazione completata";
		echo json_encode($return);
	}
	else{
		$return['error'] = "Non sei loggato o non hai i permessi per questa azione";
		echo json_encode($return);
	}

	mysqli_close($link);
?>