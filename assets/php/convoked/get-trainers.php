<?php
	require("../db_conf.php");
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);
	header('Content-Type: application/json');

	$sql = "SELECT * FROM user WHERE role = 'Allenatore'";
	$result = mysqli_query($link, $sql);
	$data = array();
	while($row = mysqli_fetch_assoc($result)){
		$data[] = $row;
	} 
	echo json_encode($data);

	mysqli_close($link);
?>