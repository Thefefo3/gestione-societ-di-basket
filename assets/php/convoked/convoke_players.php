<?php
	require("../db_conf.php");
	session_start();
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);

	$convoked_players = $objData['convoked_players'];

	$match_id = $objData['match_id'];

	$return = array();
	header('Content-Type: application/json');


 
	if($_SESSION['loggedIn'] == true && ($_SESSION['role'] == 'Amministratore' || $_SESSION['role'] == 'Allenatore')){
		//verify if a player has already been added to the convoked table
		$match_id = mysqli_escape_string($link, $match_id);
		if(empty($convoked_players)){	//if none is convoked
			$sql = "DELETE FROM convoked WHERE id_match = $match_id";
			mysqli_query($link, $sql) or die(mysqli_error($link));
		}
		else {
			$sql = "DELETE FROM convoked WHERE id_match = $match_id";
			mysqli_query($link, $sql) or die(mysqli_error($link));
			for($i = 0; $i < count($convoked_players); $i++){
				$id_user = mysqli_escape_string($link, $convoked_players[$i]['player']['id']);
				$time_of_meet = mysqli_escape_string($link, $convoked_players[$i]['time']);
				$place_of_meet = mysqli_escape_string($link, $convoked_players[$i]['place']);
				$sql = "INSERT INTO convoked VALUES($id_user, $match_id, '$time_of_meet', '$place_of_meet')";
				mysqli_query($link, $sql) or die(mysqli_error($link));
			}
		}
		$return['success'] = "Operazione completata con successo";
		echo json_encode($return);
	}
	else{
		$return['error'] = "Non sei loggato o non hai i permessi per questa azione";
		echo json_encode($return);
	}

	mysqli_close($link);
?>