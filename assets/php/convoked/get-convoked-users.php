<?php
	require("../db_conf.php");
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);

	$match_id = mysqli_escape_string($link, $objData['match_id']);
	header('Content-Type: application/json');
	
	$sql = "SELECT * FROM convoked WHERE id_match = $match_id";
	$result = mysqli_query($link, $sql);
	$data = array();
	while($row = mysqli_fetch_assoc($result)){
		$data[] = $row;
	} 
	echo json_encode($data);

	mysqli_close($link);
?>