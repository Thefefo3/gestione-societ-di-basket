<?php
	require("../db_conf.php");
	error_reporting(-1);
ini_set('display_errors', 'On');
set_error_handler("var_dump");
	session_start();
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());

	$data = file_get_contents("php://input");
	$objData = json_decode($data, true);

	$subject = $objData['subject'];
	$message = $objData['message'];
	$players = $objData['players'];
	$emails = "";
	$return = array();
	header('Content-Type: application/json');
	
	if($_SESSION['loggedIn'] == true && ($_SESSION['role'] == 'Amministratore' || $_SESSION['role'] == 'Allenatore')){
		for ($i = 0; $i < count($players); $i++) { 
			if($players[$i]['convoked'] == 1){
				$emails .= $players[$i]['user']['par_email'] . ', ';
			}
		}
		$emails = rtrim(trim($emails), ",");	//remove last comma
		
		//send email
		if(mail($emails, $subject, $message)){
			$return['success'] = "Email inviata con successo!";
			echo json_encode($return);
		}
		print_r(error_get_last());
		
		
	}
	else{
		$return['error'] = "Errore: non hai i permessi necessari per la seguente azione!";
		echo json_encode($return);
	}

	mysqli_close($link);
?>