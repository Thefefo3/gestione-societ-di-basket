angular
	.module('gsb', [
		'ngAnimate',
		'ui.router',
		'ngCookies',
		'ui.bootstrap',
		'mwl.calendar',
		'ngImgCrop',
		'ngFileUpload',
		'jkuri.gallery'
	])
	.config(function($stateProvider, $urlRouterProvider, calendarConfig) {
		$urlRouterProvider
			.otherwise('/home');
		$stateProvider
			.state('container', {
				template: '<homepage></homepage>'
			})
			.state('container.home-view', {
				url: '/home',
				template: '<home-view></home-view>'
			})
			.state('container.manage-teams', {
				url: '/gestione-squadre',
				template: '<manage-teams></manage-teams>'
			})
			.state('container.calendar', {
				url: '/calendario-partite',
				template: '<calendar></calendar>'
			})
			.state('container.administration', {
				url: '/amministrazione',
				template: '<administration></administration>'
			});

		//Calendar config
		calendarConfig.i18nStrings.weekNumber = '';
		calendarConfig.dateFormatter = 'moment';
	})
	.run(function($rootScope, $location, $cookieStore, $http, $state){
		$rootScope.globals = $cookieStore.get('globals') || {};
		if($rootScope.globals.currentUser){
			$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
		}
		$rootScope.$on('$stateChangeStart', function(event, toState){
			if($rootScope.globals.currentUser){
				if($rootScope.globals.currentUser.loggedIn == true && $rootScope.globals.currentUser.role == 'Allenatore' && toState.name == 'container.administration'){
					Materialize.toast('Non hai i permessi per accedere a questa pagina!', 3000);
					event.preventDefault();
				}
			}
			else{
				if(toState.name != 'container.home-view' && toState.name != 'container.calendar'){
					Materialize.toast('Non hai i permessi per accedere a questa pagina!', 3000);
					event.preventDefault();
				}
			}
		});
	});
angular
	.module('gsb')
	.factory('LoginService', function($http, $rootScope, $cookieStore, Base64){
		var loginService = {};

		loginService.clearCredentials = function(){
			$rootScope.globals = {};
			$cookieStore.remove('globals');
			$http.defaults.headers.common.Authorization = 'Basic ';
		}

		loginService.setCredentials = function(username, password, status, role){
			var authdata = Base64.encode(username + ":" + password);
			$rootScope.globals = {
				currentUser: {
					username: username,
					loggedIn: status,
					role: role,
					authdata: authdata
				}
			}
			$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
			$cookieStore.put('globals', $rootScope.globals);
		}

		loginService.login = function(credentials, callback){
			$http.post('assets/php/login/login.php', credentials)
			.success(function(res){
				callback(res);
			});
		}

		loginService.logout = function(callback){
			$http.get('assets/php/login/logout.php')
			.success(function(res){
				callback(res);
			});
		}

		return loginService;
	})
	.factory('Base64', function() {
		/* jshint ignore:start */

		var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

		return {
			encode: function(input) {
				var output = "";
				var chr1, chr2, chr3 = "";
				var enc1, enc2, enc3, enc4 = "";
				var i = 0;

				do {
					chr1 = input.charCodeAt(i++);
					chr2 = input.charCodeAt(i++);
					chr3 = input.charCodeAt(i++);

					enc1 = chr1 >> 2;
					enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
					enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
					enc4 = chr3 & 63;

					if (isNaN(chr2)) {
						enc3 = enc4 = 64;
					} else if (isNaN(chr3)) {
						enc4 = 64;
					}

					output = output +
						keyStr.charAt(enc1) +
						keyStr.charAt(enc2) +
						keyStr.charAt(enc3) +
						keyStr.charAt(enc4);
					chr1 = chr2 = chr3 = "";
					enc1 = enc2 = enc3 = enc4 = "";
				} while (i < input.length);

				return output;
			},

			decode: function(input) {
				var output = "";
				var chr1, chr2, chr3 = "";
				var enc1, enc2, enc3, enc4 = "";
				var i = 0;

				// remove all characters that are not A-Z, a-z, 0-9, +, /, or =
				var base64test = /[^A-Za-z0-9\+\/\=]/g;
				if (base64test.exec(input)) {
					window.alert("There were invalid base64 characters in the input text.\n" +
						"Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
						"Expect errors in decoding.");
				}
				input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

				do {
					enc1 = keyStr.indexOf(input.charAt(i++));
					enc2 = keyStr.indexOf(input.charAt(i++));
					enc3 = keyStr.indexOf(input.charAt(i++));
					enc4 = keyStr.indexOf(input.charAt(i++));

					chr1 = (enc1 << 2) | (enc2 >> 4);
					chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
					chr3 = ((enc3 & 3) << 6) | enc4;

					output = output + String.fromCharCode(chr1);

					if (enc3 != 64) {
						output = output + String.fromCharCode(chr2);
					}
					if (enc4 != 64) {
						output = output + String.fromCharCode(chr3);
					}

					chr1 = chr2 = chr3 = "";
					enc1 = enc2 = enc3 = enc4 = "";

				} while (i < input.length);

				return output;
			}
		};
	});
angular
	.module('gsb')
	.directive('administration', function(){
		return {
			restrict: 'E',
			templateUrl: 'app/views/administration/administration.html',
			controller: function($scope, $http, $uibModal){
				$scope.trainers = [];

				getTrainers();
				//Get trainers
				function getTrainers(){
					$http.get('assets/php/convoked/get-trainers.php')
					.then(function(response){
						$scope.trainers = response.data;
					});
				}				

				$scope.changeAdminPass = function(username){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/administration/change-admin-password.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, $http){

							$scope.changePass = function(old_pass, new_pass){
								var post_data = { 'username':username, 'old_password':old_pass, 'new_password':new_pass };
								$http.post('assets/php/login/change-admin-password.php', post_data)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
				            			Materialize.toast(response.data['success'], 3000);
										$uibModalInstance.dismiss('cancel');
				            		}
								});
							}

							//Verify if both passwords are equals
							$scope.passwordMatchError = function(password, password_confirm){
								if(password === password_confirm){
									return false;
								}
								else{
									return true;
								}
							}

							//Close modal
							$scope.cancel = function() {
								$uibModalInstance.dismiss('cancel');
							};
						}
					});
				}

				$scope.setLoginData = function(trainer){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/administration/set-login-data.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, $http){
							$scope.trainer = trainer;


							$scope.setData = function(username, password, trainer_id){
								var post_data = { 'username':username, 'password':password, 'trainer_id':trainer_id };
								$http.post('assets/php/login/set-trainer-login-data.php', post_data)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
				            			Materialize.toast(response.data['success'], 3000);
				            			getTrainers();
										$uibModalInstance.dismiss('cancel');
				            		}
								});
							}

							//Verify if both passwords are equals
							$scope.passwordMatchError = function(password, password_confirm){
								if(password === password_confirm){
									return false;
								}
								else{
									return true;
								}
							}

							//Close modal
							$scope.cancel = function() {
								$uibModalInstance.dismiss('cancel');
							};
						}
					});
				}
			}
		};
	});
angular
	.module('gsb')
	.directive('calendar', function(){
		return {
			restrict: 'E',
			templateUrl: 'app/views/calendar/calendar.html',
			controller: function($scope, $uibModal, $rootScope, $http){
				$scope.calendarView = 'month';
				$scope.calendarTitle = "Calendario partite";
				$scope.calendarDate = moment();
				$scope.opponents = [];
				$scope.events = [];

		  		getOpponentsAndMatches();
				
		  		//On event click
				$scope.eventClicked = function(event){
					var modalInstance = $uibModal.open({
	            		animation: 'true',
			            templateUrl: 'app/views/calendar/manage-match.html',
			            windowClass: 'ng-gallery--modal',
			            controller: function($scope, $rootScope, $uibModalInstance, $http, $filter){
			            	$scope.match = event;
			            	$scope.players = [];
			            	$scope.trainers = [];
			            	$scope.convoked_trainers_view = [];
			            	$scope.convoked_players = [];
			            	$scope.convoked_players_view = [];
			            	$scope.match_result = null;
			            	$scope.match_images = [];

			            	getMatchResult();

			            	//Get players
							var post_data = { 
		            			'category': $scope.match.game.category
		            		};
			            	$http.post('assets/php/convoked/get-players-per-category.php', post_data)
			            	.then(function(response){
		            			$scope.players = response.data;

		            			//Get trainers
		            			$http.get('assets/php/convoked/get-trainers.php')
		            			.then(function(response){
		            				$scope.trainers = response.data;

		            				//Get convoked users
			            			var post_data = { 
				            			'match_id': $scope.match.game.id
				            		};
			            			$http.post('assets/php/convoked/get-convoked-users.php', post_data)
					            	.then(function(response){
				            			$scope.convoked_users = response.data;
				            			$scope.convoked_players_view = [];
				            			$scope.convoked_trainers_view = [];

				            			//Checking convoked players
				            			angular.forEach($scope.players, function(player){
				            				if(isInConvoked(player.id) == true){
				            					$scope.convoked_players_view.push({user: player, convoked: true});
				            				}else{
				            					$scope.convoked_players_view.push({user: player, convoked: false});
				            				}
				            			});
				            			//Checking convoked trainers
				            			angular.forEach($scope.trainers, function(trainer){
				            				if(isInConvoked(trainer.id) == true){
				            					$scope.convoked_trainers_view.push({user: trainer, convoked: true});
				            				}else{
				            					$scope.convoked_trainers_view.push({user: trainer, convoked: false});
				            				}
				            			});


				            			//Function to check if a user is already convoked
				            			function isInConvoked(input){
				            				var out = false;
				            				angular.forEach($scope.convoked_users, function(convoked_user){
				            					if(convoked_user.id_user === input){
				            						out = true;
				            					}
				            				});
				            				return out;
				            			}
					            	});
		            			});

		            			
			            	});

							//Change convoked list
							$scope.chooseConvoked = function(convoked_players, convoked_trainers, time, place){
								var format_time = $filter('date')(new Date(time), "HH:mm:ss");
								var true_list = [];	//Contains new convoked users
								for(var i = 0; i < convoked_players.length; i++){
									if(convoked_players[i].convoked == 1){
										true_list.push({
											player: convoked_players[i].user,
											time: format_time,
											place: place
										});
									}
								}
								for(var i = 0; i < convoked_trainers.length; i++){
									if(convoked_trainers[i].convoked == 1){
										true_list.push({
											player: convoked_trainers[i].user,
											time: format_time,
											place: place
										});
									}
								}
								var post_data = {
			            			'convoked_players': true_list,
			            			'match_id': $scope.match.game.id
			            		};
								$http.post('assets/php/convoked/convoke_players.php', post_data)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
										Materialize.toast(response.data['success'], 3000);
				            		}
								});
							}

			            	//Define result modal
			            	$scope.defineResult = function(match){
					  			var modalInstance = $uibModal.open({
					  				animation: 'true',
					  				templateUrl: 'app/views/calendar/define-result.html',
					  				controller: function($scope, $uibModalInstance, $http, Upload){
					  					$scope.match = match;
					  					$scope.result = {};
					  					
					  					//upload result data
					  					$scope.upload = function(result, images){
					  						Upload.upload({
					  							url:'assets/php/result/add-result.php',
					  							method: 'POST',
					  							file: images,
					  							sendFieldsAs: 'form',
					  							fields: {
					  								form_data: result,
					  								match_id: match.game.id
					  							}
					  						})
					  						.then(function(response){
					  							if(response.data.hasOwnProperty('error')){
							            			Materialize.toast(response.data['error'], 3000);
							            		}
							            		else{
							            			$uibModalInstance.dismiss('cancel');
							            		}
					  						});
					  					}
					  					//Close modal
										$scope.cancel = function () {
											$uibModalInstance.dismiss('cancel');
											getMatchResult();
										};
					  				},
					  				size: 'lg'
					  			});
					  		}

					  		//Delete result modal
					  		$scope.deleteResult = function(match){
					  			$http.post('assets/php/result/delete-result.php', match.game.id)
					  			.then(function(response){
					  				if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else{
				            			Materialize.toast(response.data['success'], 3000);
				            		}
				            		$uibModalInstance.dismiss('cancel');
					  			});
					  		}

					  		//Match PDF Modal
					  		$scope.matchPDF = function(){
					  			var modalInstance = $uibModal.open({
					  				animation: 'true',
					  				templateUrl: 'app/views/calendar/match-pdf.html',
					  				controller: function($scope, $uibModalInstance, $http, match, $filter){
					  					$scope.match = match;
					  					
					  					$scope.generate = function(min_shirt_numb){
											table_data = [];
											header = [];
											var body = [];
					  						var post_data = { 'match_id': $scope.match.game.id };
					  						$http.post('assets/php/pdf/get-convoked-players-data.php', post_data)
					  						.then(function(response){
					  							if(response.data.hasOwnProperty('error')){
							            			Materialize.toast(response.data['error'], 3000);
							            		}
							            		else{
							            			var convoked_users_data = response.data;
							            			var convoked_players_data = [];
							            			var convoked_trainers_data = [];
							            			var convoked_trainers_view = '';
							            			//Split trainers and players
							            			for(var i = 0; i < convoked_users_data.length; i++){
							            				if(convoked_users_data[i].role == 'Giocatore'){
							            					convoked_players_data.push(convoked_users_data[i]);
							            				}
							            				else{
							            					convoked_trainers_data.push(convoked_users_data[i]);
							            				}
							            			}
							            			//Prepare convoked_trainers_view
							            			for(var i = 0; i < convoked_trainers_data.length; i++){
							            				convoked_trainers_view += convoked_trainers_data[i].first_name + " " + convoked_trainers_data[i].last_name + ", ";
							            			}
													header.push({text: 'N. Tessera - Cognome - Nome', colSpan: 2, style: 'tableHeader'});
													header.push({});
													header.push({alignment: 'center', text: 'Maglia', style: 'tableHeader'});
													header.push({alignment: 'center', text: '1', style: 'tableHeader'});
													header.push({alignment: 'center', text: '2', style: 'tableHeader'});
													header.push({alignment: 'center', text: '3', style: 'tableHeader'});
													header.push({alignment: 'center', text: '4', style: 'tableHeader'});
													header.push({alignment: 'center', text: '5', style: 'tableHeader'});
													header.push({alignment: 'center', text: '6', style: 'tableHeader'});
													body.push(header);
							            			for(var i = 0; i < 12; i++){
							            				var dataRow = [];
							            				try{
								  							dataRow.push(
								  								{text: convoked_players_data[i].membership_num, style: 'dataStyle'},
								  								{text: convoked_players_data[i].first_name + " " + convoked_players_data[i].last_name, style: 'dataStyle'},
								  								{text: (min_shirt_numb + i).toString(), alignment: 'center', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'}
								  							);
								  						}catch(err){
								  							dataRow.push(
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'},
								  								{text: ' ', style: 'dataStyle'}
								  							);
								  						}
								  						body.push(dataRow);
							            			}


							  						var docDefinition = { 
														content: [
															{
																text: 'COMMISSIONE CANTONALE MINIBASKET'
															},
															{
																image: 'logo',
																width: 226,
																height: 105,
																alignment: 'right',
																margin: [0, 0, 0, 10]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'CATEGORIA'
																	},
																	{
																		margin: [40, -7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [250],
																			body: [
																				[$scope.match.game.category],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'DATA'
																	},
																	{
																		margin: [74, -7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [100],
																			body: [
																				[$filter('date')(new Date($scope.match.game.date), "dd/MM/yyyy")],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'INCONTRO'
																	},
																	{
																		margin: [45, -7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [300],
																			body: [
																				[$scope.match.opponent],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'ISTRUTTORI',
																		margin: [0, 13, 0, 0]
																	},
																	{
																		margin: [38	, 10, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [300],
																			body: [
																				[convoked_trainers_view],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																style: 'tableData',
																table: {
																	headerRows: 1,
																	widths: [100, 200, 50, '*', '*', '*', '*', '*', '*'],
																	body: body
																}
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'DATA'
																	},
																	{
																		margin: [74, 7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [100],
																			body: [
																				[''],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'INCONTRO'
																	},
																	{
																		margin: [45, 7, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [300],
																			body: [
																				[''],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]	
															},
															{
																columns: [
																	{
																		width: 'auto',
																		text: 'ISTRUTTORI',
																		margin: [0, 13, 0, 0]
																	},
																	{
																		margin: [38	, 20, 0, 0],
																		table: {
																			headerRows: 1,
																			widths: [300],
																			body: [
																				[''],
																				['']
																			]
																		},
																		layout: 'headerLineOnly'
																	}
																]
															},
															{
																style: 'tableData',
																table: {
																	headerRows: 1,
																	widths: [100, 200, 50, '*', '*', '*', '*', '*', '*'],
																	body: [
																		[
																			{text: 'N. Tessera - Cognome - Nome', colSpan: 2, style: 'tableHeader'},
																			{},
																			{alignment: 'center', text: 'Maglia', style: 'tableHeader'},
																			{alignment: 'center', text: '1', style: 'tableHeader'},
																			{alignment: 'center', text: '2', style: 'tableHeader'},
																			{alignment: 'center', text: '3', style: 'tableHeader'},
																			{alignment: 'center', text: '4', style: 'tableHeader'},	
																			{alignment: 'center', text: '5', style: 'tableHeader'},
																			{alignment: 'center', text: '6', style: 'tableHeader'}
																		],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' '],
																		[' ',' ',' ',' ',' ',' ',' ',' ',' ']
																	]
																}
															}
														],
														styles: {
															tableData: {
																margin: [0, 5, 0, 15]
															},
															dataStyle: {
																fontSize: 9
															},
															tableHeader: {
																fontSize: 13,
																bold: true
															}
														},
														images: {
															logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAABSCAYAAAD90P6tAAAABGdBTUEAALGPC/xhBQAACt1pQ0NQaWNjAAB4AZXBZ1hThxoA4O+c7JCwEiIg4yAbTDAogYDICCFEQCAiJBGVeHKACISYQdwTUcE6UBHBiVRFLVqtoNSJWynuPYp6cddiFReOa/WHz3PHj74vACspRacvQe0BivVmo0IWjylVaozWASgwgApCAA1uMqQOT8yEL+RSCWZSyOLhOwTg9RVA4IuL/KR0DIN/xgE3GM0ASDoADNQSJhwAKQOAIqvZYAZAugGAO7bQYAZAyQDANSpVagDUGQC4+UqVGgDtDwDcsUqVGgCNBQCutlivBUAzAcCgLdZqAdCjADCv1EJoAUgpAFBeqiOsAKRLAOBTZCnWAZDeAgC3mNCYAMgsAPAxE3gBAHkAALCMmQoJAHkwAJ2Vn6mQAJAHA9BZYzMVEgDyYAA6y0xMMMMXkhLDRKMuv8CMBeCBmFAsjsCSCGsRYTbz0zV4ocaoxSQlxQaNfiKAljDh8I2jSSGLx+RSiUgoFon4oQIhfOdoUsjiMblUIhKKRSJ+qEAI/5hSpcbgq5cZgAAAwmtXqtQY/AelSo3BVyX1ABE9AKTFSpUag6/GrgRomQPgfEOpUmPwlc86ALsygOazWsKEwzc8uVSCFZjNhsiQEKvVKtARuAAvgO94cqkEKzCbDZEhIVarVaAjcAFeAP+IljDh8I1ALpVgIqFYJOKHCoRYApGnsRSZMYUsHsNLikosRsxk0OAExsdMClk8fCeQSyWYSCgWifihAiGWQORpLEVmTCGLx/CSohKLETMZNDiB8TGTQhYP/5OWMOHwTX8FkUcYCT1OYFk6wqrT52OSEr1WZ9aV6DGdHpNLJSKhWCTihwqE8F1/BZFHGAk9TmBZOsKq0+djkhK9VmfWlegxnR6TSyUioVgk4ocKhPB/leoIK/yN0/AJuGMEYH+WC6Q/2oHMsQFSzhoAQJQqNQZ/S2FkQToAZHs+LCY0JvgGgf+CLgIA1KTLx+ALiSITwy3GUviKDF9QgAl2wAUX8ABvCAA+hEI4REEsSGEopEEmqGA04FAAxWAEK0yBmVAOlbAYlkMtrIUGaIQm2AktsA8Ow3E4A+fgMtyETuiCp9ANr6EXQRAawkY4iAviifgiwUgoEoEMQaRICqJAVEguko/oEQsyBZmNVCJVSC2yHmlEfkb2IoeRU8h55DpyF3mM/IW8R0koC+Wi7qgfGoJGoHFoMpqJjkLz0fHoJLQMXYjWoPXoNrQZPYyeQS+jnehTtIcEJBsSj+RF4pMiSBJSGklNyiMZSdNIFaRqUj2pidRKOkG6SOokPSO9I1PJHDJG5pOjyEnkEWScPJ48jbyAXEveTG4mHyVfJN8ld5M/UdgUN0owJZIipygp+RQrpZxSTdlI2U05RrlM6aK8plKpPKo/NZyaRFVRx1EnUxdQV1O3Uw9Rz1PvU3toNJoLLZgWTUujaWhmWjltJW0b7SDtAq2L9pZuQ/ekh9IT6Wq6nj6LXk3fQj9Av0B/SO9l2DN8GZGMNIaWMZGxiNHAaGWcZXQxepkOTH9mNDOTOY45k1nDbGIeY95ivrSxselnI7bJsNHZzLCpsdlhc9Lmrs07liMriCVh5bAsrIWsTaxDrOusl2w2248dy1azzeyF7Eb2EfYd9ltbjq3AVm6rtZ1uW2fbbHvB9rkdw87XLs5utN0ku2q7XXZn7Z7ZM+z97CX2Gvtp9nX2e+2v2vc4cByEDmkOxQ4LHLY4nHJ45Ehz9HOUOmodyxw3OB5xvM8hcbw5Eg7Omc1p4BzjdHGpXH+unDuOW8n9idvB7XZydBrklOU0wanOab9TJ4/E8+PJeUW8RbydvCu8933c+8T1IfrM79PU50KfN859nWOdCecK5+3Ol53fu2AuUpdClyUuLS63XcmuQa4ZrlbXNa7HXJ/15faN6ov3rei7s+8NN9QtyE3hNtltg1u7W4+7h7vM3eC+0v2I+zMPnkesxziPZR4HPB57cjyHeOo8l3ke9HyCOWFxWBFWgx3Fur3cvJK8LF7rvTq8evv59xvRb1a/7f1uezO9I7zzvJd5t3l3+3j6pPpM8dnqc8OX4RvhW+C7wveE7xs/f79sv7l+LX6P/J395f6T/Lf63wpgB8QEjA+oD7gUSA2MCCwMXB14LggNCgsqCKoLOhuMBouCdcGrg8/3p/QX99f3r+9/lc/ix/FL+Vv5dwU8QYpglqBF8DzEJ0QdsiTkRMinAWEDigY0DLgpdBQOFc4Stgr/Cg0KxUPrQi8NZA9MHDh94J6BLwYFDyIGrRl0LYwTlho2N6wt7KMoXGQUNYkeh/uE54avCr8awY1Ij1gQcVJMEceLp4v3id9FiiLNkTsj/4ziRxVGbYl6NNh/MDG4YfD96H7Rmuj10Z1DsCG5Q9YN6YzxitHE1Mfci/WO1cZujH0YFxg3Lm5b3PP4AfHG+N3xbySRkqmSQwmkBFlCRUKH1FE6QlorvZPYLzE/cWtityxMNll2KImSlJy0JOmq3F2Oyxvl3UPDh04dejSZlTw8uTb5XkpQijGlNRVNHZq6NPXWMN9h+mEtaZAmT1uadjvdP318+q8Z1Iz0jLqMBwqhYorixHDO8DHDtwx/nRmfuSjz5oiAEZYRbVl2WTlZjVlvshOyq7I7lSHKqcozKleVTrVHTVNnqTeqe0ZKRy4f2ZUTllOec2WU/6gJo06Ndh1dNHr/GLsxmjG7cim52blbcj9o0jT1mp6x8rGrxnbjEnwF/lQbq12mfUxEE1XEw7zovKq8R/nR+UvzHxfEFFQXPNNJdLW6F+OSxq0d96YwrXBT4eei7KLtxfTi3OK9ekd9of5oiUfJhJLzhmBDuaFzfOT45eO7jcnGjSbENMq0x8w1G8ztlgDLHMvd0iGldaVvrVnWXRMcJugntE8Mmjh/4sNJiZN+nEyejE9um+I1ZeaUu1Pjpq6fhkwbO61tuvf0suldM2QzNs9kziyc+dusAbOqZr2anT27tcy9bEbZ/TmyOVvLbcuN5VfnRs1dO488TzevY/7A+Svnf6rQVpyuHFBZXflhAb7g9A/CH2p++Lwwb2HHItGiNYupi/WLryyJWbK5yqFqUtX9palLm5dhyyqWvVo+Zvmp6kHVa1cwV1hWdNak1OxZ6bNy8coPtQW1l+vi67avcls1f9Wb1drVF9bErmla6762cu37dbp119bL1jfX+9VXb6BuKN3woCGr4cSPET82bnTdWLnx4yb9ps7Nis1HG8MbG7e4bVm0Fd1q2fp4W862cz8l/LSnid+0fjtve+UO2GHZ8eTn3J+v7Eze2bYrYlfTL76/rNrN2V3RjDRPbO5uKWjp3KPac37v0L1trVGtu38V/Lppn9e+uv1O+xcdYB4oO/D54KSDPYcMh54dzj98v21M280jyiOXjmYc7TiWfOzk8cTjR07EnTh4MvrkvlORp/aejjjdckZ0prk9rH33b2G/7e4QdTSfDT+755z4XOv5wecPXIi5cPhiwsXjl+SXzlwedvn8lRFXrl3Nudp5TXvt0fWi6y9ulN7ovTnjFuVWxW3729V33O7U/x74+/ZOUef+uwl32+8Nv3fzPn7/6b9M//rQVfaA/aD6oefDxkehj/Y9Tnx87snIJ11PDU97n5X/4fDHqucBz3/5M/bP9m5ld9cL44vPfy146fJy06tBr9p60nvuvC5+3fum4q3L283vIt6deJ/9/mGv9QPtQ83HwI+tn5I/3fpc/PnzvwEDmPP8DKo7+QAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABcRAAAXEQHKJvM/AABDh0lEQVR4Ae3BB4BeVZ3w4d//nHvvW+adnpl0khASQhIgIB0VaRYQQQHFBitYEbF+a8GOuu7qqiiwigpIEbAiCCK9KIhAaElI731mMvWt957z/3InOyEhCQlNv283zyO6Ebvt9j+EYbfd/gcx7Lbb/yABu+32YngF78EaEAHnGGQtqIL3oArGgDH8o4huxG677SrvQRWsZZAqeA/WMsgrGGEr3oMqWMsrLWC33XaFKngPxoAxxM8soPqXRzDDmpEgIH5yNrVHniRZvBzJ5wj23INwv33Ivfl4wn2nMMh5EMAYXimiG7Hbbs/He1AFa4mfnEPvV/8TKdQRTptMPGsulZvvIOlbCWIRDQBFiQFFTD251x1F/ac+SPaEYxjkFazhlSC6EbvttiPegwg4T89nvkb/j35G0398Gcnn6f3Kf5B0LMVIAWwGUPAKAogAAs6h9KPqyJ94Ks0/vJBgzz3AObCWl5voRuy22/Y4D9bgVq+n483vIJ43l/Zb/0Dxl7+l/7IfIKYJY7Koc6DKDlmLiOCTHkxDG203XkXm6CPAObCWl5PoRrwUzoEqLysRsBZUwTl2iQhYy3YlDlCen0Bg2YYqOMcuMQaMYVDiAOV5GQPGsJn34D07ZSwYYStewTt2ibUgwvPyHozBLVvFuqPeTLJuBSP/9hd6v/VdBn51BUE4Fk0SUGWHjAFjwHvUV4AELwOghvZf/578aSdA4iCwvFwCXiprecWIQBDwoqmCKgSWXeI9iIAIm4lAEPCCBZadUmUrxoAxvGDegwgEAbvEewaJsF2qYAy+p4/1b3oHtWVPM+rhR+m/9KcM/OpygmgcWquxXSJgLTiH+j7UVxDqCEaMJzxgKtFB+2NHj0QyIb5zA2ZYC6iCCC8H0Y14MVRBhL5vX0wydyFSlwcEUF4cARQtlggmT6ThCx8jWbKcvgt/gEQhGAvCcwigaKlMMGEPGr78STZTBVUQoe/bl5AsWorksuA9WzEGLVcIJo6n4XMfBVUQAVUwhvjJOfR/7zIknwVj2JYAivb2k33j68i/51S0v0jvl7+D7x9AoghEAGUzMWh/P4WPvo/okBlokiBBQOma31G57R6ksR4QQNmKgvb1U/eh95B5zaHgPaiCtcTzFjHwnz+BMGC7RCBJkLo8DV/6BKapEbwHY9iG92AMXWd9jL6rLqbli98mGD+Wjvf/C0E4Co1jtiECxoKv4rUHMQ1kDj2c3InHEIwfC8aQLF2BW72OZMFiajNnYerraH/oZmz7MFAFEV6qgJeo9PtfU/n7/WAj1NV4KcRG4GpkDno1DV/4GG7dWvquuAiRHF7LgOe5xITgE6LpB9Hw5U8yyHtQwBq6P/oFei/9TzAO9Y7tEWPBW9zqtTT/6BvgHKqKGEO8ZAm9V12EsQW8KwLKc5kgj09KaN6Tf8+p+EqZ/l/8FN/diYpHNWFLYgPUJSSda2m/5QbwnlTlgXvovfZiTJDHJyWeywQFfDJA9NoDyLzmULQWI9kMyZIVdJzwTmqLZ6LGg1e2yxjEC7UnZ9F289VILgvegzFs5hWMIVm+guI1N5AZewC5t53I+uNPxtKCJglbEQFjwVfxroOgeTyFd72f6MiDII6pPfQYxSt+RbJoIZ4+QIEQYwskHfPp/eK3aLnse+A9WMtLFfASmbAZoZ780ScQ7rsPWi6DCFsTEJ6lylZUkVyWZO5CSn+6GRs0kxIJsbQhQZbC207AtreiiQMBVJEoIlm0lNIfb8KGzQxSZZA1dH/o/9B32aUY6smfdBLB+D3QahVEGKSKRBHJ8pWU/nATfRd/D1Caf/RNqNVIiY2wNGPzbeROfzOSy4Iqg7wi2QzVex+i8sR9mKCBlGAw4TCUGvnjjyfcZxJarYEIElhcTw+lX99E5dbbKf3+FvJvPZGUBPUYMmSnv5rM6w5HK1UwAqqItZRuup14+WzE5khJNkOyZAUdx51KvHgWYdsk8qe9FfDgFYRNVCEM0b4ixV/9lvJdf6TjpPfSdvPVSC4LXsEIg4ygA0W0v4TJ15F/7+mUbvg9rmsVNhiOJgmDjICx4Kp414lt3IPGsz5I9k1HU3vsKXo/cyHx6nkgDtE8IjmMbQcUvIIqhlaKV99A/fkfJJw+BbwHY3gpAl4qBaWf/BknU3fOOyFJIAh4QRIHgaV0w00M/Ok6VJVNFCWBJKHhix8nnL43eA/GgHNgLZVb76b4x+tRVQaJoM7Rddr7Kf7+V4Cl/rwP0fyjb4IqiLAVVRCh+7wL6LvkR/RdfBFSyNP0bxcwRKlBJqLpu1/GNDeCKohAkkAQ0P3RL1B64naepeA9SpG6972D/BknQ+IgsKAKIviBPoq/v4a+b19E7sTXI1FIylMlOuIgmr7/VUgSCAJQBRFqM2dRW/4IeE8qmbeY9Se8g3jxHGxdO63X/YzssUeC92AMW/EejCE67EA2nPtJynfdSsdJ76HtpmuQfBZUwXuwlvItd1B97HHqP3kuwV7j6P38NzG0ggBBAM6jvoT6IrZ+LI1nvZ/c20+ictu9dJ7+fpKBlRhpwNgWEMB5UIUkYUsSBLhKJwOX/YLmH/4bqPJSBbxkSkqdI+X7BiBJwBgGeY/kc0guS0orVbRYAmMAAe/AWkxrMzjHtoSU7+pGK1UwBjGKJgk4j+/uBQxb6jzlfZRu+T1CQP35H6P5ogtBFa1W0b4BsBYUUA9BgGlqoPnibyJG6P3RD+n99rcxTU00fPaj4D0goIrv3IBEIZKJQAStVJGMopUq2xABBN/bD3GCJgkCqHNIJqLpGxdQvfMBqn9/iOI1v6Zw9rvAewZVqhAnaK2GGIPvGwABdQmDMhm0UmX98W8jXrEAW2in7aZryRx9BKlkwRLc+k4kDElpLSbYcw/smJEUPnImqQ3nfpryXbfS8dazaL/1erACqqTKt9xB8epfM2rhI9QenUl19VMYm4PYg4BoHeHEKdSddTrRkQdR+dM9dLzlTFzPMow0EQQjUOfAOZ6PeoeQp3zT7TT9+1eQXBZUQYQXy/BycY5U75e+w6oRM1gz+dWsmfJaVg2fQf8lV4K1YC2l39zCquEzWDPltayZdASrRh5Az2e+TkqdYyuqQA0srD/27XSedBYShRBYtFJl7T6vZcMH/w8QQVIl5bu7qT38N0Bo+Pj5NF90IXiPlsp0nnIOq8cezJo9D2fNnoexZq8jWTP+UIpXXE+q6YffoP5970epUrz2lzzLowMDrNn7NfR88qsQBGAttZlPs7JxCuVf34wQokmNIZpUEHL0fOLLrJnyGrRag8CCCBrHhFMnU/jwmSgl+r/9Q1JKjVTxht+zavj+JIuWgjH0fuU7rGqeSjxrLmCQKCCeO5faimcICmNou+U6MkcfQapy5wOsPeAY1r32Daw94ljWHnEc6446nnWHn0g8ax6pwkfOpPm7FyJkqd7/APGSxQwKArRWo/b3J/B0ULz6OqKDDqTtx1fR9JkLaPzXL9H8je8w7HfX0Pj1T5MsWkbnqefQ+91vor29GDscCNAkAVV2yitisrgVa4jnLmCQKi+F4SUTBhlDSislnK5HvUdLRRzr0XKZzaoVHOvR4gAp7zvRSpmUWMtWAoupa8PkC3i3Ad/fy2aq+O4ufKkPGzZjGpoZJBaxdSg1wmlTGGQM3ed/geKfrwcDqooC6hJc31q6P/8V/IYeUpnDDwISxNYxKAqxURsSRXjtwBcHGKJxgiuvR2s1LG2YfIFBItimVky+CV/px3V3gXekStfdiPYPkKr/5LmEY6dTXfAYxSuvJ5q+P9m9XwNBgOteizpPSktFnK5DAotlOBJFiAkRDHb0cDKHv4qUFstseN+5+HIftmkUtmU0tmUUtnE0tZVz6P7k5xmSec1hgCBkEQkYkixeSrJkBUIBO3w4G97/CUq/vBGCAAks8byF9P7rN+l89/vp/8WP0d5ejB0OBOASUGWXGIPYAJUqiV9D8swiBnnlpTC8VEYQctT+NpPyH+/ELVmJkKH9nt+Qf/dpKDEShmwWhEBM9oTjGbn4b9jsSJJFyynfdDvVBx5GyCFBQCqcNoUx/Qto/Ma/opSQIMNWTAQkDPvTNbTf9xsGqYL3DPKeIb67D/Dk330qY3rnMnr9k7Td/AvAIj4C50ipKikRIZV93RGMqSwh//a34KkhYcQQMQbBIHUFRi56iMb/uICUaWpkxLwHGHbTlYBDTAReSVVuv5PiFTeQsiPbafz8JxEMvRd8i9wpb2Dk3PsJ994LcIg1DAoilIT86W9hTLyc3NtOwHf3Agb1HnWelFYraMUDQutvL2PUikcZvf4pGi/8P0AMlYTNvGeQV0AZon0DUKsh5JB8HcnSlZTuv5meb32Fnm99hYGrfkq88BnE1GGCNiAAl4AqOyUCgQVjUB3AubWEe+zJsO//lHC/KQyyhpci4CXytQ0oZfqu/i96r/wBIhmghuRDJJNhkPIsBQUkDJD6PBqVKT/yZ0qn/AkxEVDDxxsYFFgQQfI5QEGVrSgbKaahAEHANoxhiJgABSSKQATJZpG6OsCgfQOsP/p0JBPhOjdgaEC9JyVhACKQiRikbEFRFARMcyMSRaAKIkhgMQ0FQEEVVElJmKP36/9B4UNnIoU8dWedwcBlV1F54h76L/05Td+8AHUOULYRRUgQMEiVlIggxjBIDKgAHlNfQHJZUpLPAR7Uspkx7IijC2vaMI2NUImxtCJBhCqgCt6D9+B5fiJgBMSA86iW0aSIkCGacSh1Z51GdPAM+v/9EuLZc2j56ffAeQgsL1bAS5R/6+lE+xyA1OVBBJxHSyVsWztaq7FDziNBSP1Z70crVYhCUEWLJYIpe7EVVbZPGaTKTqmyibKZOpQqPobq7L8DipgIJUF9la0oz0+VbajyXJLLE/ctof9Hl9Hw+U8g+SyNF36O6kmPMXDJ5dR/5H2YfB5F2ZayPQoI/02VQapspsogVXYm3Hcfmj71JfouugRNSqgmgAcE1AMKImwmAsImIoCAKrgE1RrqKkCMUEc4cQrZY48k8/qjkLoc5WtvpOcLXycpr6TxQ59hE+WlCHixREg1fO48tuEcWAvVGjui3pNq+v7XwBq2R0QYJML2CYNE2HXCIK+E0/Zh5EP3gjFghEEKJAlSqGMrwvMTYRsigLIV7zBk6fvOxeRPO5lg0gRyb349+Te+iYHbbqD3G9+DKAIUlOcQdpkIm4mwq+Kn5yL1dQy/92ZMSxN2WDvJuqWQ9IEKYAFBCEipOsADCjg2MRjbTDByHOG0yUSHvYpoxnSksUDtwUfp+/pFVJ/6O1DDhMMw1GNHjeDlEPBSOQeqbEkTh1jLrlCXIGrYighYyyvKO6SQJzrsQP6hVIEI391F37cvouXnPyDVeOHnqdx+H6XrfofYDIY8qso/mmlvpuebXye8fBKt1/+Y1hv+i2TRMuJnFpDMX4xbvRatVPEdXaBgmhqQxnowlmDCWOywFuzE8dhhzWic4JavojbzaXqvv4l40XxcsgFDHmPqQQRUACWYNIGUKggvXsBLZS3PJSLsKrEBWMM/hSp4z3aJgDG8MhIkamLg8l9Sd+Y7yBx1ONFB+1P3gXfT/5NLMUELYEGVfyhVgvHjye53GKWZt1O56058R4nKLXcQvfoQbNswwimTkNYmtKcPVJH6OnCeZMES1Htqjz2Fv+dBkvmLcGvX4LUPUIQ8IllsMAK8B+9BAAUTNBIduC8psYaUqiIivFAB/5uJgDH8I6n3CBmyx72O0q2/o/eb/0n7Ub8h1fC5j1P6zU1oVz9g+EfTJEHCkMzRh1GeeTu1vz5F/l1vofdHF1JbNhtcDTCAAJZNPOABBRQIAIOQQSTCBMNAAe9BFZKEzcSgWiKaPBWZMI5Zs+dQXygwatRIwjAkpaqICLvK8L+dCIiACIiACIiACK8EMQalRP2nPkI0/WDKd/yR0m9uJhWMH0vD5z6Opxcxln80ESGVf+fbMLRRueevmIYGwpH7YlwBGw7Hhu3YzHBs1IoJmjFBCyYYhgnaMEE7xjZjTCMiGVAgceAcqPJcYgyeMoV3nkJ/YFm+ZCnPzJ3HXx/8GwsWLqJaqyEiqCq7yvC/VRAQz57HugPfwPrDT2L9kaew/tWnsP7Vp7B2/+Ppevd5vCJEUKoE48ZSf/4HUKr0fvXf0WKJVOGDZxFO2hfvi4gx/EMZA6pEr9qf3BuPJqktp/rgI+TfdQqODtR7fDyAq67F1dajSRG8ggLOQeLAefAeVHleImhSI2gaQd3Z72bRrDmItWQyGZIkYeHCRTz00MOsWbMWEUFV2RUBrwRVNlF2TtkpVbZPGaTKLlNliO/vo/L4A4hk8NqLohiTBa+oFtmK8vxU2YYqIGzLon19FD7wXgZ++DMqs/7CwE+uov5TH8Y0FGj66mfpePeZIMKLI+A9m3lll6mCCI1f+Syl224lmbOQlst/QHTADOyIdvyadbg166k9PovqXx4hWTEPJcbQCEEE3oFXdkZsgEu6aPrYJ5BRI6jv6aFUqKNUKmOMIZPJEMcxjz/xJL19fUzZezK7IuCVIMIgEXZO2CkRtk8YJMJOiTBIhCFiQ4Q8tmEYzV/7DnbkcCp/upv+Ky/DhA1sRXh+ImxDBFC2RxNHquELn6D6rkfo+/ZF5E59M8G4MeTPeCvZi1+D7+tlZ1TZmgigYAybGWGQEXbKGFAlOvQAht97B/HjTzPw06sJp0ym881nYseMIhg3mujQA8ifdiKaOMo33kb5pttwxVUIecQUwADOgyrPJUGISzaQPeJoGr/8GVJ7T53CXs6xvqODRYuW0N/fTxRFZLMZFi9eCqpMmbI3O2N4BWipDIlDazHbI2wUJ2ithpbK7Ig6BwpaKgMCImxFGKR9A5AkPJd6zxB1MYLBdXQSz5lP/MRs4tnzGRRa8u86hfzbTyJzxEEoZVAlpXECqlCtsS0BBFTx3b1oLQYRUIU4wfcNAAIibE3AGlL5t59M7qjjiTvm0v+DHzPICE3f/BKSy7OJsCMigPcMUg84ICCeM5/4yTnEzywgWboCCEFrbOYcOyQC3pM96jAqd91N5xc/iSYJ+bNOobLwr5TvvYueb32VztP/hf7/uJRw6iRaf3UZLd/5AdE+++N9N951oFoBEQgsBBYCi4QRPukjGD6a1qv+CwIL3qOqWGsZOWIERxx+KJMnTyJJErxXstkMi5csZfXqNaRUlR0JeDklDgJL75e/Q/9FP0fq6zDkUOfYzDmEHJV7H2Rldk/qP34OTd//GiQOAssg78EY4lnzWH/UqYi1GOrRJGEr3iNk6HjTe4gOmUH7X25kkDGkxBiGmPYWwFD+/a2UfvM7UmICwEFWELEMEmGQMqhy74N0nXI2ZDIY8pA4NlNFsGixxJqJh1H/8Q/Q9IOv4Xt6WXfIibh1HQgZ8J7NVBFCNrz3Y6hT2v50DY3f+iKVI+9j4NIrqHvXaUQHzyB69SGgSkqMsC0BDKoKIqQkX4c01aFdq+g6+1zwCYNsBlDMiHY2s5ZBRkDZljGgyrBfXYmeVGP9GW9nxO13oMUq/df8hCC7BxrHVB97iMpj92Drx5I//S3Uf/ZcGsKA6n0PUbntPuLlS9BkAFAQC5oQtE2g7ebrCSaOA+fAWoRNVBURYeKeE8jncjw9azYiQhAELFm6jBEjhmOMYUcCXlZKSktFnHYQ1EIGqfIsZVBcw2kHWiqyibKZsolL8MUOJCgAAqpsTUn5uAff280Qn/QjZCjfdg/5f3k7EgY0f+9CkkUrqNx1J2LqQBW8w+RbaL3iUsywZrRYonTdHxCy+KSXQbUYV+vAMoyU4hmiKKCgiqcLXxpgkCqupwtf6kUwgDJEUVLxwvn4uA+3fh2ZIw4md8KbKN56Pb1f/XfabrkOMYJ6dkg1BjzJgqUUr7iewofPRLIRLT/7IV2nfRC8B2NABK1Uyex9IM3f+wZDSr+9BVCUMhi2JQKqSC5D283X0nXW+ax/0ym0/uRiTFMLfRdfhEiECZtI6cAA/ZdfysDlPydzwGHkTj+Rxm9/HozB9/aTzF+EL5bQ/iINn/8Y4bTJ4BxYy5ZEhJT3npEjR1AqlViwcBFRFDEwMMCG7m6GtbaiqogI29CXUxxrasNHPq9LQNcf9w6tzZ6nyfJVqs6pOqfJ2vVae3K2rj3g9boEoxs+8jkdFMe6WeI0VX14pi5jmC43o7V4/Y0aL1mhvharJon6YknjRUu1/wc/06XkdM2M43VIzxe+pUup16XktfOdH9YhrqdPq48+qdXHn9bq47O0+sgTGi9aqik/UNJ1x5yqS6nTZTRq79e/r6nSjbfpUup1ZfNULd/1F02WrVRNElXn1HVu0Hj+Yu18x4d1MeiGj3xOU259p65s21eXUtDeb/1Q48XL1A8UVb3XZMVqLd/1F13ZPFWX0aLVvz2mqerfH9fl0RhdRpOW/nCbpvxAUVNdH/6cLgbdcO7ndZBz6itVXXfcabqUrC637Vr+4506yDl1vX3qOjeo6+pW19WtrnOD+nJF1XtNdX/ma7qUel1KXrvOOl8Hea/b5b1qnGhq4PLrdGX9ZO364P/R3q98V1dNOEyX0aTLpV2X2zG6IjtBV4TjdBmtupS8LqNNV7Xvp2v2P067PvSvGi9doYOcU3VOn4/3XlPFYknvvOsevfOue/TWP92ui5cs1ZT3XrfH8ApRwDQ3Ek6djB01HIwBY7Dtwwj3m4rkcoBn5xQUwmlTCMaPQawBa5FMRLDnOILJEwDHZqo0fvPzNJz/cYSAgeuupfvDnyVlGgpEr9qPaMZ0ohnTiF61H8Ge49BimY43v5vy3bchRLRc9J80fOkTDDIG8GAN0QHTsXuMBmPAGExjPcGkCZjWZrahCjiCCXsQTNgDyWRI2TEjiQ6YDsJGDowhFR08g7ozT8fTS+/X/gMSh2QzpMQImwgpdR7JRLT94Spyx5yId310nP4+Knc8AMZgCnWY1mZMSxOmpQnT2oxkIhCh++Nfove73wE8DR8+j5YrLwJVEGG7RMAaSBx17zuDkasew7Y2U5v5NHXvfiv5U96GaWhBXQlXWYePe1BqgAXxaKVKOG1vCh94N3bUCEgSBhnD8xERUmEYYK1FVRGBSqXC8wl4JQgYctSeeobuj30RkgSMYZAqiCFZsQohDwg7JIIQIEFA34U/wLS3gnMgAqoQBrhFyxHyiAiDRCBJaL7oQsQaer//Hfp/8nPcmnUEE8ah5QoYYZBXJJel+rdHqTz8AEJEyyXfp3DuWWilimQzpIQIqjV6Pv01JJdlM++RTET1wUcxZHmWgDEIdRSvuIHaQ4+htRiMkNJyBa3FQIYtNXz2Y5R+dSPxzFl0nvEh7KhRiDVUH3oMQ4YhIgLeI/kcbX+4io43vZPyX/5M5zvfTzR1OiQxIDxLwVq0WKL65OOAo+HcT9B8ybfAeTDC8xKBwIJzmEIdjd/6PL5vgGTOfKLDXkXdWafj1nYQPzmHeNZcgvFjCSbvSebVhxAdvD9SqOOFUgURqNZqJEmCiKAKdfk8zyfgFaBJP54ytUWPUZn3ANtjggKeEur62ZYySGMcnUiSpe9XlwGe5xITAgku7mYza8E5mr73NTR29F78HQb+eA3qHdsjEmDI03LpRRQ+8l6IEyQISKmr4ehGSzG9V/wAUJ7LBHk8FXzSR0rx+LgTpY/iHb9Gb0/YmmAkj1JBNSGltRrBXhOoP//9bPjGBQzceBXqYlImyOOpokkfmygYC94jhTzD/ngNHcecSnnmnSR/XQBe2S5jEW9pOPfTNF/yLXAOjAERdom1oAqJw9TXER12ILskcWAEjGHXKSB0dXWRJAlRFGGMUKgv8HwCXk7GkMoe/wbEFpD6AoiwXapo/wCZY45kkDFsZgwpO2IUTR//AgQhWAMibI+WygTjRrOZCBgDqjRf9HWCPcfhlq9CshlUlS2JEXyxTPY1h5A77c2QOAgD8J5UOGkvms67AMllwQggbEvRvgEyRx9JyuTzNHz04+hACaIQRNiagvNoHGNHjCQlNiBVOPcctJRAYEGETRTtGyBzzJEMMoZBxoD3mMZ6Wv9wJZU/3omEEaBsS1CXYAp15E4/CVTBGBDhBRGBwIIC3oMqGAOq4D0YA14BBREwBgLLCyUi1Go1li1bjrWWJEmor6+nuamJlIiwPaIb8RIlXtmSiCDCLlEFA4jwgiigbM17ZUhgBFRBFYxhV7jEocawJREQEbYkgLB9iVdSxgjPR9jEeWUrqhhreC4BhE28gldlM69gwBjDrvCJAxEQIWWNIIDzivLys0YQdp2qIiI8PWs2q1atJooiqtUqU/eZwrhxe6CqiAjbE/AyCIzwogmogiqIsDVVcI7nUjYyBmMMWzJGGKKAiIAIJA5QdkQBj2ADy65QVVzisMLWjCEwhl3iPanAGLYmbJfzDLIGI2BE2MwIL4QJLNtjjfDPpqqICEuXLWPlqtVkoogkSWhsbGTMmNGkRIQdCXiRFBCgVHNc8OfF9FcTImsQYZcIkHglFxouOGYCw+pCvCpGhM1EIAjYklfFiHDr3C5++cRamnMBXhkkQNV5CpHlG2+YSF1kUUACy/NxXgmMcNXMtdy9cAP1GYtXtmEEBqqO975qBMdMbCHxSmAEVRCBnkrCl/68gEriiaxhewToKsd84ODRpH72yCqaciHC9ikwUE0474ixHDK2gdQDS3r4ycOraMgGCC+OAs4rqvCl48YztjHLJQ+u5OEVvdRnAl4Ozis1p3zp2PFMaMnhFYywQ6qKiLBi5Srmzp1PFIZ470lNnToFay2qioiwIwEvlgIC1UT5xV9W0F2sIomisQMRdkoVExh8YLh/cQ/3fPhVNGQsXhUjwo4IQuq7dy3hnifXYQ242IMIoEgmQEV4+37tHD6uCa+KFWFHYq+ERrj0oZV89IY5SCVBYwciPJcAGhhumLmWm99/AMfu1UziFStCqlh1XP7ACkqVBKk51HkQYTNVgkxAUk44bFQDoFx3+2KC+oikGIMIW1ElyIUk1YTjJ7ZwyNgGUs+s7ufaOxcT5EOSYgwivCCqYA1kLXj4yGGjGduY5dan13PrY6uxgcFVHYjwoglILkBjz4cPGcWElhyqCiI8l6qSEhGWLV/BM8/MJQgCUkmSMH3aVJoaG1FVRITnE/ASiUBYCCFxvG6/VvYbWaCaeIwIKa8KCgibeYVcYFjYVeKWWR3MXNLDm3/+BLe+fwaFyOJVMSI8l1cwAsu7KzyyZgCaspy6XzsjGjLUnCeyhlvmdLJoZR+3PNPF4eOaUAWE7Uq8Ehrh0odW8tFfzQFVDtu3ncPGNVKqOYwIQxQlG1junN/FrGW9nPzzx/nDOTM4dq8WEq8EIohAWBeCgVMPG82ohgyxU0TAq5INDLPXFblj5lrCyKKqkA8ZObKek6cOI/GKESHlVQmMcPv8Dcxf2UdDPmCIjSzkQkaNrOctU4fhPIiwmSo7pCiZwLC6t8pvZq3HiGCMkIpyIWQCjtl/ONNHFCjHDkF4LhE2U2UrihIYoRJ7rn9yHQPlBDHCIGEbqoqIoKo8M3ceS5cuI4oiUtVajUl7TWTMmNGoKiLCzgS8RAqoApWEd84YwQcOGUXilcAIz0cBAT7y27n8+L5lPDC/i5OveIKbzz6AfGhQQNiaV8WIcPv8Lga6y0wY08DPT59KIWOJnRJaoTUX8pWlPdw2t4sL3ziRwAgKCFvzqgRG+NGDKzn/13PAKcdOb+Om9+1PPrSoggibeQUjsLxnLMf++DEWru7n5Muf5A/nzODYic0McaoQe/71qHEcPLYBp4oVIfFKYITLH1nDHQ+uZLOaY2xjhh+dvDeqIMIg5xVrhNOvmcX8Rd2ExrCVmmNMY4Yfnbw3qiDCLnGqWBH+vqKPXz++FhcYUJ5VSThj/+GcffAoEq8ERnghVEEEussJv5vdAc6zPapKSkQol8s8+dQsunt6iKKIVLVaY/Lkvdhr4p6oKiLCrgh4uRhhbX+V9QM1ChkLGARY2FVmoOqwRkgJEHvPmMYsI+ojLn7bFGrOc/lfV3D37E7ecvkT3Pr+GUTWoIDwLCMM+v2sDkg8J0wZRiFjGag5vCqhDThuUjNfqQt5bHUfM1f08aqxDagqIsIQp4oV4T/uW8Znfz8PVDl2ehs3vW8G+dCQeKWnnLAlARpzAXs0Zbntgwdy/I9nsmRNP6de/gSPffowJrbk8AqCgAjd5YRy7BEBNRA7RYCBagJWMAKeTRKv9Fcd3eWY4YWI0Bp6KwlLuit0lWpgBVVlKyI4r/RUEgQoRJZU1XkGqg4RQdiaVyUwQks+pOo821IQcKqkBmqOWuIxIqS8KqEV6jMBAsRe6a8kIIIAykaqDKuLcF7ZEVVFREitWbuWZ+bOI67FZKII5xzOOaZP24c99hiLqiIi7KqAl0GSeIgsX759MRc/uJK5nzmcfCikzrxhNn9f2I2NLM4rRsA7ZeKIAne8fwYTWnJcdto+9Nccv35yHXfN6eDxlf0cOq4Rr4oVIeUVjAhLNpS5f0kPZANeN7GZ1PVPrGNFT4WvvX5P9htVz+Thdcxf0sPdi7p51dgGvIIRNjMIqRsfXwuJ44hp7dz0vv3JhYbUZ29dyCUPrSQTGFQVESF2nu+eOIlzDx/DxJYcD3z0IF7z48dYsrqfzv4aE1tyqCo+8WDgrVc9xfD6iMfOP4TmXEA18ez/g7+zqKsMTqlUEtQIhJbZ64o0fPEevnjcBC58w0Rir7zzl7O5/cm1hLkArCHxyhDnFIwwa22Rlq/cx8dfvQffP2kSqVtnd3HGtbNozgdUE4+ySTYwdBZjPnbkWH74lslYEXYk8UrqikdW8+k/LqA5F5LqryacMGUYN561H6kn1gzw2v96jGxoSJziVWmtC3nyE4cSGEFV2YqCoogI1WqV+QsWsmrVaqy1hGFIHMcEQcB++01neHs7qoqI8EIYXiIBmupC8vkQnBInni0Jm+Szlpb6iKa6iHxdyKJlPXzkN3NJWSN88dgJYARCizHCc3lVUvct6magp8yYtjqOnthM6san1/ObJ9aSKkSWN0xqBa/csWADKSvC9gTZAGLPq8c3kg8tAlz3xFq+d8sC1HlUlZRXpVx1nP+7ufxuVgczV/VTih2Th+VBIbCGlBGhuRBSyIWUyjE9xRqqSkqBroEaceJ529HjOXBcI3s0ZyEXUOyt8oFDR3PhGyaSOv/Gedz++BryhYim+gxhXUgUGIZkQoM0RFgD2l+jVHUMqSYeN1CjpxijCgII0FNOIPHcuWADn711Id+7fzkSGqwREP6bgIIVIeW8ov01as5Tjh3xQI3+SsKQxHlKAzV6ijUUZaCc0FOMsSIERhii/DcBEWH1mjU8+NDDrFy1miiKMMZQrVZpbGzgsEMPZnh7O6qKiPBCBbxIIgwqZCzzPnM49y/p5rhLHsUYYUuBEagkXHjaPnzosNGkPvL7eVx59xIGYscQ5b+pomzLCIN+N6sDap43TG6htS5kTV+Vh5b3sqEUM2vNANNHFjhpWhs/un8Zf1naw+KuMnu25vAKRtiK80rKezYrVR0oHDC2kdvPmYFTpasYc/B3H6JnoMZplz6KklIktOCVxHlSw+pCFn/2CO5f0sNxlzyKsQYRBgmgImQjyxWn70NDNuC0a56GYsxJh4zmstP2IfWvtyzkx3cvIayL+OV79+VNe7fSV0lozAYMOX2/dt59wAjOvGE2192zjCAQhhgjINBUF/LweQfTkg+wxvC6HzzMzEXdzFvUzTNzOsAayFqcB++VzSLLQ8t6GVGfYebqfvDKj982hSdW9/PdG+cRBoYhRgQEGvMRcz59GG+96ikeXtHLNY+vxauSeAVhs/6+fhYsWEhnZxdBYMlEEUmSoKpMnLgne03cE2MMqoqI8GIEvEQiEBihIROQUrambKRKIWPJBoZULjDgFWOFISLskFcwIiztLnPPog2QDzll33ZSf1vWy4b+GnjPvYu6mT6ywEFj6hk7LM+K1f38eV4XHzliDF4VI8L2iLCZGAFVrEBDNiAVWcMpB46kr+oIjYBAOfb0VxMSrzTmQlLGCKERGjIBKQWUZ3lA2eSdv5zFb+9eyvGHjea6904n9W/3LOU7ty2EyPKLd03n5KlteFWG1UVsKWMN1giRNaDKVhRQBjXnQpqyAakzjxjL4Xu3kgsM5cTjPThVVKG1LiSVVBPwytUPLOfKOxcTZANwnowImcCAgrI9SmM2wDqP9lY57xdP4pxHChlQQ7VSZdnC+TyzeAWCkslEeO+pVqvU19czbeo+NDc3oaqoKiLCixXwMlG2TwQILX9Z2kvGGlLzOkoQWZKqY4gqmwiIsBWvihHhT3M6GeiuMGZkgSPGNZK6cXYHJB4EbluwgfNePZbmfMjr9mzm6uW93Dq3k48cMQYj7JAqmzmnEFpW91X5wm2LSLwiwPiWHAIYEQZqjrdOa+PQPRrwCkYYZIRByg4oZKzw8Zvmc/19yzjqoJH87l/2py60XPrQSr5w03wIDJecPpV3zhhO4hUrgiqIsJmyiSrbEsAIXmHmqn6acwGxU964dysnyjCMwPiWLEaEIV6V1JRR9Szur9GcC0gJsLa/RltDhnhNPwg75FSZOrqBnsTTko+wQUDfQJHegRILnplNkouxNsQaoVarYa1l0qS9mDB+HNZaVBUR4aUKeJkI2xdXHKhy9X3LuPKOxaRsLoDYc+AejQxxXhnkFHXKIAUEjDDo9gUbIPEcPbGFlnxIVzHm5jmdjG7LUx9Zbp/XyYqeCmObspw8rY2rH1zBY6v66SrGtNaFqIIIm1kjpIywWUMugMSzYlU//7awm81USQmgoeWi+5bxh3Nm8PpJLcReCY0wRNiWAqEVOvpjrrx/ORPHN/GLd02nEFk6izFfv2UhxJ5PvXEi5x4+htgpoRW2R9gx7xWcp3egxnEXP8IQUUUVmgsR875wJG2FiNgpImCNkPrOiXvxnRP3YkuJVwIj3PDkWhBhe0SEOPFcduoUlI28Y82qVaxYWaRcFjyKSoQmCbXE097exqS99qK+voCqoqqICC+HgFfYtDH1lICGjCVlRCjWHDNGFfj3E/ciVY49n/3TQqgmNNVnaKuPSIkIXsGIsKKnwr2LeyATcPL0NlJ3L9xA9+p+PvnOaYxuyHDOj2dyz8JuzjxoJK/Zs4mR7XWsWV/k/sXdvHXfdrwqVoQhSSWBwPDXZb2UY08uNLx1ehsXnj6VeR0lCpHF898UEMiHhluf6WD+yj7edvkT/OGcAzh2r2a8Ks9LwRqBmmPi6Hru+PCBjGvKEjtlWF3Iqye38Nu/rqCvkpAywouSDy11LTmacyFelZQIxE5Z31dFMpbAGlKBEUTYTJVteGWXiBFcErN0xSpWrlzFwECRMAgw1uCdx7kqTU2N7LnnBIa3t5NSVUSEl1PAK0gVLj99KjvilUFv/tnj3D2nA7IBV54xjQktOZxXrBESrxgR/jy3i56uEnuMqufoic2kbpvXBRmLiFCKPeQDbnqmgzMPGkl7IeK1E5q4YUUfN83p4K37tjPEo1iEsw4fw0PLevnr3E5OuvwJbjp7BvnQcMGx4xG25RWMwPlHjuG4n8xk8ZoBTv7Z4/zpwwfymvFNxE6xlq0ImxgjJLEjkwn4zVn7M6E5x/1LehhRHzF5WJ5zjxjLbx9fyy8fW8NnjhrH3m15vIIRdlnilZOnDaPrK69FhEHOQy40PLislyN/8DDPR4RtGGGHlP+myuLFSyl1raV3oEQUBuSyGZxzxLUahUId48ZNZszoURhjUFVSIsLLLeAVFjtFeQ5VAitsIhyzdyt3L9gATrn+yXWcPK0NawQFjDDod7M6wHkOH9dISz6kqxRzy5PrSH35V3NAFQLD/XM6WT9Qo70QcfTEZm54aCX3LOphoOooZCwKWBG8Kh86dDSJU8779TPcNbuDU658kn85eCT9FYcIm4nAQNVxyNgGjhzfxISWHLd/8ECO+elMli/r5Q9Pruc145vwqoCwJWWT2HlyoeW3Z09jxqgCS7rLnHjZTD565Fi+/eZJHLNXM8dNGcadM9dw2d9W8p8nTUZVQYRdJYDzUEk8IgzyHjKBUHOeV4QISeJYvHgJLTlLLpvBOUe1WiWfzzNpr4mMHTsGay2qiqoiIrxSAl5BInDGL2fx0LJeGrIBKSNQrDlmjCpw7RnTKWQsFxwzHgEuuHk+1/99NZ9+zR4cNLYB55XACKt6qzy0vBdCy9umt5Mq1RwXnLAXgRFEBAG8KolXYqek3jBlGIXmHMs6ijy2so+jJjbjvWKNYESInfLRI8bgUc6/fg53PrqaOx5aCcImIqCKWIOGhrr6DE/+6+FMbMkxsTXHwWMaWL64hzCy7IggpJxXrn7XdI4Y18jS7grH/3gmA8WYXz+9ns8dO4GmXMBZB4/izlnruXbmWj579HjaCxGqIMIusUb43dPrOePap2nOhXhlkAg4rxAYXihVBYTnJRBlIlQTqtUqhbo6Jk+exOhRIwmCgJSqIiK80gJeJFVFRNiZlRtKrFk3wPrA4KqOlA0Ny1f28U4HN5+9P6ljJ7VwgTUg4NnEqwLCHfO76NlQZtTwOo6d1EJqTGOWjx05lh3xqoxvzvLaCU3c+vAq/vhMJ0dNbEZ5VmgFVfjYEWMZlo+4f0kPhcjiVfEK1cQTWKG7lPCbp9dRLMd09teY2JIj5VVBFa/KzjTlAo4Y18j6Yo03/vRxFq3uJ5cPWbxmgOufWMuHDx/D2/Zt47vjm3hy4QaueWwtnzpqD5wqgQi7qpZ4/EBMjwcXO4aIAl7R0JF4TyrxighYIwhbU1VEBBEhpcqOKdRqNdpb6pk0ZgyjRo4gCAJSqoqIICL8IwS8SCLClpTtC0ILXjl2ejvnHjaa1K3zurjsvmU8vrKP3kpCYzbAe2WQKkMEIfWbpzsgdrx+ciutdSHd5Ziv3rGEnkoCyjZa8gFfPX5PGrMBJ00dxq2PruZP87r4txP2IjCCKohAx0CNDeUEI8LrJ7dw4pRheFWcKqEx1GcsIrC4q8yvn1oHIhgrDBGEXWVEKMWed187i3mLupkxpZXzXz2Ws695mh8/tJKzDxlFPrScc+hozl/UzaUPruCcQ0fRmA1QBRF2iREBA435kF+95wCacyGxU5pyAVbAGKE1H5IKrbAlVSUlIogIqkpPTy+lrjWsW98JQQCqbEVBjLDv9OnsvcdwEENKVRERRIR/pIAXSFUREfr6+gnDgGwuR0p4HolnYkuOk6e1keooxlwWe8KMxYiQMiI8V2iFZd0VHljSDZHlDZNbSP11aS8/vHUhJhPgM5YtSTVBY8/JU9t43cRmXjOhiaAxw+zV/Ty8rJcjJzRRdZ5sYPjKHUv4r/uXEWUDapWElA0MzimH7NnEw+cdTKrmFGUjBVV2Stm+M6+bxZ2Pr6N1RIFfnDGN/UYWuOrR1dz7VAd3L+jmjVNaOXXfdr41vI5FK/v47VPrOfuQUSReCYwgwiBlJzwYAweObqA5F5C66C8rWNBZJhcayrHHq+JUUYUvHzeeMY1ZQBCBcrlMR0cna9aupau7l/pASZIARFCeJaQUay3t7W0gBu8VYwQR4Z8h4AVQVUSEYrHIo4/N5IAD9ieXy/FCKf9NQdnEq7I9f1ncQ193heFteY6a2EzqxqfXg8JV79uPd88YQXc5IdWcC/jtrPWc9qNHuG1uJ6+b2MzU4QUOHdvIX59ax1+X9nDkhCaGeFWoOYYNyzN2TD2C0DFQY9HqfoRnhVYQAYStKMr2CM9SlFR/NeFvS3shMvzijKnsN7JA6vPH7cm9szr4/v3LeeOUVkY1ZjjroJH8+80LuPyR1Zx9yChCKyjPEjYRAZRd8suHVvL3BV3YwOKqCVgDWQsK5x0xhjGNnrXrO1m/bh2dnV3UajWMtURhQH0GrGEjZWdEhH+mgF2kqogIpXKZRx+bSbVWIxMGFGPljT+byaq+KgSG5xIBciF3LdzAGdfOIrW4uwwZi6smiDDIGGETIRMYPvaH+dy/pIeeUg2M8LqJzYxsyNBVirlt/gaoj7ju8XVUY8/ZB48i9V9/W8mf522AQsSt87r4+hsnElnDCVOG8de5nVz84Ep+O7uTXGi490MHkg0tVB1nvmok//bGiaQuf3g151zxBIs2VHjNfz1Gqpx4vFdIPFaEIVYEBEQYtKEc857r5rC2vwqhxYogbGJEiDIWIsulf1tJosrYxiw/+dsq8i05bp/Xyb2LunndxGbOPmQ0P/rLSv62vJeDfvgIiVcOHF3P5afvQ+qmOZ186fbF9FRiyIeo8iwBQkM59rzp508QWEGAxeUYmnJMH1XgDZOHsWhDid89vR5rDYsXL6a8pJ+uvhJGIAxDwihDVhJuX5VwzRJD0QGRQYTNlI0CQzn2HHnpoxw+rpGfn7YPqiDCP0XALhIRksTxxBNPUalUCWxAyinMWtFHTzkGI2js2VJcSqAcs2hZD/Pnd5ESayBj+dzr96Q+sqSufHQ1qIJTcJ45q/p5al4nYV0I1YS3Tm8ndd+CDaxa3kvUmuOWv61Eao6zDx5F6oa/r+G+OR0EDRmenr+BBxb3cOykFt40pZULRFi5vsiK9UXqcyEpTTxYYX5HiZTzytsPGM41T4zgnplreXB1P14VYwQv8JkTJ/OqMfUoUKw5FnaVwRpqVUeqlih/W7iBUuzAeRIBVQZ5hXJ/DWLPrX9dwX6teeKxjfzu7qVkRhagr8rF9y7jdRObmdyW5y1Th3H9/ct4uuaoVR0ZVYZ09FeZPa+TbFMWijVcNWFIEnsYqFH1ysPriwwJMgEUaxw0Yzj/fsJEHlzWy28fX0sisHbdelqbDVEUgXriOMYpjGpvIuwLWLK+k6jOQLFGrZwwRL3CQI3YK890l6k3wj9bwC5QVUSEuXPn0dvbSyaToVqrod4jQJgNoFjjKydM4u37tRNZIfFKYISTZgxn0qh68pFFACNCfy3h+EktnPWqkaTOu3EeP753GRhhz/Y8e7bX4a2ANRwxqZVzDh7FMZOaSQ1vzPLDs/bjRw+uZMFAjUw+ZEiuLoTA8OqJzZwxYzjN+ZDUxNYcvzx7f+5Y0M0VD60gyAak2hsyYA2/e2It59VHXHzK3hQiyw3v3pff7D8cAYxA7JTWupBTp7eR6q8mnPDzJ3hiaQ9EloPGN5EyBoJcAF75zHF7st/IAqE1qIIR+OHbp/KHOR1ce+8ygmyACQ3UhWRCw9ffsy9tdSHFmqMusnzlDXtyzN4tXPfEeu55eh1h1jLEWAOBYY+2POe9ZTLThhcYcvAeDXz/rP3JBAYBRBikCuXYMW1EgVTilZQgBIHBOUctceRzWUaNGklb+3DaWpt5MFkNupa2+no+deIkJrbm8ApGYHRTlp+dPYP7Fndz9QMrCLMB/2wBO6GqiAjrOzpYtXo1URSRJAn1hQJ1dXX01DyqgFemtOeZOryOxCuBEVJfPHYC26Nsct6N87jknqVgDXsOy3H7h19FfWSpJh5ix9T2Ot574AgSr6SOGNfIkeMbuf7JdSxIPF6VIZ6Nao592uv40KGjSbySykeWd84YgRHDFfcvwwp0lxMuOHY8SzaU+Pn9y7nknqUocMkpezOsEPKRw0azJa9gBFb2VnnrlU/y6JJuEOHiM6bxrhnDSVkRvCo4z9v3a+fgsQ04VUQgExhO36+d/qrj2tsXA4oq4DwZa/jAIaNpzgWoMmivYXmmtNcxr6PMPY+tRpVnCZB4huVDPnbkWFTZbEp7HVPa69gRp0oqGxhSqooxlpEj26hvbaOttYUwDEm8klI2ckpjLuDjR47FGkHZZER9xDkHjyJ19d1LQfmnC9gJESG1ePEShhhjmD59GhiL+hgRIDDcvbCbxCulmsMaIeW84hVEAAUEnFfqMwH3LNzAFQ+tBCNMHl7Hnz94IOObs6SMEQgtczuKXD1zLeXYYY3gvGKN0FmMITBsSdgoMsztKHL1zLWUYkdgBOeVXGi4f0kPBAYRYciPT90Hr3DFX1Zw6b3LKNccx05qoa+SYERAQBUEiALDD+5fxlNLe8EaLnn7Ppx7+Bhip4RWSBkRCAy/n93B7HVFYu8xIjiv5EPLA0u7ITSgIAIYIfHKlY+upj4TkBKBxCv50DJn3QCEFhGepQrW0Fms8bO/r8J5xRohpQpOlWcJIgxSBQHqMpa564tgDQaYsf9+7D22Ga+KEUFVUVVAEDayht5Kwk8eXkVoBRFBgMQr+dDywJIeCC0I/3QBz0NVERF6enro6+snDAJqccyECeNpamxgEyGpJOCVn929hJ/+2YOwcwomMBAY9p/QxK3nzGBUQ4bYK6ER4koCqtw/q4N7Hl0DwlZsLoTEk9QcQ2qVBLxy/6wO7nl0DQjPUpDQQGBIKglDBPjZ6ftQij03PLyKX9y7jCvuWsL2CKChhUzAz86YyjkHjyLxijVCShWSSgI1x7dvmoc6ZSsKQcaCV5LY452HqqO/r8qnrn0alK0p2IwF74nLCUO8ZyNh8boyH7jyabYhwmbeg/dswwhkA7xCEISkVBVEEBFEGOSdh9ixrrPER698km0o2MiCKnEl4Z8tYBd0bejGOY+1ljAMGTtmNEMCK7xqQhPd5ZhMYBABVXbKiFB1nrZ8yC/eMY1hdSHOK4ERUtPGNFAG6jMWEVBlMwEU6K0k7D2ywJCpo+vpqCY0ZANEQJXNREAVqomnORcSGCElwkbCde+aTiFjmbmqn0LG4ryyJQGMEfoqjvOPHMvZB48k8UpgBFUGRYHh4D2bKdYckRVEQJXNREAQ1g3UGNuao6UuYtLerbTVhWxJVUkZYxARiomy/+h6UEXjMlLtY1SboSVngAwICClBUbzzeO9AhLp8gWw2i6oyRARUIfGKAvnIkBIxDBE2aW/IMGVyKy25gO0RYVBfxTFtTAP/bKIbsQOqiogwa/YcVq5chTGGxsZGDj3kIF5OXhlkhH8KBVQVI8KucqpYEV5pPqnR29NDZ9cG1nV2UyqVMaJkAktKAVXFOYd6JQgsuXyelpZm2tvaaGhsABPwv0XALlBVRISUESGlCiIMUuVFU0AAEbaiyq4REDZRZZeJsJmwkQheFUF4PkpKsSJsjyrbpSiDFBQwAiLClrwqpVKZ3t5euru76ento1KuUItjBAgCi7UGBUo1h/MeEYiiiIamRoYNa6W1tZW6ujzWGIao8rxE2CFlI2WXifBPFbALoihCVbHWUiyVqFSqZLMZVBURQYQXTdg+EV4wEV40AUSEnRFSwo6IMEhV2ZIRYZCwWeISigNFenr76OnpoVgsUSqViOMEEbDWIsaQiUK89zjnSJKEIAior6+jubmZ1pYWGhoayGYzbElVSYkIIrxowkbC/zcCdkFrSwtLly7DGEOlUmHRosVMm7YPKVVFRPjfTFXZkoggImzJe0+5UqG/r5/evj6KxRIDA/1UKlWcc4gxWGMwxhBFId57nHNokmCtJZfL0dTUSHNTE42NDRQKBUSELakqKRFBRPjfKOB5iAiplpZmCoUCxWKRKIpYsXIlxgh77z0ZYwyqSkpE+J9MVdmSiJASEbbkvKdSLjMwUKRULtPX10d//wCVSoUkSVAFYwRrLdZarLU453DOkyQOG1jyuRwNDfXU19fT0txMoVCHtZYtqSopESElIvxvJ7oRz0NVERG6urp49LHHsdZijFCt1mhpbmby3pNobmpiiKqSEhH+f6SqbElE2BFVpVarUalWKZfKDAwM0Nc/QLFYpFqtkiQJqmCMYIzBGENKVfHe470nFYYh+XyeunyepuZGmpqaKNTVYYxhS6rKEBFht22JbsROqCoiwqpVq3l61mystVhrieOYVNuwYYwdO4bm5iaCIGBLqsqWRIR/FlXluUSEnXHO4bynUqlQKpUoDpQYKA5QKpaoVCvEcYL3HgWMCMYYjDGkVBXvPd57VCEILFEUkcvlaGpsoLGxgYaGBrLZLCLCllSVISLCbjsnuhG7QFURETo6Onl61myqtRpRGCIixHGMKuRzWVpaWhg2rJWmpkYymQwiwo6oKq8UEeGFcM6RJAmVapVKuUKpXKZcKlEqlymXKziXEMcJzjlUwRjBGIOIICKkVBXnPeo9KWMMYRiSz+epry/Q0FBPY0MDuVyOIAh4LlVliIiw2wsnuhG7SFURESqVKgsXLWL16jU45wnDAGMM3nuSJCEVRRG5XI58Pkd9fT2FQh25XI5sJoO1FmMM/wjOOVSVOI6J45hqtUatVqNcqVCr1SiXyjjvqVarxHFMkiR471EFETDGICKICCJCSlXx3uO9RxVEwFpLJhORz+cpFAo0NNRTl68jm82QyWTYHlUlJSLs9vIQ3YgXQFUREVK9vX2sXLWK9es7qFQqiAhBEGCMQVVxzqGqeK+IgLWWMAyJoogwDMhms4RhiHolCCy5XA4RYTMBaywIg7z3qFe2pKqUKxWSJCElIsRxTLlcRkSoVqskicN7R5I4VBXvFREGiQgigoggIogIKVVFVfHeo6qogogQBJYwDMnlsuTzeeoLBTLZDHV1deSyWYIgYHtUlSEiwm6vDNGNeIFUlZSIkKpWa6xfv54NG7rp6e2lUqngvccYg4hgjEFESKkqqoqq4lVBle1RZZAIW1EFBITnIYIRISUiiAgiwhBVJeW9J+W9AooqGCMYYwjDkEwmIpfLkclkyGWz1NXVkcvlyGYzBEHAjqgqQ0SE3f5xRDfiRVJVUiLCkCRJ6Ovrp6enh96+PiqVKpVKhThO8N6hCiIgIogIKWMMzyUi7Iiqsj2qiqqSUlVUlSEKWGNIhWGIMYZsNosxQjabJZPJkMvlyGWzZLMZMpkMQRAgIuyIqrIlEWG3fy7RjXgZqCopEWFLqkocx1QqFUrlCpVymXKlQq1Wo1at4bynWq2gCsJGAqqKcx7vParKswRjBGMMxhhEAAVEUFXCMCCKIlSVTCZDJopI1dXVYawhl8sR2IBMJsLagCCw7Iyq8lwiwm7/bxLdiJeZqjJERHg+qopzDlUQYZD3niRJcM6hqjxLMNYQWIu1FhFhiKpircUYwwulqjyXiLDb/39EN+IfQFV5LhHhlaKq7IiIsNv/TKIb8f8gVWVHRITddtuegP9HiQi77fZC/V//0Ho2QJ82fQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNC0wMS0zMVQwOToxNjoyNiswMDowMGY4EaoAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTQtMDEtMzFUMDk6MTY6MjYrMDA6MDAXZakWAAAAH3RFWHRleGlmOnRodW1ibmFpbDpSZXNvbHV0aW9uVW5pdAAyJUBe0wAAAB90RVh0ZXhpZjp0aHVtYm5haWw6WFJlc29sdXRpb24ANzIvMdqHGCwAAAAfdEVYdGV4aWY6dGh1bWJuYWlsOllSZXNvbHV0aW9uADcyLzF074m9AAAAEXRFWHRqcGVnOmNvbG9yc3BhY2UAMix1VZ8AAAAgdEVYdGpwZWc6c2FtcGxpbmctZmFjdG9yADF4MSwxeDEsMXgx6ZX8cAAAAAp0RVh0cmRmOkFsdAAgIM2DzKsAAABGdEVYdHNvZnR3YXJlAEltYWdlTWFnaWNrIDYuOC4zLTYgMjAxMy0wMy0xNSBRMTYgaHR0cDovL3d3dy5pbWFnZW1hZ2ljay5vcmf+g02TAAAAGHRFWHRUaHVtYjo6RG9jdW1lbnQ6OlBhZ2VzADGn/7svAAAAGXRFWHRUaHVtYjo6SW1hZ2U6OmhlaWdodAAxMjk5/3smcgAAABh0RVh0VGh1bWI6OkltYWdlOjpXaWR0aAAyODM1gNX7JQAAABp0RVh0VGh1bWI6Ok1pbWV0eXBlAGltYWdlL2pwZWeLpZ60AAAAF3RFWHRUaHVtYjo6TVRpbWUAMTM5MTE1OTc4NqvhAFQAAAASdEVYdFRodW1iOjpTaXplADc1MktCQj0+6hoAAABvdEVYdFRodW1iOjpVUkkAZmlsZTovLy9ob21lL3NpdGVzL3RpY2lub2Jhc2tldC5jaC9wdWJsaWNfaHRtbC9hc3NldHMvbWVkaWEvY2x1Yi9sb2dvL0xvZ29UaWNpbm8tQmFza2V0X0NvbG9yLmpwZ1swXWaNJRUAAAASdEVYdHhtcE1NOkRlcml2ZWRGcm9tAJeoJAgAAAATdEVYdHhtcFRQZzpNYXhQYWdlU2l6ZQBzgNfZAAAAAElFTkSuQmCC'
														}
													}
							            			pdfMake.createPdf(docDefinition).open();
							            		}
					  						});
					  					}
					  					
					  					//Close modal
										$scope.cancel = function () {
											$uibModalInstance.dismiss('cancel');
										};
					  				},
					  				size: 'lg',
					  				resolve:{
					  					match: function(){
					  						return $scope.match;
					  					}
					  				}
					  			});
					  		}

					  		//Send email
					  		$scope.sendEmail = function(players, match){
					  			console.log(players);
					  			console.log(match);
					  			var modalInstance = $uibModal.open({
					  				animation: 'true',
					  				templateUrl: 'app/views/calendar/send-email.html',
					  				controller: function($scope, $uibModalInstance, $http, $filter){
					  					var date = $filter('date')(new Date(match.game.date), "dd/MM/yyyy HH:mm")
					  					$scope.email = {
					  						subject: 'Comunicazione partita',
					  						message: 'Sei convocato alla partita che si terrà contro ' + match.opponent + '.\r\nLa partità si svolgerà il ' + date + '.\n' + "Questa è un\'email automatica. Non rispondere a questo indirizzo."
					  					}


					  					$scope.send = function(email){
					  						var post_data = { 'subject': email.subject, 'message': email.message, 'players': players }
					  						$http.post('assets/php/email/send_email.php', post_data)
					  						.then(function(response){
					  							$uibModalInstance.dismiss('cancel');
					  						});
					  					}

					  					$scope.cancel = function () {
											$uibModalInstance.dismiss('cancel');
										};
					  				},
					  				size: 'lg'
					  			})
					  		}

			            	//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};

							//Check if an object is empty
							$scope.checkObject = function(obj) {
								return angular.equals([], obj);
							};

							function getMatchResult(){
								var post_data = { 'match_id': $scope.match.game.id };
								$http.post('assets/php/result/get-result.php', post_data)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else{
				            			$scope.match_result = response.data;

				            			post_data = { 'result_id': $scope.match_result[0].id };
				            			$http.post('assets/php/result/get-images.php', post_data)
				            			.then(function(response){
				            				for(var i = 0; i < response.data.length; i++){
				            					var img_path = "assets/" + (response.data[i].image_path).substring(6, (response.data[i].image_path).length);
				            					$scope.match_images.push({
				            						thumb: img_path, img: img_path
				            					});
				            				}
				            			});
				            		}
								});
							}
			            },
			            size: 'lg',
			            resolve: {
			            	
			            }
      				});
				}

				//Blocking the user from going into day mode
				$scope.viewChangeClicked = function(nextView) {
					if (nextView === 'day') {
						return false;
					}
				}

				//Add event modal
				$scope.addEvent = function(){
					var modalInstance = $uibModal.open({
	            		animation: 'true',
			            templateUrl: 'app/views/calendar/new-event.html',
			            controller: function($scope, $rootScope, $uibModalInstance, $http, opponents){
			            	$scope.categories = [];
			            	$scope.selected_opponent = [];
			            	$scope.selected_category = [];
			            	$scope.opponents = opponents;


			            	//Get categories
			            	$http.get('assets/php/team-management/get-categories.php')
							.then(function(response){
								$scope.categories = response.data;
								$scope.selected_category = $scope.categories[0];
		            			$scope.selected_opponent = $scope.opponents[0];
								$('select').material_select();
							});

			            	//Upload data
			            	$scope.upload = function(opponent, category, new_event){
			            		var post_data = { 
			            			'loggedIn': $rootScope.globals.currentUser.loggedIn,
			            			'role': $rootScope.globals.currentUser.role,
			            			'opponent': opponent.id,
			            			'category': category.category,
			            			'event': new_event
			            		};
				            	$http.post('assets/php/calendar/add-event.php', post_data)
				            	.then(function(response){
				            		if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
										getOpponentsAndMatches();
										$uibModalInstance.dismiss('cancel');
				            		}
				            	});
			            	}

			            	//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
								getOpponentsAndMatches();
							};
			            },
			            size: 'lg',
			            resolve: {
			            	opponents: function(){
			            		return $scope.opponents;
			            	}
			            }
      				});
				}

				//Get opponents list
				function getOpponentsAndMatches(){
	            	$http.get('assets/php/calendar/get-opponents.php')
	            	.then(function(response){
            			$scope.opponents = response.data;

            			//get matches
		            	$http.get('assets/php/calendar/get-matches.php')
		            	.then(function(response){
		            		if(response.data.hasOwnProperty('error')){
		            			Materialize.toast(response.data['error'], 3000);
		            		}
		            		else{
		            			//syncEvents with calendar
		            			setTimeout(syncEvents(response.data), 1000);
		            		}
		            	});
	            	});
				}
            	

				function syncEvents(events){
					$scope.events = [];
					for(var i = 0; i < events.length; i++){
        				var opponent_name;
        				for(var j = 0; j < $scope.opponents.length; j++){
        					if(events[i].id_opponent == $scope.opponents[j].id){
        						opponent_name = $scope.opponents[j].opponent_name;
        					}
        				}
        				$scope.events.push({
        					title: "Partita contro " + opponent_name,
        					type: 'info',
        					startsAt: new Date(events[i]['date']),
        					editable: false,
        					draggable: false,
        					opponent: opponent_name,
        					game: events[i]
        				});
        			}
				}
			}
		};
	});
angular
	.module('gsb')
	.directive('homeView', function(){
		return {
			restrict: 'E',
			templateUrl: 'app/views/home-view/home-view.html',
			controller: function($scope){
				
			}
		};
	});
angular
	.module('gsb')
	.directive('homepage', function(){
		return {
			restrict: 'E',
			templateUrl: 'app/views/homepage/homepage.html',
			controller: function($scope, $uibModal, LoginService){
				$scope.loginModal = function(){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/homepage/login-modal.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, LoginService){
							$scope.login = function(credentials){
								LoginService.login(credentials, function(response){
									if(response.loggedIn){
										LoginService.setCredentials(credentials.username, credentials.password, response.loggedIn, response.role);
										$uibModalInstance.dismiss('cancel');
									}
									else{
										$scope.loginError = true;
										Materialize.toast('Errore: I dati sono incorretti oppure non sei registrato al sito!', 3000);
									}
								});
							}

							$scope.logout = function(){
								LoginService.logout(function(response){
									LoginService.clearCredentials();
								});
							}

							//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
						}
					});
				}
				$scope.logout = function(){
					LoginService.clearCredentials();
				}
			}
		};
	});
angular
	.module('gsb')
	.directive('manageTeams', function(){
		return {
			restrict: 'E',
			templateUrl: 'app/views/manage-teams/manage-teams.html',
			controller: function($scope, $http, $timeout, $uibModal){
				$scope.roles = [];
				$scope.categories = [];
				$scope.selected_role = [];
				$scope.selected_category = [];
				$scope.members = [];

				//get roles
				$http.get('assets/php/team-management/get-roles.php')
				.success(function(data){
					$scope.roles = data;
					$timeout(function(){
						$scope.selected_role = $scope.roles[0];
						$('select').material_select();
					}, 0);
				});

				//get categories
				$http.get('assets/php/team-management/get-categories.php')
				.success(function(data){
					$scope.categories = data;
					$timeout(function(){
						$scope.selected_category = $scope.categories[0];
						$('select').material_select();
					}, 0);
				});

				//get members
				function getMembers(){
					$http.get('assets/php/team-management/get-users.php')
					.then(function(res){
						$scope.members = res.data;
						$(document).ready(function(){
							$('.collapsible').collapsible();
						});
						return res.data;
					});
				}
				

				getMembers();

				//Add Member
				$scope.addMember = function(){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/manage-teams/add-user.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, roles, Upload){
							$scope.roles = roles;
							$scope.selected_role = [];
							$scope.member_data = {};
							//Upload member
							$scope.upload = function(member, dataUrl, name, role, category){
								Upload.upload({
									url: 'assets/php/team-management/add-member.php',
									method: 'POST',
									file: Upload.dataUrltoBlob(dataUrl, name),
									sendFieldsAs: 'form',
									fields: {
										data: {
											'member': member, 'role': role
										}
									}
								})
								.then(function(response){
									$uibModalInstance.dismiss('cancel');
									getMembers();
								})
							}

							$scope.player_selected = function(value){
								$scope.show_player = value;
							}

							$scope.membership_nr_required = function(value){
								$scope.required_msn = value;
							}

							//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
						},
						resolve: {
							roles: function(){
								return $scope.roles;
							},
							categories: function(){
								return $scope.categories;
							}
						}
					});
				}

				$scope.editMember = function(member){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/manage-teams/edit-user.html',
						size: 'lg',
						controller: function($scope, member, $uibModalInstance, Upload){
							$scope.member = member;
							$scope.member.birth_year = parseInt(member.birth_year);
							try{
								$scope.member.membership_num = parseInt(member.membership_num);
								$scope.member.pic_path = "assets/" + ($scope.member.pic_path).substring(6, ($scope.member.pic_path).length);
							}
							catch(err){

							}
							Materialize.updateTextFields();	//Update collapsible

							$scope.upload = function(member, dataUrl){
								Upload.upload({
									url: 'assets/php/team-management/edit-member.php',
									method: 'POST',
									file: Upload.dataUrltoBlob(dataUrl, member.name),
									sendFieldsAs: 'form',
									fields: {
										data: {
											'member': member
										}
									}
								})
								.then(function(response){
									$uibModalInstance.dismiss('cancel');
									getMembers();
								})
							}

							//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
						},
						resolve: {
							member: function(){
								return member;
							}
						}
					})
				}

				$scope.paymentsManagement = function(members){
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'app/views/manage-teams/payments-management.html',
						size: 'lg',
						controller: function($scope, $uibModalInstance, $filter, $http){
							$scope.members = members;
							//Filter players
							$scope.members = $filter('filter')($scope.members, { role: 'Giocatore' });
							
							$scope.payed = function(member){
								$http.post('assets/php/payments/player-payed.php', member.id)
								.then(function(response){
									if(response.data.hasOwnProperty('error')){
				            			Materialize.toast(response.data['error'], 3000);
				            		}
				            		else if(response.data.hasOwnProperty('success')){
				            			Materialize.toast(response.data['success'], 3000);
										getMembers();
										$uibModalInstance.dismiss('cancel');
				            		}
								});
							}

							$scope.checkLastPayment = function(member){
								var today = new Date();
								
							}

							//Close modal
							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
						},
						resolve: {
						}
					})
				}

				$scope.deleteMember = function(member_id){
				}

				$scope.PDFMembers = function(){
					
					//Format players
					var players_list = [];
					var players_index = [];
					var players_data = [];
					for(var i = 0; i < $scope.members.length; i++){
						players_data = [];
						players_index = [];
						if($scope.members[i].role == 'Giocatore'){
							players_data.push(
								{
									ul: [
										{text: 'Categoria: ' + $scope.members[i].category }
									]
								},
								{
									ul: [
										{text: 'Anno di nascita: ' + $scope.members[i].birth_year }
									]
								},
								{
									ul: [
										{text: 'Indirizzo completo: ' + $scope.members[i].address }
									]
								},
								{
									ul: [
										{text: 'Numero di tesseramento: ' + $scope.members[i].membership_num }
									]
								},
								{
									ul: [
										{text: 'Genitore: ' + $scope.members[i].par_fn + " " + $scope.members[i].par_ln }
									]
								},
								{
									ul: [
										{text: 'Telefono di casa: ' + $scope.members[i].par_home_num }
									]
								},
								{
									ul: [
										{text: 'Cellulare: ' + $scope.members[i].par_mob_num }
									]
								},
								{
									ul: [
										{text: 'Email: ' + $scope.members[i].par_email }
									]
								}
							)
							players_index.push({text: $scope.members[i].first_name + " " + $scope.members[i].last_name});
							players_index.push(players_data);
							players_list.push(
								{text: '\n'},
								{
									ul:[
										players_index
									]
								}
							)
						}
					}

					//Format trainers
					var trainers_list = [];
					var trainers_index = [];
					var trainers_data = [];
					for(var i = 0; i < $scope.members.length; i++){
						trainers_index = [];
						trainers_data = [];
						if($scope.members[i].role == 'Allenatore'){
							trainers_data.push(
								{
									ul: [
										{text: 'Anno di nascita: ' + $scope.members[i].birth_year }
									]
								},
								{
									ul: [
										{text: 'Indirizzo completo: ' + $scope.members[i].address }
									]
								},
								{
									ul: [
										{text: 'Numero di tesseramento: ' + $scope.members[i].membership_num }
									]
								}
							)
							trainers_index.push({text: $scope.members[i].first_name + " " + $scope.members[i].last_name});
							trainers_index.push(trainers_data);
							trainers_list.push(
								{text: '\n'},
								{
									ul:[
										trainers_index
									]
								}
							)
						}
					}

					//Format staff
					var staff_list = [];
					var staff_index = [];
					var staff_data = [];
					for(var i = 0; i < $scope.members.length; i++){
						staff_index = [];
						staff_data = [];
						if($scope.members[i].role == 'Staff'){
							staff_data.push(
								{
									ul: [
										{text: 'Anno di nascita: ' + $scope.members[i].birth_year }
									]
								},
								{
									ul: [
										{text: 'Indirizzo completo: ' + $scope.members[i].address }
									]
								}
							)
							staff_index.push({text: $scope.members[i].first_name + " " + $scope.members[i].last_name});
							staff_index.push(staff_data);
							staff_list.push(
								{text: '\n'},
								{
									ul:[
										staff_index
									]
								}
							)
						}
					}

					console.log((players_list));
					var docDefinition = {
						footer: function(currentPage, pageCount) { 
							return {text: currentPage.toString() + "/" + pageCount, alignment: 'right', margin: [0, 0, 30, 0]}; 
						},
						content: [
							{ text: 'LISTA DEI MEMBRI FILTRATI PER RUOLO E CATEGORIA' },
							{
								image: 'logo',
								width: 226,
								height: 105,
								alignment: 'right',
								margin: [0, 0, 0, 10]
							},
							{ text: 'Giocatori', style: 'roleHeader'},
							players_list,
							{ text: 'Allenatori', style: 'roleHeader'},
							trainers_list,
							{ text: 'Staff', style: 'roleHeader'},
							staff_list
						],
						styles: {
							roleHeader: {
								fontSize: 18,
								bold: true,
								margin: [0, 20, 0, 20]
							}
						},
						images: {
							logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXgAAAC3CAIAAACXNtIkAABYMUlEQVR4nO29C3xU1b02vOeey0yCwQoxKCokgkhtUC5ViXIIqEFBwEJLFVCqb4WI2HNeAwTf1k8CYl9PkZu+tWigPbRYAW9Jq4QqESwCJipCYiIqlxBQiCQzydxnvmftNWx29m32npmEhOzH+eFkZl9nr/Ws//1vDofDjA4dOhKHsM/Vtm+d7/B75sxc84+GmPv82JI57EJf1AWG+UJfgA4dFw9AMa17Vvq/eT8ccONP/9HdeBGiufulC31pFxg60ejQkQAIKAYwGK3WnIKU4XMNVvuFvbauAJ1odOiICxIUY0lJzn0oaeiMC3thXQo60ejQETs8Bza5q14OB7z0T51i5KATjTY0AMePK2+TlpY2aPDgzrkeHR0BCCn+xqrgmXq8t+UUGO2Z4m38x3a3frg85G6K/G00Jw2ekjJqQWdeZzeCTjTasG3L1rWrVytvM3zEiI3/85fOuR4diYW3vsx/pNJ35EPuk6Trpwu2AQ05KxYFvjvABH3kb4PJ2n906ujFui1GATrRaIYyj6xZtXrvxx935vXoiB/gDs8Xmz0HN+MN//OkIdME9OH98s22PSsjupLBaEy9zJG/wtQ7uzOvtjtCJxodPR2QYtxV60Ouk4LPTRkDk4f9ivsTHNRSXhj84TATCrJfW1NufEQ3x6iETjQ6ei6CZ+pb96wMnKwWfwWWSZuwjhNnfIffa921/JwgYzJfeq3jzpW6rqQe8RKNv7Eq0CjxnBQAOdPaP0/ribDshJzCNUcZcmY8HToASDHu6vWSX5n75jrGreB4xLVjMbHahCOCTOpPn7BdO6nTrvPiQLxE46sv99aXaztl31ytRBNyNbZWlmjahZwoM1cnGh1iYDi5di6VFGSA5Nw5ycPmcFu2vPUw51oypmWl3bVKH1QxIG6iOVKpdRe5B6wAzxebte6iQ4ckMGJbK5cKjL4URnvf1LwlXF6S/9hu547iiGvJaLLl3JN6y5OJugxcQOBMneRX5t45F59SFhfR4JlJPrCowEKhflnAKbQKTTp0SKJtz0rPwdckv0oaMi152K+4Gd62dw3ZMhQgf5isjrEllitu0Xq64Jl6g81OhzomS6CxKnCmPthUr37WQPynbyj9GR19cTSj1dHt/FxxEQ1+uNh2DDo1EA3EmdjoTIcODsRnVDY32PSV+CuBIAO0lD8WOPUp9S4ZknqlT1ofg7oElmkpn9dr+lYIUDEvyZz4L6kHUBoyOTJxeWA0U0Z2l00Tj1eiiW3HQGO1+l/EW18W21l06KCgc1481SG/JA2ZzllkGMpHb/0q2HyU/mnqnZNWsCYGRYZT0M5untJxyyRlHwEHmTIGYnKZM4fh366jgsVONHh44tADlYDqpHJL4myK9Sw6dDDsEJL0JFj7j04ZtYAvqhBe+Pu0sOcs+cNosvb7qX3cczGcke/P6nxhHFIbEdxYDRH3aOmfZ+1/2wVnnNiJJmZxhmFVJ5Vb4pnFfBYdOiBWiA18Yl2JOedgirCMwZj8kweTcx/SejocxLm9SFJBuyDwHfkQrzbrC9b+eRDcLqC/LB6i2RnzviodT/7GqnjEGU0Kmo6LDCQjaXuReKTxvdcciG71zq9pnQeDyZZ848MxhPwqBOZcWFB3Cl627AIIcRdEuomRaMDccdI2Hm1Uy7kuzuiIDZJGGQgyknlJApZJGfmYbfAUTafDigjRqevr+OAaKCKQ5mKImI0TMRKNP1Z/E4egq1GZaPD4Y4i40aHDc3Bz254XBB8Kgn05xMkypDzwnpXdKPwCF+yqWGjPf7aTuSZWoonDQENBin0o3iqGS5yn0NHTQIrdVT7DL/JAAZUBy7jk9s53n4iZZTBEIXR3x9gLkGM3IBr8suJnqRXBJumwSAqoZt1oldDRFQApG2u1eNonDZkmWY+K+Jheuy/sbWG0swyWSVflM13H6KsVna/lxUI08etNTDTHk55zoEMT5AyxqXnFtuwJ4s/BMs1bZlCWIT6mGx9WzzKSqln3Ahdw3HlnjGGf+PUmhvX2y32l5xzoiIrampqWFkITYXdT2/7/F3I2iLe59f7fSbIM4Hzvv0Jtp8k7oyn5Jw+q9DHJqWbdC8S7L1NylPSi6jO0I7zgsRBNPBE0fEAyknQ/6zkHOqJi2dKSfXv3Km9T87Q0y7Tufi7w/SH63nL5TSrjZbq7ukQh6eHGrbVV/TFwYn844E0ZUdgR1bw0Ew3YIVEsIBcfrOcc6IiOcPDhCYNmDz0t+eUXoWH/67n3JL/y1mz1fvk2LS5j6p3juOMPas4ml8TQXQBmAcUkXT9dIK14DmzyHHo97PmBa+Tg/vRVy+XDE560qZ1oEiTOAJKFrPScAx1RQUIfTtcGbWGGSRF/S2aUbRzDSBANWbo/Xk1ZxpDUK61gjcrTdVOW4fKeBD4mrPFtVev93+zg+AUwmJMNSelGR5Zr59MQ9BLb0UEz0SRKb2JY4SiZEcZoJjBIT31GlY5uhIgtlpSJsYi/xdQinmyp+vARZ3aQnVoma/qk9WpiZLsFy/DTuBm25BtzrrKEAKTo1wdPE82RVsAg/GIz/ej6pCE/61CHtzaiiSeRUgwxEcSZcyCA+owqHd0CUW2xII60Cevkvm0pL4zUyjMY7aMXqzF5dlmWAbOwokqu+ipZpBHVnpWh1u8jpbxMVlNav+QbZloHjO/Ya2WhjWj8JxPg2OYg5hQ950CHHDDnnRVFyutQWsFauVnnrno5+MPX9L3t2olqZlcXZBmjvW/S9dNt2RM05SsRivnoeeJlY6UYqEiWfiNTRj7WmTmW2ojGW5dgMy3f8UTqnOs5Bzqk4K0vg7qkPOeTc+fImTBBGZ7PN9FpZkzOUFORs6uxDESY5GFztCYJS1DM1WNSL0RepQaiUZ9IiR9FJWUEm+q5386nOnYGvK4bjHsOFOpvcmB7MAntfRz4ppm0iS9HPWOXYhlbdkEMFR7EFGO79h5+rdJOhgaiUR8QbO6drbIwatgb2UZTzoG1f57yyKus81XWe79q2lO/IIf/+SDgusGTp0wZMXIk9yEX+iWHrH79srKyVF6bVjhbWioqKvZ+/HHtoZra2lrBt8NHjBg8ePDkqVM0NfPGHW3bsrWmpkYQaXJ5VhaONnZcPuBIS6MfRu0mLtlKHJeN4ytfBv9Hprts27p178d7cbNOp5P/FW4TG+M2Bb8zP2WxsTl4sjl0/mieMH9Le95T4gugLUM9X/zNf/SU3WbIzkxK/ekTUWdszeeflL/0vz/58pTgjEDfdGNmuin3SktetjW7jzZtoP5UwOUNK2xAD87/5HPXVbu/z9r/xt7a2o38z+kwJr/YFIloZhHF2GzXTpKjGDx8/Epyw487EX/AxAYtRKPa3wQJ1nQmW41QwzmevHVqWQYEb7A65L5dv6vttX1uPFGMhtuG/ujx4hWOtPMbY5Tv2L591tYHMOWWr3iWzoSooV/zHnuscP5jKi9PPfCM16xa9cbWbXg/Nj9/7Lhxi5YUCzbA48cg2LhhAy4Y1yA5sPjYsb1iWUnJiYYGjBDx1AUB4RdYXLRwucOBo82cPZtR0U1csgUwWGbW/Q8oX0xNfSSdDRSzZtVq3IXD4QDN4dR85qK3CQ7CZeBci5cU028FVX7LD3hf2dXGP35ejpW+kVOa+FeI8fDS4z9W6MdEeXDDq6+eONGIOY/tR2dbc9qzSd2pQP13gfIDHlwJtplza0rB0CTlH4HDCztaq4/6FTZ46NYUHJC+33Us5YUdbSdO7h80yIWBUTh/vmAY41GS57i0hHuODC279c8ngi0NEf+9oqJExlXphh0VFXgoGCriszhbnHjKOBHOgnPdO2WyYIXWBA1Eoz7y2pyRE3BUqyGasC+ysqnP1U4aMl0yog8rxtIy18nm4LThyQVDbVgczH0HpI3L52+DnwkPBiN70ZNFGIUcgyhQycxf3q/ywtSDm3igj2UrnlVaLlhioZSEhw3WkHvSOOaiooUYNxgQs2bPlpSAsC8GJbbcULphecmyiu0Va18kPhqFbuLKrcQ5KhEAu3CTHCOV/oa4U1minMKAZLEXxvTkiZNKSn43+d6J4lrimPxrZqQLdiV1f6+fLr5Tem1nN0+Blo3lp/pY0HHnSrkboQTtbGkuuMF+1x295KQVXAB9g8G2eb+7pMyFIy+Z4OA+VwafSgQo3NTM3c7ynfa3d36BtWfj34olRWk6BrjniKWi9I/PGz55PnC69lxzGLP1yltTRy+Wk2Iw/rG44rmvWbdubPs5wgf3FSgYZ8EzxehaXFwcg3Sjlmg0hc9geVGpUtKRBOJQqQ+b++bi4IE99YLPoSuVlDn7pptKH+olkD/FwMPDvNpYWoqHBLVBzXkTCDzmwl8/Sh528WJuLVJGFpG/VlDZRxIYc5jMOOaGv/w56pqDUQJWzR+Xj13winmNUgMMUPAj5gzkx6ijE1ey7e23wDXFxb/zfPbnO69pVnMKLDxydgfPgU0hNxs6bDAaHX0lN8NPB4rBb4spNP9Wv+27PWomBZgI/ALWeKGiFRwx7abkx/NT1VytMtzpQ+e9euxE4xHl+U9Bn+PY22+e98iv7v/ZvWt/kWZPMhCnda+rHPnL5SYgngh+YYwoNUOFA1YIvEDHi4qKIE9hfdKkyzPqiUa93kRjh0jIkDoPEuQ99QcXr10Mu7ws2tpy11Abnr3K4wCY5HhUmAZUdFS/YzygyztlOq2PSg6UZfBmxwfvq19qcHZcA3aEYIWVLSFXIgCVujGBwZLq94Jok2psfebV1wc8KCtZ8CE5JBhq3/nkj0yQtVNYU40pl4q34QgaE3uUbY/WVF4sac9OTaOLXNVR/9oZ6WSqxwpLv5HLd5hONJ5SPzbApH2rX149zTr7lbal5a7nZman3vyfCv2nKO9rfSIcwH07Rr5P1yetAzjxEg31Ipl750TdkiJwpl6lUma09xUHL7o8YSwpUKc1sQwFSLr2UA1mmtYdYwOG9bxH51KWidO0xgeOiamiiWUoMFDWvLguqqklZmA44hQxjOlHH7xvz443X9jRKlaUBGANdtLiTOuHyzg9wjbwTqb6kGADjmXwOK74fkvMBQPycqyZ6ekYhPM2NcfGNaaMgaberZvK9judzm1vvalmAhOL74fLafwh+G7JxN4L/3667qqiEVfILpmUZZQUWBXAGKPrE14QP9U7SVQRTfCMlt56bPgzHr9KJ7RPdQqlpP9yaZkTj1aSZdQYibB+VlRUqLyAOAFGwL+JZRmMHijbEINjOyax2syaFdV/FDOoDSgGQBmBlNrYHFRWhC0yUfMQk31Hd9H31gHjDU6J4bGoaGH8LEMByQucCK5ZucOldcGjdbkMr98PloE2HZVliMW3YlGw6TC1+BJzzIDxEx9YUHpgBgaDnGwOrScqy7Dup4hXZMTIEXKH4rhm0ZNFcqY9MVQRjfp0avALFxdj7p3tU0M0qsUZcW2R6qP+D+t9a+ITWZeveJb7fTsOlBGwXiWQZbAmQzcBU8Sj+kHPx5RL1CXxAVkmtpsNNtVDTOibboRWMn14ssKWcgFsLf+YH3Hu2tJI7ZVPXxVssGbV6h0VFXgc8bMMBbgGahS4Jvsys/I1c8Bksec/y90CFFhlmx3JwNiz0nf4vUiaktFstPdJu2sVNcdMnjoFN8VIiY/EIFhURN1Gkkemxhcw3aBBg/DIMK7Wrl59eVbW4uJiSVMRtln+3IrJEydtLC1VaWdURTRa9KbzlbtMGTlM4koESarim/e5c6+0qLH519bUVGw/L7lk9cvifD2YpWmJm/yS4Bgh6npFr7Oh4Tj+w0WyYS/j5ARUKoupcb3TcAkck/6ZlubgDosfQeBWTwgwbbgxilODZ7mz476UHaU0ugozFgsJf9JCwFnP83DfNOSqMVJ6E9QKnKv88zbGYLBcfr35pVcFjjMiyJSWznvssUSxDAXG4UO3pryyq41VpqJ4JKz9R6fmPcXX+7jnSB3t/OGaPy5/Qu4lxgMvRkoCUgL96W/4iRT4PZ3OZbg18WhZs2oVMUcWSz9lqlJhcM58cDa3L/mJXi0tnDtXzmWBkYwfENSGR6lmOYlONBDV1Ifh8kXZBJa0YKtpCMUZDDuIM8UTokQ6UqdpbW0tGJr7Hfet3ruY9WrPmj3LIRWQllioYQRqwYHUQ8PqMG5AOtR/ibWocP588S4YCpjMyo+Z86o4HA7uNomZtmQZuADrUhbvZ0kU0tgVj77HUomzY6kEtdFPaCiTghhPs20hI/ADTzLZeDbuk/pTAXOGZYzU7q0fPX/yrB8TPveqZLO3jfmGsMxg3iMmE89hf/AnzQos4/KEN+93lx/wcDF7kLAKhiZNvylZQXyec2sKdqE+b7ltgJRRjycNabdwzpo9mzIvhmshq2LjyWZl9cOb49/Wr35+xd/TDBEDEKsriaNj6MNtOH5c8DTxrPH7r1m3TnKc0HgcMZvgIFh+Bl03GN8OYkejeF/MHfA1eEqNUBOdaDQ5tvmirDlxRCPpwsSYs9sMyhFTlK0xowQ2NrpoEPl5+/bEGk0kEZURcD33TiSxZGKnIw3xmHzPRMEuJM6ttlaSgDhQJxfOK3aX4rCYbzhsAv1fHLgDgmVwIsFNgXDxyyuEBdG0ezDLa6cC3Id40PxnDSVF0gwMcSZSoxM/5ivrxC4Y/G6YeE9NG6DAMmAxGtiCM3JhgdDjXtvnxguquoI7DFxTUuZaMNYuyUemjIH2vKfEazB9OnheYBm8p7EqEV3J8ekD/YhSRvxKswdxupIYWEtwBMFPuqG0lGV5aWc5vlVQ2bASbNtCpsnG/5F4TLhCHBYbJIZo1CdS4kfk/wR4j6EQf8KIZEQWUHXUr6w0YbgsXyvtXsVvNJNdQ4hNq2jhmlhtlmpAGUFZPYEsk+ZwSFIenuWIkSOoVML/nKoDytEWi54sknNy0cPi9nHqNxJqOeJfIV1L+UPf93WF+bIh4BqqT0kSDZVoIL8ox+wbUnqLP4Q4w1VakXT0bn/jr/Ykk0KQDiRlzOq+6SaBCwmDDSQyb1MzvlUI1wI3vVDRWnbAI7bUmDOy0yasU8g2Wra0hPPT+Q6/1/rv/6a6Es61YPwlC/9+pvXmlb3kI9Swb0uLU/Ahsb/Ijz08IIiWct8yrKgFBUpSI6PfTp44Se5bPqIQDWhCfZFUsWXOlKEqEUEZ1v55ks+m/lSQW20kAQk2K+saBfcqfaj4HTHoOy6UhjKCwvGj2omJ7W3FCgj/fFtS7aEa5fgXHBYEVyHv9qbug7G3j4GC1hE5FrgAvqWGwnPodePxPfa8Jfnj8uWMlypVdYNJ+PTPizMGo+QuwTP1723bODpbadhj2IBfJB3V9PNZr/6grByNzrFCqecTDXHCpqdbZEYyRS2bmwbpL+JX+uHrCGOarJas4RMf+G3JP2+DAK7S+EpB08rkxh4dmRC3IZUoHweDTZJKMIMgRuE4UV3mUYhGU4NtsRkF1BM/0chl5X71XWCBfDgmdGzikFqnpFkw7MIOwVLBLxg/Go43KDMC1pyx+flR9RfBCKupqRmsuAsGEKQ55aWGSna4/Y4gGvFa6q3ZGvj+kDWNmB5wv5gD0BkFPEg6C6qDmGg4ccaQ1IthfhB8660va60sqT7iUbDrYdj844AXG8gZYvC5snIEDLvSgg24P2krXuM/n1a+nYrtFZdnZQ21fN68ZTHtaUdvxJ5XTEUzSYFFGXs/3ovhLTcGsG7Neyz6c8fU4OdACYCrSgDRqI/ZBVWLNc/47cG27AJJjdTlURKqGVaxYqJpFhRjx43DTIvt8tQAj0GZEbBBbH4fZX0nqgWHApLF2tWr1Ui/moD1WbyWejnX7DkRr0ZkUwj51M4lQ3I71YmUf6QF9IympCHTmPbCEmUZqEUM68+SOyYdNsqGP3wLHsGWcgI1PT4NAhJ7l+RQ89m+n1zW6q4+54k3mm05d/NL5wxmpzTDaFgSGhqOZ/XrJ/ctOCJ+8xwen0I2HIeoEo1a/7RkwdH47cFy4kz9d2S8KthosIHKVLcRI0copy/HjyiOIaczhkmOmTx5qlLwFcPeWtTjyHkr4gStvCE4phGyzKnPA4r1RoJNaiUaAVr//TwNBTba0s2XXd/umGfqwTJ4Q11ICqZcDJuB8jTEAUMLW8oSDXt8nGvAHb8ReJfk4KpcevTLT/KyLWRKmizG5N5pE9byl1jcQqqxNRLrrBqQpjs6vSarX1ZtafSAT6XfVJO/yZwp0fsuzlqBWA0kj2C0qS3eA3W3M+sVxoBaNio3hkAeNfTU0d40BVChXfChkWg0UcCVKNKEsM/FtWoibYl49SQwBlrK56k/lCOO4E8+kkcKfdiS8B/b7Xr/t2F/21en/AvGpjAma/KPf5k87GFuA0zDtj0rQ66T3vq2wPf+lrJ5qaMWJLwdSszAIBRUF5KEEtFo6qxi7X+b9AlUV9sTwybznEgoIPMvNUcIOrs60VCyUK681U0hpjnLFTd7DmwKe4TWEz6Um7LLoW3fOi4UmBANT5h3bi/q5Fp5poyBDHM6qjhPvNcfLvMd+4iTU4yOzF73/T9uxJK2DduLBNMHfzorinpN70BlvyOgSDSqS+rhl5XTQvFzx0Y0tM678jYuT1gheooq5FHh1GhgSyxiFjouz8oSB01woCISNujoWERNMFodBrMN/8cUkhswIZUSTXtLsO+byMJjvfo/+J9DFtDaW9IZzfynvA1tBck8GWXoEkGmsiTsOUv/ZH8WJnV0MccymH2uioWSFAnpRq7Lqybwk5tihspCK7JEo6mzipw4w8RhD1YoAcucs84oGGKGXWl5ZVfbiZPf948m0ODn7qA6CRSY88oPAyrGju0VWnVpiEIKPgjqd6xRQTQ72FB3ZXN1okAGg9FC1JwzdXLzJKzOGMx3OfkOvwfVg7wzmlOGz+U+JzXJ25d87ZtO3N7V8hFYdNgoL2D49qvvApIlrGjIL1WH5aywuH3Xzqf9x/ZwWZHm3jmkKNfS8z+Iu2q9u1qpIwi/2LYynPLCMlhmY2lp/EuRmukjSzSa+tIqtJ4yZ6itF8EH7bAXw44cMJLsNsOb/9g5P/cOhc1oiHBHOHc5DIo4C2QBiqmoUIqqosDwhfjDt8sojCGG9bht27I1qt+xomI7BkrnWXOMZMj5j30k93xjaG7tqdlK9Sbzj67jC0qte4Ql9TLTTRgVEHVzpZrPMazDCBtU1nsVHE/4lmEpif8hP0MS/A6Wl7SgEUHmg9+Ffa2Rvcy21FsX8VOW+GWSFaDSkhXVJUTjOdUcKk7IEo16vclo76sgtsQm0chVM+Jj4GVm5eDgacOT//xa+fjJDyhwNokZY0OtY7hIlcjql7VvtZKAOvPB2bTumQLX0GSCNS+u44Zv1DGEm5p1/wM4snJlABq8G+0mEgZjckbQczbw3ReJOuB5M7DBlHxDu9o6kjo7BkxlnU+ORyDI0NDevGybpFADcQbfTmuf8SRILJCWT8NB3zc7nDu2RCwyBpP50mshyPCZMRxwiwuYSkLltFIeexBj165eLQ5l6ghIE436ziqMfKr++XNotAdLVoQQI7uPqZ6XCyMGhNvdJ1PnPTpXrvIgLdy7TEWVyXhAxxyJypMJ6qEJbKTWH1ucUbwBrToq+JCOEuXzzpw1azFbAkKSa2j+3r1TJquJNkoUTJcOCv7wdbD5aKIO6P3yrYgZ2JqqUFyOQ16OtaTMpaAcYdhAZpm3qfnZqQ5BngFEoYVbSMVYvt4EluEnFmDe7qioEMT1Q5ABGwZtBiZIdhQLMhStHy4P/kiV5mhS5+JQHns0+gHSdNSFdvI9EwddN1iuFrUaSBONtkTKaC17TY5MTUSjbJ3hIIi/lMQrq/7PnN/8fvLESZhO+fnj8GNhVpPmEmxi9ImGhjgLjqkBzjho0CBoKArzmV4DSAECyKzZs2i+LG0PsKG0dN/evWPz83e0L9CFe2EU+Ythy3pFDgsdauqUwWyAFu694XgDPWzMVR1jhi27gDTwCnjj9AQZzJEAf07L4C94YRq5JwWIKi/YWjfvd8vVCQcBrZmRDkK578Uf7hpqgzKV08dM+x/844AXcjQIiCMp3A5p9c0DxhX0pvz88w/FVbn0XBEZi6Qgw5yzVLCXrSr4S61Eozj2sLCRAbC0RLmbyqKiIgxFvME8wtEggMfQfUWGaL6NMWNbEuRHUR2EJVkRQhIYMSWMq/yAR0GddthTt739FmbvxldL+UmJ5/p+zO+4hk184NngcTqLlWRUWqJlzapVkLP4gQmgGJr9PDi7nbVLDX8xLNdgg42lGxa3r27FHTbWe4oRGC3gCOgI6nVzaRiJrAG2CrWQOWAwWVNGnhcG/ac+k9sPHAGd+rV9boWCDxBkNjzUC0OLZGzvd59sDvVNN4JxiifY+YMtOXeOYFGEOENrQdEHTdr4vvtE6BzrGYwm+21PiQUZNR3y+LD2H61+Y4w9PHq5ob64uHjvx3uJVv7Si+INuBojdD1ml+fS2LqvSBANUXpVCyBQi6KGV5syNJhpVEZSMuyIGZ1tLT+gZLejoDXcmXO94rCwCya8slwQP7AC4PFEzV2k3Q6YFZH2bJKd2/igYyhq+wuMBjogaK84cT+8Ds0plYDZxgTc3rq34zqIgRAN9KZIC0pLCj9givh05AGKAdGs39Wm3LpAUJhCgNS8YvGKSGsVQibFv21713gPbYlcntFssKQkXf9zAcsIelephFmLnwQjH0sXhp9kiQKMnLUvrlv0ZFH+7WPAHRgGdGxwzbYYXp8cmoSM9RLaFlZuQX+0KNcs/khTIiUoqWn9zXLfsvE1GkqoylWEkMP04cmFm5oVvJUCSM5bUjCltLRDiYYaX5aXLMsfFz15km6v5uER/nI4MIxUpkpJ1rgCqRU+Ondv1SdqjpAQmC8b4j+6O/DdQclvVZaaxrxleHoTVBjuKyLmOBsU9sUSVTzBsWhrS3Yfk/oOcOdPbbWnFawVKy+YllBv16xbZ08ynv37z0Kt3zFBkjlFLTKmio2C2J+YG+8qOHklAS5QcAtgQFKpHxOByN2sNE3LpNGehYJlDH9y3VdoBQk11yBBNJoCgpWhlaoVevRIgrYTXFrm3PDgJZJicKCxWlmzY/uuFXVCVBvE6YrtFYkt/kKrcEKo0dozlw9ckpoQ8gQi6fpfgGgikS8iQDBRRTTspA22HKfvk66byn2lZqXMy7HeNdT2QkUrFCJNzW0VWIYWxBx9TejsX+8JByKCjCn9yrS7X2JHdbu2tjTJU/15OQiqPqkB5xYYLJ9FyUn96kFLaqncS4poGuMt7BAzNIkzFEsmOKa+2BRD9XnmXMONTptmkFHH3j6G9sRJFNfQGmgx8xekIeV2wB0BYqaxpnKxJAKoCSUn89ZkDf5wmEmNuIoF+YdqLgMDZinjLCTepTSVErHAwcSB65f02E9bXTufiUTimawpNz5C8iFE0GqU4cOWo8qCKQAWpBZnC42QSIiajOWZFlFSub2QaHxHKjs5MeQ8jOaQs1Fr3A0EGVp9nmGHjvodqaELgt/Y/PzOSTXi98SRtL3FBvDXvRMnxcBftN01podCD8yEwF213nNwM8YVZz01XTIgcOpzyY3VDADW6tdMiIYNvjVf3k5oVW9m5rhm/tjUqK0L5FgGZI2fcdKEsU/++IDvWzalwGQxpl4mWXOT5DdVPqO+KIIYKl0lYixfsWIRQzpBq2+RKgdaB3rZimfVy9FCokmg3qQZoQBU1vTJG7RKhliOiifYaXAEho6a1itcX1pMTmg0agpqJAS0P+SiJ4sm3zMRi4x6YVUhCJja80A0oBuVvUr53gTwXYcSTWvlUm99OeanuW8uKMDmIgWGkgZPcbFEI/ZDq4kIt16VxzBvh1q/x+rEsLoY/1tNCjsGTGa6adWO1g/rfXNuTZETbcRubOZc3XuMoqcL7x2Xvi/sIeqSwWSzXTc1ZUShxFGCvhhMv3wYkjM02RYEANdkZfWjbdcL5z8Wg2jD3bLWuBAR0cTpdIwPbBrI0rQJa7XuWDA0CcNl4ZYWqFHThicreC5pa/SNpaVcX1q2vUkDDREWQ6WtSz0o19Dq3PiXtMFWjEqgFwzpw+FwyFWTwDF3fPA+FCgaMaTstqdV2QnXsGOFkqzc7StTsNxe3I6QZai91kTK5Z5/rKSp20e/Z5jTr298ad/nwonnOUiyLhVyYi19hzHhN6q+aQ0HTAajKal1N8Pspl+FXI3e+jaV+bQU4Je8bOsLO0gXbRDNtJuSBYVmaI83/ie06T10h+HDb/zzb3J/5N8TZnvvGpMzHHf8QVIoC/vdH5VtcF8hXWNUJU42BxV+czVjlbZdX7a0BKLN8BEjJk+doiYoBqOloqIicssjRmx8bkVcvbc1JVJ2EKCfe+vLYpAPMUS2PJqxeT+pU//KrrbR2dbsPuYhgfpL/WTEO1ucNTU1tTU1OyoqMGMhOtJGKwwbpk2j+CQPS77rl+BYG2rEnfng7DWrVhH/NJuWRjyL/c57hVhXNGnGhEcruGC5Y4K/KInk3z6GtjfhH3Dvx3txxB3bK5xOJ5+MQF44uwKhSOZbRt0LuPbqTIXMQFPvQblXnmg4+vUpr/AgoRZzqI34ayTlC5rykpPlOHQqVH00xJhNZs/5I4Ravws5ZfeVA201WX3Uv35X26KtLXabAbvjw8x049WjZ1kMo5iPP+Y/EYYNRHp13bPXfvensOcIe1nCgnh8YEhfYz1SHw5WH5VlQJqdp3CRl192SZZPdqAyqscqXepocXg6/DBa2HqdaYJKaRgzNNKCu+WYY6/aEU2gqQ7yreR2YU9z8OzXMZwgBkB9UyYao6Ov5HX2Ypj/dRUzY0Jg54Hvqr764cNvnK/s2sowkcodmBh4DFjGBRQeg709IaBRM4uLSU/e2kM1JF65tIazTFP/IjBr9mz1rnfO78hWAPiYn+dC20VhQRN0pKPDTuvFq9mredvM4DnFSFw3L3Xk/LUzvwiHgva8mYLoEkglZzdPZWRAPQaPj0/3DyKduZOGzuDrKVHznhWAqQ66gTQExqk66q8+FsKKxZRBECOyGH0ipPvdVDKELEfecX+ymobJGCwpjvHPm/veIHlYz8HNbXteeHwMpCSlWvpRcd+v/uuXsRpoxKDRVTRgD1RCypizDSr529DGlXQQgoPi8WC0IxpMb7kZTup6dBbR+I58qFCvhFG8TgA/xoz7GAlbf5cEjUpgEkp0IKbOTF+SBNZwvjECD1TwTCGVGJIuCbtOtu1/SUA0RnumXH4cFzgeCcMxmGwD2mXnx1Y3iw/o4JlDTffcdr1j3ApJcyGpR1WxqO3UZySrwGg297nBkb9cbrhSE1Wcl8RoiZjXBNqbiR0tHVjAgFHZEpfRmP0UPxTqlejoFoBkIfjE31gliDRLvfk/MWND7iZxGafkYXOc5RL21PORVgFqeTULDCJq62YpghavkuSOwMnPnO/9Jw0CAssl3/iwpAObiTXqVw7qI+a7JlQRjaauuAmB+ro+OrogIM6IBww0YgHRWK64xZhyKbbEsi+oTYmnnzRkmiDYxGjvS/UmEBNtSAKZKLFXDnJJGfW4nOzQuvs5b907NFlcwe7L0CyniqJEzRqtEfNdEKqIRpM4Y89/lo4n6sACZfi+rdRazTO2CtU6uggkgyQwisSZRRGhpu20/9huQZEHyBQYQpxEgMnmyF9BpQz/sY/oh+KsH62lAtrtK9OvlhGkR5qs1ituto8VFu7gAJ5t2/NCAuPRtEbMd0GoIhrl/hgCcKsWFUnYpWl6zAHXOi4sqNMh6maCPFXJgDRMPLE/EeRiSr8y+MPXrvd/e8nMCsEu6ZM3QgUD3YA+oExxFpPA6S/pG2uWsIhkzLXoxanY3GWTBtjnOlLxO7pJIp6oX0lcBOIMo1qiURvIKOe0wvAKNFart4p1nW4SPRxgmVn3PxB1M77XUyEDwFtXLtZKHON/3/z6jLC/DYqJ2D2MyZ/MCOc/zZk0mKzmPkMFX8UwchQEmXYlxFlBJnX0YjnhQrJpQfywZRd0d3GGUUM0mvQmNmRTGpb+eeqJ5iL4ZS8m1NQruXIEhXKCrka5LTEJxXZfyCC266Z6Dmzy1f/DNuAOOScxH2EPyTghkf4i+QUCtcr8b0bRIhNpgN10mCYuGSwp9jFPKwgyiTXK8HERiDOMGqLRpDeRkM1EQLcEd18o5zSK7b5AyohC35GdoZYG0rFo2uvKy0zoHJHJVSBJGbXAVbFQ8isORB8ZMh1zWPJckK18X70baYCtGIlHQSNllM8YG+RaQnc7JFKiUa5Srr55u5z+peMiANZ8TEuxszZ90qukuoK3paW8MP3eUoUjBJ2NlAJMGQMkN4BQk5w7Ry5sD4oS+MXa/zZJioFg5f5sIy6D/GE0G+19JHMjOcSfJKkMlWVtuz6iEI0mx7ayGKK+npYtpyD6Rjq6KqKmy7mr1rMKTrvZS5xKd6x0vvub4Jm6lnd+nXb3S7LHP+dyMqX3l9sG8xOjyPPF5sC55c3kyDRnkpaEcqzhP7a79aPnQ22nI0bfaLoSw67BENA6rtrBRSPOMFGJJv722xR8P6UyMOAU2tHpuAhAjabpkzcKPjf3vSFlxLy2j1cHvj/kfPcJxx1/kNw9dK67o+UK2dKODGv6EWRCygGDs3XX8pDzZMQcY04GT8mF4XG34K76U2K9S2J09yA9PqIQjUbHtjRBkJzsaDozh4sgZEBHVGDVgSwgLrxgG0xyMcA1/uMfQ65xjP+/4sEQPF3LEJeTxailSqwkQDFtu58LQmanvZZMVus1+akyMcEcsPq27VnZ0SGs5r65F5PvNapEo1b5lGu/rSl4iQv91HHRw1tfjokkXrTBNRhIrbtWBE59fvbv09LufEEw3yI1QE22eOYhUZRAFq3fU4oxmG2Wq8dGpZiQqxEU03EWGT4uMgOCEtFo0pugA3PKObHsOE8Gm+r8jdV8iqF5sfWngvXftWv8lnulZdiVFvzLhX4mHLSfOcnzP96uZgftNJDVL0uhnGrXh7Olhebg0jYP/K8Gs3c3YuTILnh3WIEkcwWtA8YbU/s4K4rCnrMtbz9izb6L7/QJBzz0/zGckQQNfvmW+/O/kGFJbTG2tKTrpiZd/wvlgYftPV9spnUCYzivVqjsodiNoEQ0mvQm0LwC05cf8Gze5/nqu4DdZsjuY+bXCgH7VNb5XtnVlnnZJVNa/1k4P7JM0dpCyieNWqmArWi1ihZhoXUSBNU0QD1cOQWHw4Fvx47Lj6E/liYUPjpXffFQUKFkowwKWrye9pYbNGhQVr9+ghvE3VVUVCwvWaa+ldXMX97PvVdu781h+dIS7hcLNtWvvs+kvD0HGi8unlTmvjf0mvZ6S3lhsOmwt/YN3zf/Sr5hZsRuEulLqU1v8h1+z1OzNXimPuK0NlkxmZN/fD9V1pQBqdxdtb4z0/209jnoyoDYQdLxFbZISMZ2/anA0jIXKOauobYF+amS5YiwmJy9+oGyqrMbS0t3bN++nK3fBdEDkKsUA4KIWoCSFnMFv4iLsIgBWQCgHScWFy0cm5+PU3dEsYVFRUWY/GqKtuLiwQ5yZEprXJ1oaLh3yuQ169bJlwsh6f/4JfHDbijdQNv3KLeCoh0ss7L60T/x0ylf57zHzlcYIM+FFEm6NMq98QCugZgg1qEwKtLvLfV++WbbvhfD3pa2vWvcn20kocDhkMojY4j7vnnfe/i9kLMhUgvdaIYIg4Ok/vQ3ahw6nU8xFBeNASFw8jP3p6867lwpSzQJydiGIFNS5gK5vP7oJYI2xhxoAPglvbMLb2UmT51S+OtHaZ1thi0NJddxjXanlzsvbW+A2aW+DvMgVnWivEaFqcK5cyEjbHv7LTW7qwSYlOv7p7wl+IiyjFjlwd3Ne3QupQOVzTaxDX4HvCg9jb19zPIVK5Rb9KovpMZ/RsrPRQ7QoSBrSFZmsF07Ca+2PSu9X/0TdOM/GinZGfY0uyqXGpN68X1P4bbTvoa9YW8z5KCwzwnZh2t7An4xZWQnDfmZGmGB1AOtK+80RUmAGBqqdE14DmzC8kDlUFmiiV+coSwDQUauOQGRXYfN4YvNmA+Y2JhjoImYm0JwLCM5S9WA1qnDQRJbxJOISyXLsP6rYRlsLHn9kLxwYZBHYiuqiFNDMVxWUgIa7YS+4+rhrS8PnKmTyzkCB+GFgQv1J+Q8wRBLjdvHZrTgQ8kDGsw20lbpkixLv1G2AXeotBxj2PvqyzrH3CuH2BqqdDW0fvA779c7mHNRCLJEo8lAI0b1Ub8cy4BfLJnDLP3z5NYWCPa1h0hx3+EjhLm5aoAVOx6WoQBbQWSYNVvWOKIVIAjQByuDRCllptDLgrIMLasesxUJO9Jq+LQbd9fhmmDTV81vzErOnSOXGYC10XTpIFoQy3TJNXgvHqW0cIQtu8DkyFQpF0B+8TdWBRqrL2SvIR4SlcdzoYDfsPnNB2lPdIYNsMR8lyWaeNrIuTzhhVtaRmdbxSyDAaTMMgw7E9a89OLkeybGcGpCMRs2QGOK08lSwZpXE2WjocoOCAIcqrzl8qUlcroVZRlIMQq2YfUA3+GqlLsXxgxr/9ExCwXu6vXe+jKBqCsGqAS8Q8rp+6T7/ymwDHWPglmCTXWBLlCQnw/lPJ6uD5Lv/q8lEY2VLUJoTM5g5CQaUsEsDmpfv4tEOkhqTGxRknK8xHoTH9SmEEO7pY2vll7O7quwjaDGiqQCAs1lbH7CLMGkH2ZLS9Rmktu2bgVLSrIMad37ZBFbz/xZud3xc20s3YBbO8G23aCl7Wc+OFvOiLNoSTE2TmyXXgosJPFoH2zNvRJ31XpSHDpHOgwf4gz+bd2zMuFlGS4sum86Mdsr6Wn/sT2RRp1s6IAjfwVNx5chmjgMNI3Nwdf2u4sn2JUbudHB5K0rd4yTjp3Bkruo6LjWs2Pm5CsSBHVFCT4Uz0lMWsxDrWeXBFQhqspFZRnIF3J2E6oPbnv7LcmDcOZhkOOs2bMgoThbnNSxjcMqdKqjXXo3lG6IqtBpAggifmcNdod0gxfWJHHCN0U89fS6Jizd07HtO/xe60e/P9/m2Gg2985x3LmSm9qyEk3Mpyw/4O2bbiwYmhR1y+qj/rKyyq9W3FrfSCQgLoyFmxXLV6zQenbWsDJb7lvMPbAMfzJT6QbyCwBpYviIEYuXFDccb3A6nQlpUcy5mZTVE655syQjcPqgrGxStBCKlcA8zEbNPLashHjr0xxpkmogaAvSHy5S0DRKUOxKUz0aCjW1GlRCgbAgFHcRw0qi0O0kmkjhnh++pvFNBCZr6k+fsF07ib+ZBNFgz3hKt5cf8ORl25S3cXnCS8ucH9b7Bl5mzssx/dcD45N+/ACNZKH9G1W2dpWEI002lAuahWAys02zSIMbLPugmw2lpZMnToLyRYLf4u6NrdLNBPqjLCNHrGtWrVLQB2nAnqQTitp9GerGGvm+pDQEigHRgOm44+NQ3Ld4KLgFhYunEBvFrP3zxNXFY0bwTL2k5YLNnHz8oqkSK5fH0zVBi5z6v95Bm1sREDffNWkFa8R3IUE0msSZ5Nw5/KTtQ59Unmxed/etA6FCye0Clpm3qZk095yRfi5+r8px5UMjRs5miMY0f9GTRTSUJuFGSnAQaa425WNJaYU2uMGUW760JH5fjEo3EzYrfHSuAssQ4w4rE8kdAeSI3RXkr8XFxVRkk7wp2tln25bzRBODKDdIFHLNsEINNclpPZoYIZ9TLtb44gg5oehGdQtI4Z6qlzmjLxOtmrIU0Wgx0AhsdZ+d/hwa0PB5W7AEuSqfkZSM1u9qoyyT3ef82X315VRopL5bEE3MRkpni7QbgmGnHNQi5a7DNNJE60lF16DKzcQ5khSUROr/krskNW54SiXgGjn2xOeztj4ABS1OIa759Z+n3rqIX4szNW+JpX9eJ+Q6XxzoFpkH7WqDsTCYk60D71AuQigp0ai1rolDGDGNqRgCQTdtwrqzm6cI9GfOVMxnGUbUi4MaKfnyvErQhtBybmnaoJp6Z7iuw2PHjcsfl8+XnuJ3wahxM3FBMQqOJIZVrMbK515R91nh3LlRL+lyeRKhwghOFKccl3LLky3lhZCc+VxjZUMZsPDgEZNwlYvLdptAdH3HtphiSLWOrJvsY56JqvEJiUaTY1tsuMK450RoNjG3QKClV9b57DaD2FSMk/L1cLE8rxKgjDWrVkNbUZjh1CjDsNYN2uqcpkTNmj0LMy1+loGIQbMHFA5Fw5fVhN7VHqpRyDbK6tePb1KJGSBonCjOzrwmRyZjNDnf/U3KiHmCZEU82WS82H4GeNCBprpAIylUrlLS6QkNBbusOIO52bZvne+bf7WjGJPVfOkg++2/Vam6iohGi94U1RUnTrGtPuqXzKtkRHp4fv64GLJmwBQgGrzUOKepUQZbQrgAqWGv5SXLZs6apcxTaoBjKhiY1LMMUFtbq3AvWSxiv9BzGDx4sJr+TcrwHNrCBH1hNrwl6DyRMkKipy3Dkg5eNIQKpOM5uDmqHacnNBTsgnUhQq5GV+Wy4PeHIinvFEaz+UfXqacYCgmJRv3O4kUGM3byVKVl0eUNyxGNAIOuG0wPqMkkjHmLaQm1CLurVwRwCuyFF7UEk38Vcw6VoexmoizT4nQmPEwuHiTkSsAsgdNfBk59xoQCkGQD330hWSKPDzBOat6S5GFzXDuXxqZVxV9nryvAlDGwS+lN3pqt7i/+yhUGozCYk02gmLzFMRjg2xGNJse2tf9o8YdOp5O/wIZlwsPVgB5Hfd0WDnSSg2uId3nFs5qmUPw5h+BZZREDFNPQ0ADdreuwDMNKNOI4xhiQVrC6be8aoi+HAqRE3mv32W97SrnEN8N6jtImrHVXrZdrXaCALjU/Y0YXqQsBBmj9938HTlS1E2HYGF/r1f+RMnxuzN73dkSjSZwRtz0WI57Av3gAgkhzpC0qKhp7+5iZs2cLotGUQWNPMPFAVaAMrb5eqGDYV0Ea4txquLwYIhI7CArBR1oBucaSmev64HdhXyu0emfFIuuVtyo0eOQAuQasodxXgMgvBhMTDobbTifqgi84JMsMdibY+oF/9dRsYwJuvsealAdLzlBZHkwZ7YlGU1NKKQPN5VlZtefswXLykcsjXYSRmBJ5oIlOg2MNpSEO3ZHvbyjdsLG0FGv12Px8TaXz2EyrvYsgFn3wvtZTg0E29lMKAsJXa15cR6Nv4+QaNXUI1UBlJT2VgAjTa/o25z8XkCbZoYDv2w/8J/Yl/+RB5dYCDDuoTAVr5QIjGFZ+MZhtYX+br2GvdcB4+iEmarcODr5QzZtoYVNvfXmopeF80B0L/Mjmy29SWR5MDQQSjVol2WjvK3kFWK5bzoWxQBIWb5B9mbnqqF/8OcaK4IAgLAcQh36BfQvnP4YXrXfJ+bOp1ymqCWbRkuL828eQ7EqNxhroj1GDgHABUM1ooYZ4uEa5DqEmRK2kpwl4oGkT/0RyeXc+A7kG0k3b/pc8h15Pvfk/lTUpGhih1MzEYBTukpHdfb3mpCB/57ZVUeIXk82YlpU0eEr8IowA54lGk2NbwRVH10ZvfZmkKyEvx/rafndjc1BQcM8i6gmFhToGcyyZdsePC/QdWsiKOefPJnE0GzaAxJS1qiw2EUEhKkcO2KvF6aTBzQpcw9mSsrL6KUQP84VESaQ5HIlNiUwgiGgz7XUSqP7N+1D7Q66T0KSMKZcq0w3bEnuBpX8e1ioxg4R55kkK61V53ZdoxD1nOgjBM/Xew+/6vvonUWnb8wvpyWlLsw2aFLVIe8zgEY22XnHSBhrMh717PlKw6uVeaRl4mbmkzLVmRjr/c4GnHPpObW3tmpdeVH9JFDRvSM6OS/3ZDMuGVKvasX27Ah3gdmJw+hIrz3MrQDTLSkqUpZXzXNMvS04qUXY8jxg5AoohbqdLmZb5wMC15y0Jj1rgev8pIjIHfZRuDCZrcu5Dtmsnyo1sUrdowjBalYov7RqTM3AEfskrKwk+7pDW1x2N5Nw5HRofBNHBf+wjz5dvB5vqmaC3nf2FlV8YSzKp7HPd1I7O5Ggn0ajfTfDr4H4CZ+owJi5trdq37xN39RGFfZdMsM9+9ezSMidXsEbQnZJ4l9l4lhgiREA0EAGiupyoVpU/Lh90oBB/HPPs5awwxCatGNHDcQ0jU+wOVLJm1WpGhq+oDUsuj4kPGg/dQRXXo4LtePsH0heJbQ4H6SYcCpB645++au77k5Rhj8g5jzABohpKsY1Cs+0uC8zwDrLOYC6T0qjQUdw/CIUX1kttdOBXLbBePabTMsUiRKM1Y/uHP4+X/HyojdS8UYjKA7L7mIsn2F+oaJ16tGnOrSl52bbe2Xl0WSO1Dti6CvdOmRxbOZi9H++lBVkWFS2M6nLCZviqRT43Kh5wVpioET34tvZQjRzXjB03DrQrZyrC9ZMuCKtWK9u5Sd2sooX4d0dFBYiY6JLR3PAdAQxr+9hl/EhTiPH+o7tbTuxnzGqXVlPGAEg0AjUfMzbsc3Z0j9oEAsyYWJY5Ty6eZoFzmqEVlE02U6+rkgZPsVxxc+fniJu5q0zI4TLTTdCMyg54lKPyCoYmYYP1u9qgQ5UwGDEb2RfBoEGD1qxbF9uqW8sWl6PN0t54602qHOHF+pvGiRuSLF9agu0VKpDH6YvhGCRquUywaouzRZJrQAfDR4zYwN6F5L6Li4sJpcqbhGiIIP6lHR22bdlKsy46rqWMMjDKU295Ei/f4ffcn20kJkmI9AGv58Amb80WyjgK5cQNNlbpDgUFn6eMWpB0/XTPF5t9Ryq7bA4nK7zngWLiFyWCZ+r9J/b5T1YHvjtI+j2IDaxGM/jFmNpHU3n2DsI5oklECyeKgqG2V3a1ucaGlSvsgZKgOi0Yaz+SPh4Di36Y1a8ff5nVanqgehOd0lQ5gjhDqsyxvVMY1rDKHR+s5HQ6oaApiBs4YJzlr8AgDQ0NaqpeUGuOJCuRG7n/AbmLoZmiOMW9EyfhfvkdrLALRCHQCt2Gfk49cfRz/CwdIeBQVTrYVE9TB0jOgT1TPNCtA8bjJQziYBkHsonBkiK5AluzRpD+ByE/7UzGPyBbnmYBLU+BC6BVgYPOuOorxQC2rIyDb14gXnmrXX29dEmAWYJnv/E17CViC7+ZDB8gF6PJkHKpuc8NmFZdJ0GMk2gSZrSfMDQJRLN5vxtqUdSN0y/PHj35acmvqLSvqRA39CaILfxPMME4lxNmHdsxNqIoYWph3irMLhBEbW1t4fz56i9AEstXPEvC854siprZRLlGzErEGZ+fj19DzmWOjXd88P6ykhKarsX/ilbMEuuP1C5Oave9SuKMSOfcBBGNq6JIsmCw0d6XVMO6frpgsmEGJg97GC8Qh+fQFt+RneHW0+GgF4pV4NTnrlOfG8zJjNlmvmyIpW+u9eoxmEXYKxwKgEHk5m2kBj5vmtEy5gF2nIN9QiwDxtm8jHBHBmFPeiJzZm6cVMIH5Ur/sY+CzUdImypPM65XgllYmwtjshhTLu0KkoscCNHEWYpcAAgy04Ynv7bPDdFGrmkcBeuPeEruW8wrrfkHOyoqFApEcUnbKkHrnMevWVBpgmo3UdvRgWsKW+aKuQZshSPMe3SuXOPKSDG9FQy//bZAQhQD39bU1LC1MhKmQMmVJceUhpyCl7lvLnQH8WJL5JERhXhFfCU1W4Nnv2V9JW4IO/6ju/FyV72MRZsxGJhwGIKP0epQOa+IVKWlUKZcfwVz75yOMHDQ07GxLcdDrd+xAktIbGqhoC2rIDSZM4dBvjP3Gdr1q3+xRJM4vYkCskxlnW/hFueGh3rJbUMCugrWyo0S4njW2NeJRhInpNAvwypWtCFBQo7GaTdq0g6oBCTgGvVH0JSDit953969CSk0oR6Bk9XO8kLqc5GcIcSQwWpVzLm2tr4jlUT9CYfZuRdZ1UFGzQ17DeYkEgaSnGG6dJDJ3seceWNCxApKTAkHNYbiykOes8HTtWF/G5sPaAgHPOdr7opAqC0UNDoycY/dhVkEiEg0CT/ukgn2wk3NfB82H8osQ93bDoe27Jsd2ysuT1DNBFqSilThS1xzNcx/Iq3MnQslRbnIDscpAq7Bm8jnx+9f++K6+ANn6O8877HHEsXOFBBY1ITP0a47Cu3iKDCjkobOoLkLVJvAXr7D75FpCbnmnBE0iHn7w9dkh+pX2QXfwhiMBgtR3mnMl5ltOMew2VIdpFxwQhDtzEs/pPE+IBTaMjyq6hC5eCKwpJr73EAvu+uYWmKGOc5S5HLI7mN+PD+1pIz8rAvGtmu9YsoY6Bi3Qo6SuX4AWVn9NPV1yuqXdaKhAfpF4fzoPWcVgJMWslU41yaiTxsfpPZN8WLCoazlSGHL8/JLe8sO5RooUFGbZyvD2dKyZtVqiGy0+E5sB5GD0aZBs3BXr/cc3Jwy6nE1WYWc5cXF1n41mKz28c8T6cB5IsBKB5jMEA1YQwaRemihJto593w0MfHF8OquCf7UAo4+In8qSiVC0POGgoYk4kQDG9I+4gm08nQpmDsuwbpgaJLdZiwpc9afal6Qn5p7pQUDJWnIdLnwAWr9hcaE0b9oSTGJUtMCiAljx41bs2oV7aMg8L+oQUNDA3Z/Y+u2sfn5WutLqL/Impqa5UtLojq8BXINn2veeOtNWsgCMtcsctca6AY/ckVFBS4A72MOI1CGKSOH0dI9jhTTZ9vFRe1OyeGc4ymIaWlpX16LihVUN6H2DnIKTzNjNGF7onmJPMHSOb5x47yYdo7L+LJVxwlWXRPmhBto+MjLsWb36QW5BmrUjUOumjJj9riBd4s3o/U0ofsw7Uf/vr17JXsGyYGtv7uicP78ja+WUv8LLQlMZvV1sl4V8At1A9NgtqhlaKJelbJpCVcI3WfyxEkK2wgg4Bpq96W1BKl/Oj8/n0QPRbtHepsMK1stLi5Wz6SanoK1f14MQbq0oWDbnheIU7Z/nkBZIJaaI5Vc8qG5z1CDyRIO+gOnDljbr/8KRl+qedH3lIm4r6i5ROs1M6JcHDAgdYox3bBDU4fCnEDHNh/U84cFZ2Bm7l8eGbb/4NGNpRuKi3+HF9+SggW2traWYeP0qMrDjX7W/azBGMwBB6fl8mgK5Y7t27mSTjiLYHaBNegbSDFqKl2puaq0aBMYSln8dTPBLBv/ZyQYBDdYsZ10v6OfC2iO+4UZ9h5JSrrqWhkMm+KgxlTMr+aBqW60943NbUxialjPFMOrEEAtPtC4OaIhn5usTNDPLxYRFVTzou91FuhkmO35iXGsnD+ilP9vxMhMrtQ+P5iFYRs5SS7F8VfD5VIoGSbi9N378V7BNjRQTb01NCE1emnXujgPwl0P1DFqXcZv62xxiikM5ALui61JVmyXSlrixp15BKris1Ww6avWyqVcrrPB6gj7WgMn9rffpTHsdXWOSkKMm85GEonjPMnEFJuTfu+GnqM9mTuZ2rUGsyQKdJpdkFN3GujdXZCcSQFsOQUdkeLorS+3sM1bGFZn8dWXh9xN/A3APs3bZoGMEttRgAv2C+NfvI8vzI+DQle8iw/Svbd16IgHbMp1QUJ6VAoAocYyfSvt5EPsweEQv0sPzSRyVSxktaRcU0aOOTNXvdmVOkZo5x8mEsjq7OT0hYsVOtHo6BBYO4Zo2MSozTSw2GBOCgc83sPvpvB4BF/5jlRiMxKgjFd7CyTNQuL+TJRsoiMqdKLR0SEAEaiM3NMKz8HNNMbPkNQr7Drp+/Z9fgMp0lChYG1L+TzJ0LguJZ4IimRf3NCJRkdHAcKFs1y6h1w8YKWVnbbsCdar/8NzYFO47YxgA1J1uGCts6Koi0srF2Vgnhx0otHRUYBQY+0/Wi7HMh5QR49twB2QbsJBn+/wewInN7gmffLGtj0rO0J9Swgg7l3oS+hU6ESjowORMmpBRxCNgc1yIEVeLKlhb4vny7fF0TSkvFbekqQh093Vf+qIa4gT5h7j2KbQiUZHB6Ij3E+QBbhMBVOvqwKnPg+eqZXbGGRkz19BKt18QTp8d532T2r6L15M0IlGR8ciedicRBGN0d5XkA+VNHiK69TnYb+b7+SW2jFSec93pNJ/pJK6pRJySbGBuuEv4AV0PnSi0dGxwCSP3/0kphgKaEyGj35PutNV/dEx7vdRj2Nl4/1SSXWuC8k4PY1lGJ1odHQC4nE/yVEMw0bB+BurDBY7aaVw/N+tlUsJqWXmqgl25xiHtFWrL+vkeubWc0Wyew50otHR4cDMjy3N0pZdINnIEfzirlofcjcZrSmk/BUQCgW+P2QK+gInP23d+f9Zr7o9ediv1NTchMKV0ptoVZ3GOPgpemBKp040OjoDEB+0Nl2SZBm2eM0zgaavmYAb8kvI10L+bTvNhAKk1G7ASxxSRpP/5Ke+rfebMwbYhv5S5azmGAcs5mOr/2m6WvVIur5TO213EehEo6MzAN1HE9GYMgZizgs+hNDh2vk0OMWY1CvU5rcNvpeqVM7t/9t/dDcTDKRP3kA6Bxyp9H3zvjH1ssDZb4O7ngX7JN/0iMqSWgxtxZs5DPpa256VCfeLs1laaq/kYoJONDo6A7SxkXrLqz3vKYHiA5YBoRhs6eGAy3rNOH6dxpRhj7Q07AsHfd4v30oaOgM0AZKCHuStI+nd4ZDP88Vrns//kvTj+9VPctJUM38FNKnWyqUJNBhDsuv8LpFdATrR6OgkYI6p1EeShkwT+Kox1SnLMAZT8o0PC/giwmLuJvdnG2kZc4aVofACPXkObvaf2B8OuGOgG1yzZfpWKGuJEm06qNl214dONDo6CebMXDVEw/aT+5XgQ+f2IpKr7W0WswwF+KVt75owqRa823LFLdzn4KDUvCUhV6O7ar3v6G6DyeKtecP9yctyx5G8Hog2YCscIU7RBvpgj8pv4kMnGh2dBGv/21qZkqibJQ0Rdl/BDGeCPiYUVGAH27UT3VUvhwNe96elfKKhwPQG3SRzdGM0Qbrx1r6dfNMjKk3FuCpL32Guymfiyf+25fRE6wyFTjQ6OglsOGz0HEtbTrsYEwgj3i/fIP2OfjRYwDKCkleWfj/1fftB4PtacU9uivN0s/8l37F/M0ZT64fLjamX2W9bokbQ4BI1tbrPOICqYtvxIoBONDo6D5b+ecpEY8suEMx5186lxADsbU5t3z25tXKp70hl+uQN3PYpIx/zHd3FhAKuD55Ju1u6JxfVfVJvf5rSjf/UgVDzkZZ3HrVcflPKqAVqzLTYzJw5LAYLMSnX38MSKfnQiUZH5wEiSdueFxSmqMBW6m+sCrmbDOFg8k2/5rOA5+Bmau6BfGHPjzQIJmHBP7oucOrzwOmDOIWYNSABGWz2s5unQrACX4BuiL9893OMtxknan7jQdvAO5XbZlJY++eZCtZqVaMsmT2rLoQAOtHo6FSwdRuk65Zj/gvEGWJ/9Z41OPrxlSZq2aXvIR+BIzg7i/323559/RdM0NcKAhIF+3Fl97AXXimjHsfFpE98GUdo/eBpxhD01r3l+6ZCjVuK1NaasE5TvRvSV68HQycaHZ0KiAykWpWUUJOc287ZRHoPkCQDR8rwR/mfC7w/mO3pkzfS9+Apk71vsPmo/5v3wyJVyIiveDIIZCucIjVvCXiq1y/eJH6lz/4Mzcvz6QY1dmJa7waMg+OouXGzLtHo0NFpoPPTVbFQ8Lktu0BgwsDMZ/ytTGof/oQntuH2QgS4w1tfxskgKbc86Xz3N+GA2/X+U447/sDf0uTIFCg79FA00QHSDWlHVfUn37cfhF0nSLsFNj5Y2U6MvUwZ2bidqCabHpjfxIdONDo6G9b+eUlDpvF9N2AfQcIB5q3/uwOMyZY0+F7+55zSJPjQ2v82Kr9gPpt6XRU8U+dv2C9wP0maovlcQy8DdNP60fOgKv+JT/yN1WBAZcMNzpg+eYNze5GCyQZaodxXPQQ60ei4AGBdPA7OWIN5LpjJviM7DaakcNDFN5ewZcklWsWHXCdpDxb6pyN/ObXUOCsWpd9bym0GMmqzSpii+VzDUPvLPS/hRO79LzJGK0ll+Pq9lOHzFIrIkNYLE9YpBBD3tHp6YuhEo+PCALxgzsyFMGLLKRDPYf+RyrD7jPWadl03wT5yGgr0LByHyi/E/XTpIFLis+kwP1AYXCZnihZwDUOTDzKHkQKgX/3TwBjc+1/yfL7JPua3cpoUDSCGwiVpHu6ZiZR86ESj44KB5ElPkFjqSa/IlgZjWj/B/PTWybp4sAs4i2MK++2/bd5yP9Sf1g+X95rxDrcZlCBIKJIVZ8RcwyZDzAF/uXYuZcLBYPOxlnceVa50I2kehvLVMxMp+dCJRkeXA2nbdO09kCb45uGQq1G5HiiYgphm2V0gd1iuHkObc7urXk4e9jDdhpqi5cr9ibmGiahFa0FP7k9eNvW6yndst++bHck3/VpOSKEpFK2VJdwZe2wiJR860ejocoDeZMrIEehTtDG2Mlr3rAQp0Pepoxb4j+4izVgO/C3p+l9wMgXEKIEpmg/KNeIoYdKvrv9tbXtWhtw/MIZIqlTqzf8pGeyLjc0ZOTRsJ2XU4z02kZIPnWh0dDkEncRbZGlPNIHG6OXNIfJwrm4ivPz0N67KpVCgWt75dfqUv3CbgUcCZ+rl5CNwTeBMnWPcCgFBUGmIRPdVLjXZLw86jzl3LLYNvFNSYKHdMoOuxh5Yh1wSOtHo6FoInqk3987BfBY4vNVINAwbhse5uq0Dxps+2xj84etg81FBN0vwSPO2mXLlgYNNXzVvmwUGgR4k+IpE903fSioWtzYyRqv/+B7ft+/bb/utWLTBJz05uUkAnWh0dC34T1aZM3MhUwg+V1kznBYV5hKgHON/T63Crg+XXXLFzZxChDeO/BVcUoLkccBZgcaqVFGtP4Z1mUWMxEFPOOiFaGO98haV5dB7JnSi0dG1gLltYV3L/A9VijMUbCpTJdVZoP4kDf25+/O/MEGfQIGi2o0C19BDBbbNBCWJZZPzRuL9fzQkX2J0ZLaUzQXX6LqSJHSi0dG1EHQ2WjCNHXEZUFsrl5rPVZBIHvaw9/B7oZYGKFBte9ekjDjvcmIb5j6r3HMKklTzG7NoBqb4W2okZhMXKsEy/iOVni82qyxw06OgE42OLgQqXIScJwUpiObe2lKf2RrDRWkT1lFdJu2uVTRW2Htoi/XK0ea+N3BbQnRKzSvmvNFyoBmYkjVraOICvm3ds9LkyEy6fjpOzeZJ6ZrUeehEo6MLgQTvXj8ds9renmhimLHBpq/a9qykQTGkpcHoxa6dzxB7SkVRr2mv8w9IvVRRuYZ6o+x5T0maeNnCEawmVfUnYo222aFJ4Y2aAjc9ATrR6OhawLQPNFabRKpTDA28QQ2gGOp+tg4Yb/7y7UDjJ2FvS0t5IT8HilHNNSCvlvJ5IC85QwzVpEjiQl2ZLWdC2OsC3YBr9BQEnWh0dDlItsG1XpWnlWgAd/V6o6Mvneck2fLv08Kes8Ezda27n0u95Un+liq5BkqZq2IhrdEnaYihocCsXLaSOumhVTVvm9nD6UYnGh3dA7QMaAw7gjtI+F/mMFBA+qT1EWPNl2+ZMwbaBk8RnIJRwTVMpLJfddKQ6XKaEY3uo8UAg87G5GG/6uF0oxONju4BNvdaNnVAGZBB0grWmnpnR4w1lUuZUKDt49WmSwbwDcOMFq4haZzV6/EiBWvO5VgJQPsuiOmmB9pudKLR0W2AieqtL4+hixt2aSmfR7nGOmB80pk6EBYxDL+7IO3uPwo4AlwTcp6UK2wsBi4JL3PfXFtOgaS0wqcbKFOgGHzYUjbX3DsnagW/iwY60ejoNmBrvkQJe5ED5RraniVlRGHg9JfEMBwA1zyRft/fBMIF5r+4ZqgyAier8SLldbInSEorHN14vtgMurHlTMA2JEvT62Ir8tx2cQs4OtHo6E6wZA5LGfV4bMYafnBNWsHq5td/Hmw+GnI3nX3tPoHDmzlnkNbENQwb3QdRyHNws2SeFMPSTcqoBbgSb30ZGAdCTRLbIJwKONbsgou1tLBONDq6GTCBMTO1UgAF8U+XzaVdE9Im/ok6ocLeluYtM9KnbkoI1zDn8qS8dWVyQTe01h9eviOVvvqyoLORFYIcvvpyyDjgGnPmsIsslUEnGh3dD5r6nAgArmmtXEqrFPf62WsQZ0A0obbTzVsfSJ/y50RxDT2RQu4CBdgEL+hTPjZ3gQYWM2xFHnfVnyDjmDNzwTsdZMcBIYZ9zs4xEulEo6NbArMXMySG1rQML5CPcM201yNc03oqsXINhUIKOAeS+ckKOKykVgbSMffOpjIOOAgyDkQeSjrmjJz4S0+QDhONVeAynMiWXSAox9FB0IlGR3cFqR8+fatC7wEFcIF8xF5z1+qWd34dDrgh18jZazDJ1fi8JaGQAi4ANkjpvQAzn8o43rpySByQaKzsdQab6j0HN9MCGpB9TBk5uAWQlNHqUD4yzX0PNFaTcqhn6lhBhrQMxg/YOSzD6ESjo1uD9h7ARHJXrdcaNwzigMhAGmn3zk67+yXnu0+E3E3EXvP6zx13/EHs82bUxddIgqaAJ+fOURk+w8k4DEsT4Aj8C9IhBuP+t4H1KFOAeih9QOTh9sVmDPllHNwnkI8CZ+qNNjs2w474k9qAOtOzrhONjm4P2k0hBrohRTnZ4Bq80u/7W0SHcje1vPOI446V4lg+zEw1TSnlQB1SpCXmuc4wakDuLnNYMkMytqBbBSGVNFYHm+pCXhfXIQ/cwWcWhqUbkAv3BhsYHZnJucMuVNE/nWh0XCSgdEPj4qB3qKEDfiAftddAnCFyTcDr/OeClJGPCXIUcAps7KwoUlnuT/KMNJ7YlDGQWHmJBpQNPUgl70TKg7Z3SOGY4oKEDFsaObaL7AjoRKPjogKNi0shPS13er7YrNCmloJyjT3/2Ugy1H1/aykvDJ6pCwe9rf/+70DTV4LcSyL7TN7o3F4UQ4YnH7gw8bXhAsA73J/qY2ooUXblGBydaHRchMCsg6ZjY2PhoK0oCzgkkK+8MDWvmNqG0+8tdb77hP/EJ0wo4K17J/D9obSCNXzDCrEfT1gLuUl9moJKENmEx18xcJm1/2hL/7wumLepE42OixlY6qmAQ2rEyPSopGitLPEfqaR+aMcdf3BXv+L57M+QayDdnH3tvrS7VgusG8nD5lj758WjRnUE2HrJxAfX1bhGJxodFz9ojRi8wDVte16Qk25I8YfNU2jxh+Tch8yXXuv615JwwEtqZb39SNKPZ3AdLymoGuWu+lNsOeUdBFu2dG7nhYVONDp6ELha4nLUQI210LYgrYBxev3i7eY3Hwy1NEC0cX/2Z9/R3WI1KmXUAhzWVflMVHtQJyBpyLQuZQPmoBONjp4FNdTAJj1Gij+k3rrI93WFt+4dJhQgatRf78En/F50zDnRRllc6mgY7X2hJHZZe7BONDp6Itha4uuiaj2Bk9XUTmy98hZS29zbAk0Kb8wHX3PcuVKyP7fni80QiDqTbkwZA7t+4T6daHT0UFDRxtI/jyQTKWo9kFN6Td/aa9rrrp1P+4/tYcLBwPeHfth0d8qNjyQNnSE4Ji0Y3Al0AxHGyjqYukXjXZ1odPRoQNdgDbrrFXiBZiFiVjvG/d5/bLersiTsOcsEfW37X/Icel2cxMS3PXvryuOMuBEA2pyFTSDoFvzCQScaHToivbRBN3JZ2sEz9TQe13LFLb1+9lrrnpW+w+8xoQBJYnr7YfNlQx35y8VJTDSWh2ZI+r6tjI1xoBmZe5OkbVNGdpc1wUSFTjQ6dBDQkGIihtSVK0fckEzOvCWhYXOcFYuCTYch2gQaPzn710mWq8ekSrWy5DIkaa4Al6kUbKoXyFDgFIPVQTMSCLOw/3bI3XY6dKLRoeM8aJ0avCDC+E9WkSRGNjFaXO8OW6bfWwpNqvWj50Ntp8MBt6++3P/NDtu1k+Sa4dIsge4rlcQDnWh06JBAJH0xGogmNf0Wb83Wtr1rwTXhgNdz8DXvl2/brr1H773Nh040OnTEC9vgKXh5DmxyV78SDvrAOKAbT+0b1mvyU3pMQxVl6ESjQ0dikDR0Bl6+w++17X8p5G5igj4oU3hZrrzFfttve7h0oxONDh2JhHXAeLyCZ+rdBzfbrh7DmJMDjZ9Q7/iFvrQLif8fSOvgZPhXOCIAAAAASUVORK5CYII='
						}
					}	
					pdfMake.createPdf(docDefinition).open();
				}
			}
		};
	})
	.filter('categoriesFilter', function(){
		return function(input, role_filter, selected_category){
			var output = [];

			if(role_filter.role === 'Giocatore'){
				angular.forEach(input, function(player){
					if(player.category === selected_category.category){
						output.push(player);
					}
				});
				return output;
			}
			else{
				return input;
			}
		};
	})
	.filter("emptyToStart", function () {
		return function (array, key) {
			if(!angular.isArray(array)) return;
			var present = array.filter(function (item) {
				return item[key];
			});
			var empty = array.filter(function (item) {
				return !item[key]
			});
			return empty.concat(present);
		}
	});